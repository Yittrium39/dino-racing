The following license applies to most code and art assets contained in this
repository. Please refer to any included libraries for their respective
licenses:

 - libjpeg (permissive free)
 - libpng (zlib/libpng)
 - zlib (zlib/libpng)
 - cjson (MIT)
 - GLEW (Modified BSD/MIT/GNU GPL)
 - freeglut (X-Consortium)
 - GLFW (zlib/libpng)
 - GLM (MIT)
 - boost (boost)
 - bullet (zlib/libpng)
 - fmod (proprietary)
 - freetype (permissive free)
 - Assimp (BSD)
 - librocket (MIT)

Certain assets in this game were taken without permission from Mario Kart
Double Dash, Castle Crashers, and other games. Use these assets at your own
risk.

Copyright (c) 2013 Richie Steigerwald, Chris McKee, Charlie Schaffer, Steven
                   Livingston, Kevin Ubay-ubay, Robert Burton

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

