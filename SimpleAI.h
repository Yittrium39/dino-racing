//
//  SimpleAI.h
//  CrusinPangaea
//
//  Created by Robert Burton on 2/19/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include "KartControl.h"
#include "Spline.h"


class SimpleAI {
    KartControl& kart;
    Spline& spline;
    
    
public:
    void update(float deltaT);
    int nextPoint;
    glm::vec3 oldPosition;
    float timePassed;
    SimpleAI(KartControl& k, Spline& s):kart(k), spline(s), nextPoint(0), oldPosition(glm::vec3(0,100,0)), timePassed(0){}
    
};