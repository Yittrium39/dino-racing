//
//  Spline.h
//  CrusinPangaea
//
//  Created by Steven Livingston on 2/19/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#ifndef __CrusinPangaea__Spline__
#define __CrusinPangaea__Spline__

#include <vector>
#include <iostream>
#include <glm/glm.hpp>

class Spline {
public:
    std::vector<glm::vec3> splinePoints;
    void createSpline();
};

#endif /* defined(__CrusinPangaea__Spline__) */
