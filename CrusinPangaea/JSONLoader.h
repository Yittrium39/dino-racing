// Once Sentence Description:
// This class is an alternate interface for cJSON calls

#pragma once

#include <string>
#include <memory>
#include <glm/glm.hpp>
#include <vector>
#include "cJSON.h"

typedef cJSON JSONNode;

	class JSONException : public std::exception {
		std::string info;
	public:
		JSONException(const std::string& str) throw() : info(str) { }
		~JSONException() throw() { }
		virtual const char* what() const throw() {
			return (std::string("JSONException: ")+info).c_str();
		}
	};

class JSONLoader {
	JSONLoader();
	JSONLoader(const JSONLoader &);
	~JSONLoader();
public:
	static std::shared_ptr<JSONNode> load(std::string filename);

	static JSONNode *getNode(JSONNode *parentNode, std::string name);
	static JSONNode *getNode(JSONNode *parentNode, int i);
   static std::vector<JSONNode*> getNodes(JSONNode *parentNode, std::string name);
	static int getInt(JSONNode *parentNode, std::string name, int _default);
	static float getFloat(JSONNode *parentNode, std::string name, float _default);
	static glm::vec2 getVec2(JSONNode *parentNode, std::string name, glm::vec2 _default);
	static glm::vec3 getVec3(JSONNode *parentNode, std::string name, glm::vec3 _default);
	static glm::vec4 getVec4(JSONNode *parentNode, std::string name, glm::vec4 _default);
	static int getCount(JSONNode *parentNode);
	static std::string getString(JSONNode *parentNode, std::string name);
};