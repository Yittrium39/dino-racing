//
//  MeshShader.h
//  CrusinPangaea
//
//  Created by Richie on 2/17/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#pragma once

#ifdef _WIN32
#define GLEW_STATIC
#endif

#include "SceneShader.h"
#include "Program.hpp"
#include "VertexArray.hpp"

class FlatShader : public SceneShader {
   gl::program meshProgram;

   public:
      FlatShader();
   
      virtual bool canRenderRenderable(const Renderable &renderable) const;
      virtual gl::vertex_array vaoForRenderable(const Renderable &renderable) const;

      virtual void setModel(const glm::mat4 &model);
      virtual void setView(const glm::mat4 &view);
      virtual void setProjection(const glm::mat4 &proj);
      virtual void setMaterial(const Material &mat);
      virtual void use();
   

};