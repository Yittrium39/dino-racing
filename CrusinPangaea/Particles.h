//
//  Particles.h
//  CrusinPangaea
//
//  Created by Robert Burton on 2/25/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include "KartControl.h"
#include "Spline.h"

typedef struct {
    glm::vec3 location;
    glm::vec3 velocity;
    glm::vec3 color;
    int active;
    int life;
} Particle;

class Particles {
    
public:
    void update(float deltaT, glm::vec3 kartPosition, glm::vec3 kartDirection);
    void addParticle(glm::vec3 location, glm::vec3 velocity);
    void createExplosion(glm::vec3 location);
    glm::vec3 getColor(int life);
    std::vector<Particle> particles;

    
};