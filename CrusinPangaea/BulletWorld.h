#ifndef BulletWorld_H
#define BulletWorld_H

#include <vector>
#include <glm/glm.hpp>
#include <btBulletDynamicsCommon.h>
#include <BulletDynamics/Vehicle/btRaycastVehicle.h>
#include "BulletKart.h"
#include "SceneObject.h"

namespace physics {

class BulletWorld : public World {
	void clean();

public:
	BulletWorld(float gravity);
	~BulletWorld() { clean(); }

	virtual physics::Kart *makeKart();
	virtual void setTrack(SceneObject*);
   virtual void update(float deltaT);

	btDynamicsWorld* getDynamicsWorld() { return dynamicsWorld; }

private:
	btClock clock;
	btAxisSweep3* broadphase;
	btDefaultCollisionConfiguration* collisionConfiguration;
	btCollisionDispatcher* dispatcher;
	btSequentialImpulseConstraintSolver* solver;
	btDiscreteDynamicsWorld* dynamicsWorld;
	btAlignedObjectArray<btCollisionShape*> collisionShapes;
};

}
#endif // BulletWorld_H