//
//  SkinShader.cpp
//  CrusinPangaea
//
//  Created by Richie on 2/17/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#include "SkinShader.h"

#include <exception>
#include <iostream>
#include <sstream>
#include <glm/gtc/type_ptr.hpp>

SkinShader::SkinShader() {
   gl::shader mesh_vertex(gl::shader_type::vertex, GLSL(
      in vec3 position;
      in vec3 normal;
      in vec2 tex_coord;
      in ivec4 boneIndices;
	  in vec4 boneWeights;

      out vec2 tc;
      out vec3 norm;
      out vec3 pos;
      
      uniform mat4 model;
      uniform mat4 projection;
      uniform mat4 view;
      uniform mat4 bones[100];

      void main() {
		 mat4 boneTransform = mat4(1.0);
		 boneTransform = bones[boneIndices.x] * boneWeights.x;
		 boneTransform += bones[boneIndices.y] * boneWeights.y;
		 boneTransform += bones[boneIndices.z] * boneWeights.z;
		 boneTransform += bones[boneIndices.a] * boneWeights.a;

         tc = tex_coord;
		 mat3 mv = mat3(model[0].xyz, model[1].xyz, model[2].xyz);
		 norm = mv * mat3(boneTransform) * normal;

         pos = (model * boneTransform * vec4(position, 1)).xyz;
         gl_Position = projection * view * model * boneTransform * vec4(position, 1);
      }
   ));
   gl::shader mesh_fragment(gl::shader_type::fragment, GLSL(
      in vec2 tc;
      in vec3 norm;
      in vec3 pos;
      
      out vec3 fnormal;
      out vec3 fcolor;
      out vec3 fpos;
      
      uniform sampler2D tex;
      void main() {
         vec4 color = texture(tex, tc, -4);
         if (color.a < 0.01) {
            discard;
         }
         fcolor = texture(tex, tc, -4).xyz;
         fnormal = norm;
         fpos = pos;
      }
   ));
   meshProgram.Attach(mesh_vertex);
   meshProgram.Attach(mesh_fragment);
   glBindFragDataLocation(meshProgram, 0, "fcolor");
   glBindFragDataLocation(meshProgram, 1, "fnormal");
   glBindFragDataLocation(meshProgram, 2, "fpos");
   meshProgram.Link();
}

bool SkinShader::canRenderRenderable(const Renderable &renderable) const {
   ShaderContract position_contract(ShaderAttribute::Position, 3);
   ShaderContract normal_contract(ShaderAttribute::Normal, 3);
   ShaderContract texcoord_contract(ShaderAttribute::TextureCoordinate, 2);
   ShaderContract boneIndices_contract(ShaderAttribute::BoneIndices, 4);
   ShaderContract boneWeights_contract(ShaderAttribute::BoneWeights, 4);

   AttributeInfo position_info;
   AttributeInfo normal_info;
   AttributeInfo texcoord_info;
   AttributeInfo boneIndices_info;
   AttributeInfo boneWeights_info;

   bool has_position = renderable.getAttributeInfo(position_contract, position_info);
   bool has_normal = renderable.getAttributeInfo(normal_contract, normal_info);
   bool has_texcoord = renderable.getAttributeInfo(texcoord_contract, texcoord_info);
   bool has_boneIndices = renderable.getAttributeInfo(boneIndices_contract, boneIndices_info);
   bool has_boneWeights = renderable.getAttributeInfo(boneWeights_contract, boneWeights_info);

   Material mat = renderable.getMaterial();
   
   return has_position && has_normal && has_texcoord && mat.diffuse.hasTexture && has_boneIndices && has_boneWeights;
}

gl::vertex_array SkinShader::vaoForRenderable(const Renderable &renderable) const {
   ShaderContract position_contract(ShaderAttribute::Position, 3);
   ShaderContract normal_contract(ShaderAttribute::Normal, 3);
   ShaderContract texcoord_contract(ShaderAttribute::TextureCoordinate, 2);
   ShaderContract boneIndices_contract(ShaderAttribute::BoneIndices, 4);
   ShaderContract boneWeights_contract(ShaderAttribute::BoneWeights, 4);

   AttributeInfo position_info;
   AttributeInfo normal_info;
   AttributeInfo texcoord_info;
   AttributeInfo boneIndices_info;
   AttributeInfo boneWeights_info;

   bool has_position = renderable.getAttributeInfo(position_contract, position_info);
   bool has_normal = renderable.getAttributeInfo(normal_contract, normal_info);
   bool has_texcoord = renderable.getAttributeInfo(texcoord_contract, texcoord_info);
   bool has_boneIndices = renderable.getAttributeInfo(boneIndices_contract, boneIndices_info);
   bool has_boneWeights = renderable.getAttributeInfo(boneWeights_contract, boneWeights_info);

   if (!has_position || !has_normal || !has_texcoord || !has_boneIndices || !has_boneWeights) {
      throw std::runtime_error("SkinShader: missing either position, normal, texcoord, bone indices or bone weights.");
   }
   
   gl::vertex_array vao;
   gl::attribute position = meshProgram.get_attribute("position");
   bind_contract(vao, position, position_contract, position_info);
   
   gl::attribute normal = meshProgram.get_attribute("normal");
   bind_contract(vao, normal, normal_contract, normal_info);
   
   gl::attribute tex_coord = meshProgram.get_attribute("tex_coord");
   bind_contract(vao, tex_coord, texcoord_contract, texcoord_info);

   gl::attribute boneIndices = meshProgram.get_attribute("boneIndices");
   bind_contract(vao, boneIndices, boneIndices_contract, boneIndices_info);

   gl::attribute boneWeights = meshProgram.get_attribute("boneWeights");
   bind_contract(vao, boneWeights, boneWeights_contract, boneWeights_info);

   return vao;
}

void SkinShader::setModel(const glm::mat4 &modelm) {
   gl::uniform model = meshProgram.get_uniform("model");
   meshProgram.set_uniform_mat4(model, glm::value_ptr(modelm));
}

void SkinShader::setView(const glm::mat4 &viewm) {
   gl::uniform view = meshProgram.get_uniform("view");
   meshProgram.set_uniform_mat4(view, glm::value_ptr(viewm));
}

void SkinShader::setProjection(const glm::mat4 &proj) {
   gl::uniform projection = meshProgram.get_uniform("projection");
   meshProgram.set_uniform_mat4(projection, glm::value_ptr(proj));
}

void SkinShader::setMaterial(const Material &mat) {
   meshProgram.use();
   
   gl::uniform tex = meshProgram.get_uniform("tex");
   
   if (mat.diffuse.hasTexture) {
      meshProgram.set_uniform(tex, 0);
      mat.diffuse.texture.bind(0);
   }
}

void SkinShader::setBones(const vector<glm::mat4>& bones) {
	meshProgram.use();

	for (int i = 0; i < bones.size(); i++) {
		std::string index;
		std::ostringstream convert;
		convert << i; 
		index = convert.str();

		gl::uniform bone = meshProgram.get_uniform("bones[" + index + "]");
		meshProgram.set_uniform_mat4(bone, glm::value_ptr(bones[i]));
	}
}

void SkinShader::use() {
   meshProgram.use();
}
