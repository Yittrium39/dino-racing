// One Sentence Description:
// The Application class creates a window and manages the lifecycles of
// the different controllers or "screens" of the application.
// Design details here:
// https://bitbucket.org/Yittrium39/dino-racing/wiki/Design

#pragma once

#ifdef _WIN32
#define GLEW_STATIC
#endif

#include "config.h"
#include "Controller.h"

#include <stack>
#include <memory>
#include <chrono>

class Application : public gl::window {
	std::stack<std::shared_ptr<Controller> > mControllers;
   
   typedef std::chrono::high_resolution_clock Timer;
   typedef std::chrono::time_point<Timer> Time;
   typedef std::chrono::duration<float> Seconds;
   
   Time mCurrentTime;
   
public:
	Application();
	~Application() { }

	void draw();
   void update();
   void event(gl::event &e);
};