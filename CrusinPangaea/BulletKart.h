
#pragma once

#include "World.h"

#include <map>
#include <string>
#include <btBulletDynamicsCommon.h>
#include <BulletDynamics/Vehicle/btRaycastVehicle.h>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

class btVehicleTuning;
struct btVehicleRaycaster;
class btCollisionShape;


namespace physics {

class BulletKart : public Kart {
public:
	BulletKart();
	~BulletKart();
   
   bool accelerating, braking, turningLeft, turningRight, boost, powerUpBoost, jump, drop, forward;
   virtual void setAccelerating(bool a, bool forward) {
	   accelerating = a;
	   if (forward)
	      this->forward = true;
	   else
	      this->forward = false;
   }
   virtual void setBraking(bool a) { braking = a; }
   virtual void setTurningLeft(bool a) { turningLeft = a; }
   virtual void setTurningRight(bool a) { turningRight = a; }
   virtual void setBoost(bool a) { boost = a; }
   virtual void setPowerUpBoost(bool a) { powerUpBoost = a; }
   virtual void setJump(bool a) { jump = a; }
   virtual void setDrop(bool a) { drop = a; }


   virtual void update();

   bool getBraking() const;
   bool getAccelerating() const;
   bool getPowerUpBoost() const;
   bool checkReverse() const;
   float getSpeed() const;

	void setStartingLocation(glm::vec3 location);
	void applyBrakingForce(bool which);
	void applyAccelerationForce(bool which);
	void applySteeringRight(bool which);
	void applySteeringLeft(bool which);
	void applyReverseAcceleration(bool apply);
	void toggleReverse();
    void applyPowerUpBoost(bool enabled);
    void applyJump(bool enabled);
    void applyDrop(bool enabled);




	void resetScene();
	void renderKart();
	void renderKartModel();
	glm::mat4 getModelTransform();
	glm::mat4 getViewTransform();

   glm::vec3 getPosition() const;
   glm::quat getRotation() const;
   void setPosition(const glm::vec3 &position);
   void setRotation(const glm::quat &rotation);

	btDefaultMotionState* getMotionState() { return vehicleMotionState; }
	btRigidBody* getVehicleRigidBody() { return kartChassis; }
	btVehicleRaycaster* getRaycaster() { return kartRaycaster; }
	btRaycastVehicle* getRaycast() { return kartVehicle; }

	float getWheelRadius() { return wheelRadius; }
	float getWheelWidth() { return wheelWidth; }
	btScalar getSuspensionRestLength() { return suspensionRestLength; }
	float getSuspensionStiffness() { return suspensionStiffness; }
	float getSuspensionDamping() { return suspensionDamping; }
	float getSuspensionCompression() { return suspensionCompression; }
	float getWheelFriction() { return wheelFriction; }
	float getRollInfluence() { return rollInfluence; }

	btRaycastVehicle::btVehicleTuning kartTuning;
	btVehicleRaycaster* kartRaycaster;
	btRaycastVehicle* kartVehicle;
private:
	// bullet stuff
	static btCollisionShape* kartCollisionShape;
	btDefaultMotionState* vehicleMotionState;
	btRigidBody* kartChassis;
	btAlignedObjectArray<btCollisionShape*> collisionShapes;

	// kart model stuff
	std::string kartName;
	float kartDimensions[3];
	btVector3 cameraPosition;

	// wheel IDs
	int wheelFrontLeft_ID;
	int wheelFrontRight_ID;
	int wheelBackLeft_ID;
	int wheelBackRight_ID;
	glm::vec3 startPoint;
	glm::vec4 startOrientation;

	// kart properties
	float gVehicleSteering;
	float steeringIncrement;
	float steeringClamp;

	float gEngineForce;
	float gBreakingForce;
	float maxEngineForce;
	float maxBreakingForce;

	float wheelRadius;
	float wheelWidth;
	btScalar suspensionRestLength;

	// wheel properties
	float suspensionStiffness;
	float suspensionDamping;
	float suspensionCompression;
	float wheelFriction;
	float rollInfluence;

	// steering properties
	bool steerRight;
	bool steerLeft;
	bool isSteering;
	bool isReverse;
    
    btVector3 savedSpeed;
};

}