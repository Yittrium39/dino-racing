//
//  GBuffer2.cpp
//  CrusinPangaea
//
//  Created by Richie on 2/27/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#include "GBuffer.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <sstream>

/*
 * Create the textures and depth buffer that we will render into
 */
GBuffer::GBuffer(int width, int height) {
   GLint restoreId;
   glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &restoreId);

   // Create FBO
   factory().create(framebuffer, glGenFramebuffers, glDeleteFramebuffers);
   glBindFramebuffer(GL_DRAW_FRAMEBUFFER, framebuffer);
         
   // Create texture
   texColor.Image2D(0, gl::DataType::Float, gl::Format::RGBA, width, height, gl::InternalFormat::RGB32F);
   texNormal.Image2D(0, gl::DataType::Float, gl::Format::RGBA, width, height, gl::InternalFormat::RGB32F);
   texPosition.Image2D(0, gl::DataType::Float, gl::Format::RGBA, width, height, gl::InternalFormat::RGB32F);

   texColor.SetWrapping(gl::Wrapping::ClampEdge, gl::Wrapping::ClampEdge);
   texNormal.SetWrapping(gl::Wrapping::ClampEdge, gl::Wrapping::ClampEdge);
   texPosition.SetWrapping(gl::Wrapping::ClampEdge, gl::Wrapping::ClampEdge);

   texColor.SetFilters(gl::Filter::Nearest, gl::Filter::Nearest);
   texNormal.SetFilters(gl::Filter::Nearest, gl::Filter::Nearest);
   texPosition.SetFilters(gl::Filter::Nearest, gl::Filter::Nearest);

   glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texColor, 0);
   glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, texNormal, 0);
   glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, texPosition, 0);
         
   // Create renderbuffer to hold depth buffer
   glBindTexture(GL_TEXTURE_2D, texDepth);
   glTexImage2D(GL_TEXTURE_2D, 0, gl::InternalFormat::DepthComponent32F, width, height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);
   texDepth.SetWrapping(gl::Wrapping::ClampEdge, gl::Wrapping::ClampEdge);
   texDepth.SetFilters(gl::Filter::Nearest, gl::Filter::Nearest);
   glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, texDepth, 0);

   // Check
   if (glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
      throw std::runtime_error("Unable to create framebuffer");
   
   factory().create(shadowbuffer, glGenFramebuffers, glDeleteFramebuffers);
   glBindFramebuffer(GL_DRAW_FRAMEBUFFER, shadowbuffer);
   
   // Create renderbuffer to hold shadow buffer
   glBindTexture(GL_TEXTURE_2D, texShadow);
   glTexImage2D(GL_TEXTURE_2D, 0, gl::InternalFormat::DepthComponent32F, width, height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);
   texShadow.SetWrapping(gl::Wrapping::ClampEdge, gl::Wrapping::ClampEdge);
   texShadow.SetFilters(gl::Filter::Linear, gl::Filter::Linear);
   glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, texShadow, 0);

#ifdef _WIN32 // need a framebuffer with color attachments on AMD GPU's
   gl::texture nullTex;
   nullTex.Image2D(0, gl::DataType::Float, gl::Format::RGB, width, height, gl::InternalFormat::RGB16F);
   nullTex.SetFilters(gl::Filter::Nearest, gl::Filter::Nearest);
   glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, nullTex, 0);
#endif

   // Check
   if (glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
      throw std::runtime_error("Unable to create shadowbuffer");
   
   glBindFramebuffer(GL_DRAW_FRAMEBUFFER, restoreId);
   
   getQuadVAO();
   
   restoreFB = -1;
}

void GBuffer::begin() {
   if (restoreFB != -1) {
      throw std::runtime_error("Must call end before beginning again.");
   }
   glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &restoreFB);
   
   glBindFramebuffer(GL_DRAW_FRAMEBUFFER, framebuffer);
   
   GLenum buffers[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2};
   glDrawBuffers(3, buffers);
}

void GBuffer::end() {
   if (restoreFB == -1) {
      throw std::runtime_error("Must call begin before end.");
   }
   glFinish();
   glBindFramebuffer(GL_DRAW_FRAMEBUFFER, restoreFB);
   restoreFB = -1;
}

void GBuffer::begin_shadow() {
   if (restoreFB != -1) {
      throw std::runtime_error("Must call end before beginning again.");
   }
   glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &restoreFB);
   
   glBindFramebuffer(GL_DRAW_FRAMEBUFFER, shadowbuffer);
}

void GBuffer::end_shadow() {
   end();
}

void GBuffer::draw(float depth) {
   float z = depth;
   getProgram().use();
   getProgram().set_uniform(getProgram().get_uniform("depth"), z);
   texColor.bind(0);
   texNormal.bind(1);
   texPosition.bind(2);
   texShadow.bind(3);
   getQuadVAO().draw_arrays(gl::primitive::triangle_fan, 0, 4);
}

void GBuffer::draw_shadow(float depth) {
   float z = depth;
   getDepthProgram().use();
   getDepthProgram().set_uniform(getDepthProgram().get_uniform("depth"), z);
   texShadow.bind(0);
   getQuadVAO().draw_arrays(gl::primitive::triangle_fan, 0, 4);
}

void GBuffer::setEye(const glm::vec3 &eyePos) {
	getProgram().use();
	gl::uniform eye = getProgram().get_uniform("eyePos");
	getProgram().set_uniform_vec3(eye, glm::value_ptr(eyePos));
}

void GBuffer::addLight(const glm::vec3 &lightPosition, const glm::vec3 &lightIntensity) {
	PointLight light;
	light.position = lightPosition;
	light.intensity = lightIntensity;

	myLights.push_back(light);
}

void GBuffer::setLights() {
	getProgram().use();

	for (int i = 0; i < myLights.size(); i++) {
		std::string index;
		std::ostringstream convert;
		convert << i; 
		index = convert.str();

		gl::uniform position = getProgram().get_uniform("light[" + index + "].position");
		gl::uniform intensity = getProgram().get_uniform("light[" + index + "].intensity");

		getProgram().set_uniform_vec3(position, glm::value_ptr(myLights[i].position));
		getProgram().set_uniform_vec3(intensity, glm::value_ptr(myLights[i].intensity));
	}
}

void GBuffer::setLight(const glm::vec3& lightPosition, const glm::vec3& lightIntensity) {
	getProgram().use();

	gl::uniform position = getProgram().get_uniform("light.position");
	gl::uniform intensity = getProgram().get_uniform("light.intensity");

	getProgram().set_uniform_vec3(position, glm::value_ptr(lightPosition));
	getProgram().set_uniform_vec3(intensity, glm::value_ptr(lightIntensity));
}

void GBuffer::setSpot(const glm::mat4 &spot) {
   getProgram().use();
   gl::uniform spot_u = getProgram().get_uniform("spot");
   getProgram().set_uniform_mat4(spot_u, glm::value_ptr(spot));
}

/***********
 * SHADERS *
 ***********/

/**
 * getVertexShader
 * makes a default vertex shader
 */
gl::shader &GBuffer::getVertexShader() {
   static gl::shader shader = gl::shader(gl::shader_type::vertex, GLSL(
      in vec2 aPosition;
      out vec2 vPosition;
      uniform float depth;
      void main() {
         vPosition = (aPosition + 1.0) / 2.0;
         gl_Position = vec4(aPosition, depth, 1.0);
      }
   ));
   return shader;
 }

/**
 * getFragmentShader
 * makes a default fragment shader
 */
gl::shader &GBuffer::getFragmentShader() {
   static gl::shader shader = gl::shader(gl::shader_type::fragment, GLSL(
      
      in vec2 vPosition;
      out vec4 fcolor;
      uniform sampler2D col;
      uniform sampler2D norm;
      uniform sampler2D pos;
      uniform sampler2D shadow;
      uniform mat4 spot;
      /*
      void main() {
         vec4 rgba = texture(col, vPosition);
         vec3 normal = texture(norm, vPosition).xyz;
         vec3 pos = texture(pos, vPosition).xyz;
         outColor = rgba + vec4(pos, 0.0);
      }
      */
		uniform vec3 eyePos;

		struct LightInfo {
			vec3 position;
			vec3 intensity;
		};

		uniform LightInfo light;

		vec3 cookTorrance(vec3 pos, vec3 norm, vec3 diff) {	
			float distance = length(light.position - pos);
			float coeff = 0.0f;
			float attenuation = 0.0f;
			float lightRadius = 1000.0f;
			float cutoff = 0.05f;
	
			coeff = max(distance - lightRadius, 0.0f) / lightRadius + 1.0f;
			// attenuation = max((1.0f / (coeff * coeff) - cutoff) / (1.0f - cutoff), 0.0);
			// attenuation = (lightRadius - distance) / (lightRadius * 0.01f) * 0.01f;

			// if (attenuation > 0.0f) {
				vec3 viewDir = normalize(eyePos);
				vec3 lightDir = normalize(vec3(light.position) - pos);
				float roughnessValue = 0.2f;

				vec3  halfVector = normalize(viewDir + lightDir);
				float VdotH 	= max(0.0f, dot(viewDir, halfVector));
				float NdotH 	= max(0.0f, dot(norm, halfVector));
				float NdotL 	= max(0.0f, dot(norm, lightDir));
				float NdotV 	= max(0.0f, dot(norm, viewDir));
				float sNdotH	= sqrt(1.0f - NdotH * NdotH);

				// schlick approximation for fresnel
				// kelemen and szirmau-kalos apprixmation for geometric term
				float R0		= 0.1f;
				float R			= R0 + (1.0f - R0) * pow(1.0f - VdotH, 5.0f);
				float G			= min(1.0f, min(2.0f * NdotH * NdotV / VdotH, 2.0f * NdotH * NdotL / VdotH));
				float M			= max(0.0f, R * G / (NdotL * NdotV));

				// beckmann DF
				float kappa		= sNdotH /(NdotH * roughnessValue);
				float D			= max(0.0f, 1.0f / (3.141592654f * roughnessValue * roughnessValue * pow(NdotH, 4.0f)) * exp(-kappa * kappa));

				float sRadiance	= M * D;
				float dRadiance = NdotL;
				vec3 luminance = (diff * 0.5) + light.intensity * ((dRadiance * diff) + (vec3(0.5, 0.5, 0.5) * sRadiance));
		
				return luminance;
			// }
			
			// return vec3(0.0f);
		}

		vec3 addAttenuatedLight(vec3 pos, vec3 norm, vec3 diff, vec3 lightPosition, vec3 lightIntensity) {	
			float distance = length(lightPosition - pos);
			float coeff = 0.0f;
			float attenuation = 0.0f;
			float lightRadius = 25.0f;
			float cutoff = 0.1f;
	
			coeff = max(distance - lightRadius, 0.0f) / lightRadius + 1.0f;
			attenuation = (lightRadius - distance) / (lightRadius * 0.01f) * 0.01f;

			if (attenuation > 0.0f) {
				vec3 lightDir = normalize(vec3(lightPosition) - pos);

				float NdotL 	= max(0.0f, dot(norm, lightDir));

				vec3 luminance = lightIntensity * (NdotL * diff) * 2.0;
		
				return clamp(luminance * attenuation, vec3(0.0), vec3(1.0));
			}
			
			return vec3(0.0f);
		}

		void main() {
			vec3 position = vec3(texture(pos, vPosition));
			vec3 normal = vec3(texture(norm, vPosition));
			vec3 diffColor = vec3(texture(col, vPosition));

			vec3 sumColor = vec3(0.0);
			sumColor += cookTorrance(position, normal, diffColor);
			sumColor += addAttenuatedLight(position, normal, diffColor, vec3(5, -8, -162), vec3(0.0, 1.0, 0.0));
			sumColor += addAttenuatedLight(position, normal, diffColor, vec3(-141, -12, 69), vec3(0.0, 1.0, 0.0));
			sumColor += addAttenuatedLight(position, normal, diffColor, vec3(-201, -13, 15), vec3(0.0, 1.0, 0.0));
			sumColor += addAttenuatedLight(position, normal, diffColor, vec3(-474, -11, 182), vec3(0.0, 1.0, 0.0));
			sumColor += addAttenuatedLight(position, normal, diffColor, vec3(-600, 20, 250), vec3(0.0, 1.0, 0.0));
			sumColor += addAttenuatedLight(position, normal, diffColor, vec3(-550, 46, -152), vec3(0.0, 1.0, 0.0));
			sumColor += addAttenuatedLight(position, normal, diffColor, vec3(-40, 37, 301), vec3(0.0, 1.0, 0.0));

			sumColor += addAttenuatedLight(position, normal, diffColor, vec3(-72,-8,-124), vec3(1.0, 0.0, 1.0));
			sumColor += addAttenuatedLight(position, normal, diffColor, vec3(-159, -12, 35), vec3(1.0, 0.0, 1.0));
			sumColor += addAttenuatedLight(position, normal, diffColor, vec3(-569, 31, 146), vec3(1.0, 0.0, 1.0));
			sumColor += addAttenuatedLight(position, normal, diffColor, vec3(-373, 53, -271), vec3(1.0, 0.0, 1.0));
			sumColor += addAttenuatedLight(position, normal, diffColor, vec3(-115, 29, 120), vec3(1.0, 0.0, 1.0));
			sumColor += addAttenuatedLight(position, normal, diffColor, vec3(-17, 13, 199), vec3(1.0, 0.0, 1.0));

			sumColor = clamp(sumColor, vec3(0.0), vec3(1.0));
         
         vec4 shadowpos = spot * vec4(position, 1.0); // this goes to projection space
                  
         vec2 lookup_pos = shadowpos.xy / shadowpos.z; // this goes to clip space
         lookup_pos = (lookup_pos + 1) / 2;  // this goes to texture space
         float light_depth = texture(shadow, lookup_pos).r; // depth between 0 and 1
         float dist_from_light = shadowpos.z / shadowpos.w; // depth between -1 and 1 (projection space to clip space)
         dist_from_light = (dist_from_light + 1.0) / 2.0; // converts from clip space of light to texture space
         
         vec3 shadow_color = vec3(0.0);
         if (lookup_pos.x >= 0 &&
             lookup_pos.y >= 0 &&
             lookup_pos.x <= 1 &&
             lookup_pos.y <= 1) {
            
            if ( dist_from_light - light_depth  > 0.00001 ) {
               // Using distance from light for shadow attenuation, not distance from occluder
               float brightness = clamp(200 * (1 - dist_from_light), 0, 1);
               shadow_color = vec3(-brightness);
            } else {
               //float brightness = clamp(200 * (1 - dist_from_light), 0, 1);
               //shadow_color = vec3(0.3);
            }
         }
      
         
			fcolor = vec4(sumColor + shadow_color, 1.0);
		}
   ));
   return shader;
}


/**
 * getVertexShader
 * makes a depth vertex shader
 */
gl::shader &GBuffer::getDepthVertexShader() {
   static gl::shader shader = gl::shader(gl::shader_type::vertex, GLSL(
      in vec2 aPosition;
      out vec2 vPosition;
      uniform float depth;
      void main() {
         vPosition = (aPosition + 1.0) / 2.0;
         gl_Position = vec4(aPosition, depth, 1.0);
      }
   ));
   return shader;
 }

/**
 * getFragmentShader
 * makes a depth fragment shader
 */
gl::shader &GBuffer::getDepthFragmentShader() {
   static gl::shader shader = gl::shader(gl::shader_type::fragment, GLSL(
      
      in vec2 vPosition;
      out vec4 fcolor;
      uniform sampler2D depth_map;

		void main() {
			vec3 diffColor = vec3(texture(depth_map, vPosition));
         diffColor -= vec3(0.99);
         diffColor *= 100;
			fcolor = vec4(diffColor, 1.0);
		}
   ));
   return shader;
}

/****************************************
 * PRIVATE SINGLETON RESOURCE ACCESSORS *
 ****************************************
 * We have these so we can access the static shaders. The reason each
 * of the static shaders is not a member of the class is because we need
 * to initialize them after we have an OpenGL context, and we shouldn't
 * initialize them if we never end up using them.
 */
 

/**
 * getProgram
 * gets a plain program
 */
gl::program &GBuffer::getProgram() {
   static gl::program program = makeProgram();
   return program;
}

/**
 * getDepthProgram
 * gets a program that renders the depth
 */
gl::program &GBuffer::getDepthProgram() {
   static gl::program program = makeDepthProgram();
   return program;
}

/**
 * getQuadVAO
 * this is the accessor for a lazily initialized static quad VAO
 */
gl::vertex_array &GBuffer::getQuadVAO() {
   static gl::vertex_array vao;
   return vao;
}

/**
 * getQuadVBO
 * this is the accessor for a lazily initialized static quad VBO
 */
gl::vertex_buffer &GBuffer::getQuadVBO() {
   static gl::vertex_buffer vbo = makeQuadVBO();
   return vbo;
}

/****************************
 * LAZY STATIC INITIALIZERS *
 ****************************/

/**
 * makes a default program
 */
gl::program GBuffer::makeProgram() {
   gl::program program = gl::program(getVertexShader(), getFragmentShader());
   getQuadVAO().bind_attribute(program.get_attribute("aPosition"), getQuadVBO(), 2, gl::type::Float, 0, 0 );
   program.set_uniform(program.get_uniform("col"), 0);
   program.set_uniform(program.get_uniform("norm"), 1);
   program.set_uniform(program.get_uniform("pos"), 2);
   program.set_uniform(program.get_uniform("shadow"), 3);
   return program;
}

/**
 * makes a program that renders the depth
 */
gl::program GBuffer::makeDepthProgram() {
   gl::program program = gl::program(getDepthVertexShader(), getDepthFragmentShader());
   getQuadVAO().bind_attribute(program.get_attribute("aPosition"), getQuadVBO(), 2, gl::type::Float, 0, 0 );
   program.set_uniform(program.get_uniform("depth_map"), 0);
   return program;
}

gl::vertex_buffer GBuffer::makeQuadVBO() {
   float vertices[] = { -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, -1.0f, -1.0f, -1.0f};
   gl::vertex_buffer vbo(vertices, sizeof(vertices), gl::buffer_usage::static_draw);
   return vbo;
}

gl::texture GBuffer::getColorTex() {
	return texColor;
}

gl::texture GBuffer::getPosTex() {
	return texPosition;
}
