//
//  Particles.cpp
//  CrusinPangaea
//
//  Created by Robert Burton on 2/25/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#include "Particles.h"
#include <iostream>

void Particles::update(float deltaT, glm::vec3 kartPosition, glm::vec3 kartDirection){
    
    //1. Generate new Particles for karts (could also be done elseware)
    /*glm::vec3 newKartDirection = -kartDirection;
    for(int i=0; i<2; i++)
    {
    addParticle(glm::vec3(kartPosition.x, kartPosition.y+0.5, kartPosition.z),
                glm::normalize(glm::vec3(-newKartDirection.x+(rand()%100 - 50)/150.0,
                                                  0+(rand()%100 - 50)/150.0,
                                                  newKartDirection.z+(rand()%100 - 50)/150.0)));
    }*/
    
    //((rand()%10 + 20)/100.0)
    /*particles[newP].location = kart[0].location + (float)((rand()%500 + 300)/1000.0)*kart[0].direction;
    particles[newP].velocity = glm::normalize(vec3(kart[0].direction.x+(rand()%100 - 50)/150.0,
                                                   kart[0].direction.y+(rand()%100 - 50)/150.0,
                                                   kart[0].direction.z+(rand()%100 - 50)/150.0));//direction
    particles[newP].velocity *= (rand()%10 + 20)/100.0;//magnitude
    */
    
    //2. Kill any particles at end of life
    for(int j=0; j < particles.size(); j++)
    {
        if(particles[j].life == 30)
        {
            //kill particle
            particles[j].active = 0;
        }
    }    
    
    //3. Update alive particles
    for(int j=0; j< particles.size(); j++)
    {
        if(particles[j].active == 1)
        {
            //update particle
            
            /* acceleration = Force / mass
             * all particles have mass=1, so effictively,
             * total acceleation = sum of forces.*/
            glm::vec3 acceleration = glm::vec3(0,0,0);
            
            //upward force
            acceleration.y +=  9.8*0.003;
            
            //drag/decay force
            acceleration += -((float)0.05)*particles[j].velocity;
            
            //update velocity based on forces, save old velocity
            glm::vec3 oldVelocity = particles[j].velocity;
            particles[j].velocity += acceleration * (float)(deltaT);
            
            //updates location using simple midpoint method
            particles[j].location += ((float)0.5)*(particles[j].velocity + oldVelocity)* (float)(deltaT);
            
            //color update done based on life
            particles[j].life++;
        }
    }
}

void Particles::addParticle(glm::vec3 location, glm::vec3 velocity){
    //add a particle here
    Particle newP = {location, velocity, glm::vec3(0.5,0.5,0.5), 1, 0};
    particles.push_back(newP);

    //also, could add: if any (active == 0) are found, just overwrite that particle
    /*for (int i=0; i< particles.size(); i++) {
        if(particles[i].active == 0)
        {
            particles[i] = newP;
            break;
        }
        //if no dead particles found, add a new one
        particles.push_back(newP);
    }*/
}

void Particles::createExplosion(glm::vec3 location){
    
    for(int i = 0; i< 120; i++)
    {
        glm::vec3 velocity = glm::normalize(glm::vec3(rand()%100 - 50,rand()%100-50,rand()%100 - 50));
        velocity *= (rand()%10 + 20)/5.0;//magnitude
        addParticle(location, velocity );
    }
}

glm::vec3 Particles::getColor(int life){
    
    glm::vec3 white/*yellow*/ = glm::vec3(0.5, 0.7, 0.1);
    glm::vec3 blue = glm::vec3(0.3, 0.3, 0.9);
    glm::vec3 red = glm::vec3(0.9, 0.1, 0.1);
    //glm::vec3 yellow = glm::vec3(0.4, 0.4, 0.0);
    glm::vec3 black/*green*/ = glm::vec3(0.5, 0.7, 0.1);
    
    glm::vec3 color = glm::vec3(0.1, 0.1, 0.1);
    
    float changePoint1 = 0;//min life
    float changePoint2 = 10;
    float changePoint3 = 20;
    float changePoint4 = 30;//max life

    
    if(life >= changePoint1 && life < changePoint2)
    {
        color.x = (1.0-(life-changePoint1)/(changePoint2-changePoint1))*white.x + ((life-changePoint1)/(changePoint2-changePoint1))*blue.x;
        color.y = (1.0-(life-changePoint1)/(changePoint2-changePoint1))*white.y + ((life-changePoint1)/(changePoint2-changePoint1))*blue.y;
        color.z = (1.0-(life-changePoint1)/(changePoint2-changePoint1))*white.z + ((life-changePoint1)/(changePoint2-changePoint1))*blue.z;
    }
    if(life >= changePoint2 && life < changePoint3)
    {
        color.x = (1.0-(life-changePoint2)/(changePoint3-changePoint2))*blue.x + ((life-changePoint2)/(changePoint3-changePoint2))*red.x;
        color.y = (1.0-(life-changePoint2)/(changePoint3-changePoint2))*blue.y + ((life-changePoint2)/(changePoint3-changePoint2))*red.y;
        color.z = (1.0-(life-changePoint2)/(changePoint3-changePoint2))*blue.z + ((life-changePoint2)/(changePoint3-changePoint2))*red.z;
    }
    if(life >= changePoint3 && life <= changePoint4)
    {
        color.x = (1.0-(life-changePoint3)/(changePoint4-changePoint3))*red.x + ((life-changePoint3)/(changePoint4-changePoint3))*black.x;
        color.y = (1.0-(life-changePoint3)/(changePoint4-changePoint3))*red.y + ((life-changePoint3)/(changePoint4-changePoint3))*black.y;
        color.z = (1.0-(life-changePoint3)/(changePoint4-changePoint3))*red.z + ((life-changePoint3)/(changePoint4-changePoint3))*black.z;
    }
    
    return color;
    
}
