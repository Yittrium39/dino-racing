
#pragma once

#include "VertexArray.hpp"
#include "Attribute.h"
#include "Material.h"

#include <glm/glm.hpp>
#include <vector>

class SkinMesh;

// Data type that specifies whether or not an object should
// be drawn using an index buffer object
namespace DrawType {
   enum draw_type_t { elements, arrays };
}

// This is a struct used by the renderer to determine which OpenGL call
// should be used to draw the object
struct DrawInfo {
   bool isSkinned;
   gl::primitive::primitive_t mode;
   unsigned int num_elements;
   DrawType::draw_type_t draw_type;
   gl::vertex_buffer ibo;        // ignored if draw_type is arrays
   gl::type::type_t index_type;  // ignored if draw_type is arrays
};


/* This is a class of objects that can describe to a renderer what
 * data they have and how it is layed out. */
class Renderable {

   // The ID can be used by the renderer to cache information about the renderable
   // Assignment of IDs is currently not thread-safe!
   unsigned long ID;
   unsigned long nextId() { static unsigned long ID = 0; return ++ID; }
   
   // The transparency flag may be set so the renderer knows whether or not the object
   // requires depth sorting
   bool mHasTransparency;
   
   public:
   Renderable() : mHasTransparency(false), ID(nextId()) { }
   Renderable(bool t) : mHasTransparency(t), ID(nextId()) { }
   
   bool hasTransparency() const { return mHasTransparency; }
   unsigned long getID() const { return ID; }
   
   // getAttributeInfo is called by a renderer to figure out where data is stored.
   // The renderer sends a ShaderContract to the renderable with a request for where
   // vertex position data is stored. The renderable then populates the AttributeInfo
   // object with what VBO it is stored in along with the offset and the stride.
   //
   // The ShaderContract variable also specifies what kind of data it is expecting (i.e.
   // float, int, etc.) and how many of those objects it is expecting (so for position it
   // should pretty much always be 3.) The ShaderContract can also ask for normals,
   // texture coords, color, etc. If the renderable does not have a location for the data
   // or the data it has does not match the format the renderer expects it must return
   // false. Otherwise, it must specify a value for every field of AttributeInfo and return
   // true.
   virtual bool getAttributeInfo(const ShaderContract &c, AttributeInfo &info) const = 0;
   
   // getDrawInfo is called by a renderer to determine how the vertex data is stored. For
   // example, if the data is stored as triangle strips using an index buffer object the
   // renderable must return a primitive type of gl::primitive::triangle_strip and draw type
   // of DrawType::elements
   virtual DrawInfo getDrawInfo() const = 0;
   
   // getMaterial is called by a renderer to determine which textures it needs to load in
   // order to draw the object
   virtual Material getMaterial() const = 0;
   virtual Material &getMaterial() { return getMaterial(); }
   
   // TODO: implement rendering engine component for this piece
   // getBoneTransforms is called by the renderer to get the current transformations of any
   // bones in the model. By default this is unimplemented since most meshes are not skinned
   // meshes.
   // Classes that implement this mush do the following:
   // For each bone that transforms the mesh, add the bone to the bones array in the order
   // of the weights in the VBO. For example, if each vertex in the vbo two weights, and the
   // first weight is for bone 11, and the second weight is for bone 3, then add the transformation
   // for bone 11 in bones[0] and the transformation for bone 3 in bones[1].
   // In this example, numBones should equal 2, but it may also be less than two. If you have more
   // bones than are specified in numBones, you may either return false, or add the first
   // few bone transformations (but in general numBones should be equal to the number of bone
   // transforms you have.)
   virtual bool getBoneTransforms(float TimeInSeconds, std::vector<glm::mat4>& Transforms) { return false; }
   
   // TODO: change the return type and implement this
   // This function will get the bounding sphere for a renderable so we can test each frame whether
   // or not it is in the view frustum
   typedef void BoundingSphere;
   virtual BoundingSphere getBoundingSphere() { };

   SkinMesh *currentSkinMesh;
};