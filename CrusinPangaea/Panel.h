//
//  Panel.h
//  CrusinPangaea
//
//  Created by Richie on 2/18/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#pragma once

#ifdef _WIN32
#define GLEW_STATIC
#endif

#include <glm/glm.hpp>
#include "Shader.hpp"
#include "Program.hpp"
#include "Framebuffer.hpp"
#include "VertexArray.hpp"

class Panel {
   enum { MAX_DEPTH = 1000 };
   
   // Vertex Shaders
   static gl::shader &getPanelVertexShader();
   static gl::shader &getTransformPanelVertexShader();
   static gl::shader &getBlurPanelVertexShader();

   // Fragment Shaders
   static gl::shader &getPanelFragmentShader();
   static gl::shader &getBlurPanelFragmentShader() ;

   // private singleton resource accessors
   static gl::program &getPanelProgram();
   static gl::program &getTransformPanelProgram();
   static gl::program &getBlurPanelProgram();

   static gl::vertex_array &getPanelVAO();
   static gl::vertex_buffer &getPanelVBO();

   // static initializers
   static gl::vertex_array makePanelVAO(gl::vertex_buffer &vbo);
   static gl::vertex_buffer makePanelVBO();
   static gl::program makePanelProgram();
   static gl::program makeTransformPanelProgram();
   static gl::program makeBlurPanelProgram();

   gl::framebuffer panelBuffer;
   
public:
   Panel(int width, int height) : panelBuffer(width,height) { getPanelVAO(); }
   void begin();
   void end();
   void draw(int depth);
   
   static void drawTexture(gl::texture &tx, int depth);
   static void drawMotionBlur(const gl::texture &finalTex, const gl::texture &posTex, const glm::mat4 &prevViewProjMatrix, const glm::mat4& viewProjMatrix, bool blur);

   gl::framebuffer &getFramebuffer();
};