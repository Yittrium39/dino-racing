#pragma once

#include "Program.hpp"
#include "VertexArray.hpp"
#include "Renderable.h"
#include "Material.h"

#include "Camera.h"
#include "SceneObject.h"

#include "SceneShader.h"
#include "TextureShader.h"
#include "FlatShader.h"
#include "SkinShader.h"

#include "GBuffer.h"

#include <map>
#include <set>
#include <glm/glm.hpp>

class SkinMesh;

// SceneObjectShader consolidates information contained in a Renderable and in
// the associated material. The Renderable is pre-processed and converted into
// a SceneObjectShader the first time it is rendered.
//
// This structure contains enough information to render an item almost by itself.
// The only additional information needed would be stuff like model/view/proj
// and bone transforms.
//
// Specifically the data in the structure is:
//  * program: a description of how to render the data         (SceneShader.hpp)
//  * vao: a description of where the data is                  (VertexArray.hpp)
//  * info: a description of how OpenGL should run the program (Renderable.h)
//  * material: textures and colors of the object              (Material.h)
struct SceneObjectShader {
   SceneShader *program;
   gl::vertex_array vao;
   DrawInfo info;
   Material material;
   bool hasTransparency;
   SceneObjectShader() : vao("scene object") { }
};

// This structure represents a single task to be performed by the renderer for one
// frame. The point of this is they can be sorted by any of our rendering properties.
// Additionally, we can group rendering tasks together based on the same material or
// the same program.
struct RenderTask {
   int ID;
   static int makeID() { static int ID = 0; return ID++; }
   RenderTask() : ID(makeID()) { }
   SceneObjectShader *object;
   float depth;
   glm::mat4 transpose;
   // TODO: Add bones to this structure
   SkinMesh *currentSkinMesh;

   // This is the ordering for RenderTasks. For this ordering, we first separate
   // transparent objects from non-transparent objects. After that we sort by depth
   // and then by ID (ID ensures they are not equal so they don't get dropped
   // from the set container)
   bool operator <(const RenderTask &o) const {
      if (!object->hasTransparency && o.object->hasTransparency) {
         return true;
      } else if (object->hasTransparency && !o.object->hasTransparency) {
         return false;
      } else if (depth != o.depth) {
         return depth < o.depth;
      }
      return ID > o.ID;
   }
};

class SceneRenderer {
   // This is a mapping from renderable to SceneObjectShader. A renderable contains
   // a lot of information and the information gets consolidated and pre-processed
   // into a SceneObjectShader. The first render call for a renderable, a SceneObjectShader
   // gets created, stored in this map, and used to render the object. For subsequent calls
   // the mapped-to SceneObjectShader is used so all of the preprocessing is done first.
   std::map<unsigned long, SceneObjectShader> mShaders;

   // Here we would have a list of shaders that will render our image, right now we have just
   // one shader since we only have one type of mesh.
   TextureShader textureShader;
   FlatShader flatShader;
   SkinShader skinShader;

   // This is the list of all things we have to render this frame. The ordering of the set
   // is determined by operator< on RenderTask where the smaller element is rendered first
   std::set<RenderTask> mRenderQueue;
   
   // This is set before any tasks are added to the queue. This is used to determine view
   // and projection transforms, to get the frustum for view-frustum culling, and to determine
   // the relative depth of objects so partially transparent objects can be depth sorted
   Camera mCamera;
   
   // This is used to create the shadow map
   Camera mSpotLight;
   
   std::vector<glm::mat4> transforms;

   SceneObjectShader *addShaderForRenderable(const Renderable &renderable);
   void enqueue(const Renderable &r, const glm::mat4 &transpose);
   
   // Set deferred rendering
   bool deferred;
   GBuffer gbuffer;

   public:
   
   // Sets the mCamera variable, described above. This may not be called if there are any
   // objects in the render queue
   void setCamera(const Camera &c);
   
   // Adds RenderTasks for all of the renderables of a SceneObject to the render queue.
   // For each renderable, a SceneObjectShader is created if one does not exist, and a
   // RenderTask is created. The created render task is added to the queue for this frame
   // if it is within the viewing frustum.
   void enqueue(const SceneObject &o);

   bool inFrustum(glm::mat4 MVP, glm::vec3 center, float radius);
   
   void render();
   
   SceneRenderer();

   GBuffer getGBuffer();
};