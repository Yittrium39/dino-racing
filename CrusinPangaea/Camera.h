#pragma once

#include <glm/glm.hpp>

class Camera {
   float mPitch;
   float mYaw;
   glm::vec3 mPosition;
   
public:
   mutable glm::vec3 lookat;

   enum direction_t {
      Up, Down, Left, Right, Forward, Backward
   };
   
   Camera() : mPosition(0,0,0) , mPitch(0), mYaw(90) {}
   
   void move(direction_t dir, float distance);
   void rotate(int x, int y);
   
   void setPosition(const glm::vec3 &pos) { mPosition = pos; }
   void setLookAt(const glm::vec3 &pos) {
      glm::vec3 lookVec = pos - mPosition;
      mYaw = atan2(lookVec.z, lookVec.x) * 180 / 3.14159;
      float yawmag = glm::length(glm::vec2(lookVec.x, lookVec.z));
      mPitch = atan2(lookVec.y, yawmag) * 180 / 3.14159;
      lookat = pos;
   }
   
   const glm::vec3 &getPosition() const { return mPosition; }
   const glm::vec3 &getLookAt() const { return lookat; }
   float getYaw() { return mYaw; }
   float getPitch() { return mPitch; }
   void setYaw(float yaw) { mYaw = yaw; }
   void setPitch(float pitch) { mPitch = pitch; }
   glm::mat4 getViewMatrix() const;
   glm::mat4 getProjMatrix(float aspectRatio) const;
};