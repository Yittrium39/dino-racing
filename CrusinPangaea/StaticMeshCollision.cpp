//
//  StaticMeshCollision.cpp
//  CrusinPangaea
//
//  Created by Richie on 2/19/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#include "StaticMeshCollision.h"

StaticMeshCollision::StaticMeshCollision(const StaticMesh &s) {
   
   vbo = gl::vertex_buffer(&s.vertices[0], s.vertices.size()*sizeof(glm::vec3), gl::buffer_usage::static_draw);
   ibo = gl::vertex_buffer(&s.indices[0], s.indices.size()*sizeof(unsigned int), gl::buffer_usage::static_draw);
   numIndices = s.indices.size();
   
   mat.diffuse.color = glm::vec3(1.0f, 0, 0);
   mat.diffuse.weight = 1.0f;
}

DrawInfo StaticMeshCollision::getDrawInfo() const {
   DrawInfo info;
   info.mode = gl::primitive::triangles;
   info.num_elements = numIndices;
   info.draw_type = DrawType::elements;
   info.ibo = ibo;
   info.index_type = gl::type::UnsignedInt;
   return info;
}

// Specifies which attributes the vbo has and where they are located
bool StaticMeshCollision::getAttributeInfo(const ShaderContract &c, AttributeInfo &info) const {
   info.stride_bytes = 3 * sizeof(GLfloat);
   info.buffer = vbo;
   info.data_type = gl::type::Float;
   
   switch (c.attribute_type) {
      case ShaderAttribute::Position:
         if (c.size != 3) return false;
         info.start_bytes = 0 * sizeof(GLfloat);
         break;
      case ShaderAttribute::Normal:
         if (c.size != 3) return false;
         info.start_bytes = 3 * sizeof(GLfloat);
         break;
      default:
         return false;
   };
   return true;
}