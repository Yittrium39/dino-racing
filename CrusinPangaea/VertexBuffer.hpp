//
//  VertexBuffer.h
//  CrusinPangaea
//
//  Created by Richie on 2/14/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//
#pragma once

#include "Platform.hpp"
#include "Factory.hpp"
#include <stdint.h>
#include <vector>

namespace gl {

	namespace buffer_usage {
		enum buffer_usage_t {
			stream_draw = GL_STREAM_DRAW,
			stream_read = GL_STREAM_READ,
			stream_copy = GL_STREAM_COPY,
			static_draw = GL_STATIC_DRAW,
			static_read = GL_STATIC_READ,
			static_copy = GL_STATIC_COPY,
			dynamic_draw = GL_DYNAMIC_DRAW,
			dynamic_read = GL_DYNAMIC_READ,
			dynamic_copy = GL_DYNAMIC_COPY
		};
	}


	class vertex_data_buffer {
	public:
		void push_float(float v) { bytes((uchar*)&v, sizeof(v)); }
		void push_int8(int8_t v) { bytes((uchar*)&v, sizeof(v)); }
		void push_int16(int16_t v) { bytes((uchar*)&v, sizeof(v)); }
		void push_int32(int32_t v) { bytes((uchar*)&v, sizeof(v)); }
		void push_uint8(uint8_t v) { bytes((uchar*)&v, sizeof(v)); }
		void push_uint16(uint16_t v) { bytes((uchar*)&v, sizeof(v)); }
		void push_uint32(uint32_t v) { bytes((uchar*)&v, sizeof(v)); }
      
      template <typename t>
      void push_data(t *v) { bytes((uchar*)v, sizeof(t)); }
      
      template <typename t>
      void push_data(t *v, uint count) { bytes((uchar *)v, count*sizeof(t)); }

		void* pointer() { return &data[0]; }
		unsigned long size() { return data.size(); }

	private:
		std::vector<uchar> data;

		void bytes(uchar* bytes, uint count) {
			for (uint i = 0; i < count; i++)
				data.push_back(bytes[i]);
		}
	};


	class vertex_buffer {
      GLuint obj;
		factory &factory() {
			static class factory factory;
			return factory;
		}
	public:
		vertex_buffer() {
         factory().create(obj, glGenBuffers, glDeleteBuffers);
      }
      
		vertex_buffer(const vertex_buffer& other) {
         factory().copy(other.obj, obj);
      }
      
		vertex_buffer(const void* data, size_t length, buffer_usage::buffer_usage_t usage) {
         factory().create(obj, glGenBuffers, glDeleteBuffers);
         this->data(data, length, usage);
      }

		~vertex_buffer() {
         factory().destroy(obj);
      }
      
		operator GLuint() const {
         return obj;
      }
      
		const vertex_buffer& operator=(const vertex_buffer& other) {
         factory().copy(other.obj, obj, true);
         return *this;
      }

		void data(const void* data, size_t length, buffer_usage::buffer_usage_t usage) {
         glBindBuffer(GL_ARRAY_BUFFER, obj);
         glBufferData(GL_ARRAY_BUFFER, length, data, usage);
      }
      
		void subdata(const void* data, size_t offset, size_t length) {
         glBindBuffer(GL_ARRAY_BUFFER, obj);
         glBufferSubData(GL_ARRAY_BUFFER, offset, length, data);
      }

		void get_subdata(void* data, size_t offset, size_t length) const {
         glBindBuffer(GL_ARRAY_BUFFER, obj);
         glGetBufferSubData(GL_ARRAY_BUFFER, offset, length, data);
      }
	};
}