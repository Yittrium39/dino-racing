//
//  Panel.cpp
//  CrusinPangaea
//
//  Created by Richie on 2/18/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

//#include "Platform.hpp"
#include "Panel.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

void Panel::begin() {
   panelBuffer.bind();
}

void Panel::end() {
   glFinish();
   panelBuffer.unbind();
}

void Panel::draw(int depth) {
   float z = depth / MAX_DEPTH;
   getPanelProgram().use();
   getPanelProgram().set_uniform(getPanelProgram().get_uniform("depth"), z);
   panelBuffer.get_texture().bind(0);
   getPanelVAO().draw_arrays(gl::primitive::triangle_fan, 0, 4);
}

void Panel::drawTexture(gl::texture &tx, int depth) {
   float z = depth / MAX_DEPTH;
   getTransformPanelProgram().use();
   getTransformPanelProgram().set_uniform(getTransformPanelProgram().get_uniform("depth"), z);
   
   glm::mat4 matrix;
   matrix = glm::scale(matrix, glm::vec3(1,-1, 0));
   matrix = glm::translate(matrix, glm::vec3(0,-1, 0));
   getTransformPanelProgram().set_uniform_mat4(getTransformPanelProgram().get_uniform("xForm"),glm::value_ptr(matrix));
   tx.bind(0);
   getPanelVAO().draw_arrays(gl::primitive::triangle_fan, 0, 4);
}

void Panel::drawMotionBlur(const gl::texture &finalTex, const gl::texture &posTex, const glm::mat4 &prevViewProjMatrix, const glm::mat4& viewProjMatrix, bool blur) {
	getBlurPanelProgram().use();
	getBlurPanelProgram().set_uniform(getBlurPanelProgram().get_uniform("depth"), 0.0f);
	
	getBlurPanelProgram().set_uniform_mat4(getBlurPanelProgram().get_uniform("prevViewProjMatrix"), glm::value_ptr(prevViewProjMatrix));
	getBlurPanelProgram().set_uniform_mat4(getBlurPanelProgram().get_uniform("viewProjMatrix"), glm::value_ptr(viewProjMatrix));

	if (blur)
		getBlurPanelProgram().set_uniform(getBlurPanelProgram().get_uniform("doBlur"), 1);
	else
		getBlurPanelProgram().set_uniform(getBlurPanelProgram().get_uniform("doBlur"), 2);

	finalTex.bind(0);
	posTex.bind(1);

	getPanelVAO().draw_arrays(gl::primitive::triangle_fan, 0, 4);
}

/***********
 * SHADERS *
 ***********/

/**
 * getPanelVertexShader
 * makes a default panel vertex shader
 */
gl::shader &Panel::getPanelVertexShader() {
   static gl::shader shader = gl::shader(gl::shader_type::vertex, GLSL(
      in vec2 aPosition;
      out vec2 vPosition;
      uniform float depth;
      void main() {
         vPosition = (aPosition + 1.0) / 2.0;
         gl_Position = vec4(aPosition, depth, 1.0);
      }
   ));
   return shader;
 }

/**
 * getTransformPanelVertexShader
 * makes a xformable panel vertex shader
 */
gl::shader &Panel::getTransformPanelVertexShader() {
   static gl::shader shader = gl::shader(gl::shader_type::vertex, GLSL(
      in vec2 aPosition;
      out vec2 vPosition;
      uniform float depth;
      uniform mat4 xForm;
      void main() {
         vec2 position = (aPosition + 1.0) / 2.0;
         vPosition = (xForm * vec4(position, 1.0, 1.0)).xy;
         gl_Position = vec4(aPosition, depth, 1.0);
      }
   ));
   return shader;
}

gl::shader &Panel::getBlurPanelVertexShader() {
	static gl::shader shader = gl::shader(gl::shader_type::vertex, GLSL(
		in vec2 aPosition;
		out vec2 vPosition;

		uniform float depth;

		void main() {
			vPosition = (aPosition + 1.0) / 2.0;
			gl_Position = vec4(aPosition, depth, 1.0);
		}
	));
	return shader;
}

/**
 * getPanelFragmentShader
 * makes a default panel fragment shader
 */
gl::shader &Panel::getPanelFragmentShader() {
   static gl::shader shader = gl::shader(gl::shader_type::fragment, GLSL(
      in vec2 vPosition;
      out vec4 outColor;
      uniform sampler2D tex;
      void main() {
         vec4 rgba = texture(tex, vPosition);
         if ( rgba.a < 0.001 ) {
            discard;
         }
         outColor = rgba;
      }
   ));
   return shader;
}

gl::shader &Panel::getBlurPanelFragmentShader() {
	static gl::shader shader = gl::shader(gl::shader_type::fragment, GLSL(
		in vec2 vPosition;
		out vec4 outColor;

		uniform int doBlur;
		uniform sampler2D posTex;
		uniform sampler2D finalTex;

		uniform mat4 prevViewProjMatrix;
		uniform mat4 viewProjMatrix;

		void main() {
			vec3 rgbNW = texture(finalTex, (gl_FragCoord.xy + vec2(-1.0, -1.0)) * vec2(1.0 / 800.0, 1.0 / 600.0)).xyz;
			vec3 rgbNE = texture(finalTex, (gl_FragCoord.xy + vec2(1.0, -1.0)) * vec2(1.0 / 800.0, 1.0 / 600.0)).xyz;
			vec3 rgbSW = texture(finalTex, (gl_FragCoord.xy + vec2(-1.0, 1.0)) * vec2(1.0 / 800.0, 1.0 / 600.0)).xyz;
			vec3 rgbSE = texture(finalTex, (gl_FragCoord.xy + vec2(1.0, 1.0)) * vec2(1.0 / 800.0, 1.0 / 600.0)).xyz;
			vec3 rgbM = texture(finalTex, gl_FragCoord.xy * vec2(1.0 / 800.0, 1.0 / 600.0)).xyz;
			vec3 luma = vec3(0.299, 0.587, 0.114);

			float lumaNW = dot(rgbNW, luma);
			float lumaNE = dot(rgbNE, luma);
			float lumaSW = dot(rgbSW, luma);
			float lumaSE = dot(rgbSE, luma);
			float lumaM = dot(rgbM, luma);
			float lumaMin = min(lumaM, min(min(lumaNW, lumaNE), min(lumaSW, lumaSE)));
			float lumaMax = max(lumaM, max(max(lumaNW, lumaNE), max(lumaSW, lumaSE)));

			vec2 dir;
			dir.x = -((lumaNW + lumaNE) - (lumaSW + lumaSE));
			dir.y = ((lumaNW + lumaSW) - (lumaNE + lumaSE));

			float dirReduce = max((lumaNW + lumaNE + lumaSW + lumaSE) * (0.25 * (1.0 / 8.0) ), (1.0 / 128.0));
	
			float rcpDirMin = 1.0 / (min(abs(dir.x), abs(dir.y)) + dirReduce);
	
			dir = min(vec2(8.0, 8.0), max(vec2(-8.0, -8.0), dir * rcpDirMin)) * vec2(1.0 / 800.0, 1.0 / 600.0);
	
			vec3 rgbA = 0.5 * ( 
				texture(finalTex, gl_FragCoord.xy * vec2(1.0 / 800.0, 1.0 / 600.0) + dir * (1.0 / 3.0 - 0.5)).xyz 
				+ texture(finalTex, gl_FragCoord.xy * vec2(1.0 / 800.0, 1.0 / 600.0) + dir * (2.0 / 3.0 - 0.5)).xyz);

			vec3 rgbB = rgbA * 0.5 + 0.25 * (
				texture(finalTex, gl_FragCoord.xy * vec2(1.0 / 800.0, 1.0 / 600.0) + dir * -0.5).xyz 
				+ texture(finalTex, gl_FragCoord.xy * vec2(1.0 / 800.0, 1.0 / 600.0) + dir * 0.5).xyz);

			float lumaB = dot(rgbB, luma);

			vec4 smoothColor;
			if ((lumaB < lumaMin) || (lumaB > lumaMax)) {
				smoothColor = vec4(rgbA, 1.0);
			} else {
				smoothColor = vec4(rgbB, 1.0);
			}

			if (doBlur == 1) {
				vec4 currentWorldSpace = vec4(vec3(texture(posTex, vPosition)), 1.0);
				vec4 currentPos = viewProjMatrix * currentWorldSpace;
				currentPos /= currentPos.w;

				vec4 previousPos = prevViewProjMatrix * currentWorldSpace;
				previousPos /= previousPos.w;
			
				float velocity = float(currentPos - previousPos) / 1.0f;
				velocity = clamp(velocity, 0.0, 0.01);

				vec4 test = texture(finalTex, vPosition);
				vec2 newPos = vPosition + vec2((-4 * velocity), 0.0);
				for (int i = 1; i < 8; ++i) {
					vec4 currentColor = texture(finalTex, newPos);
					test += currentColor;
					newPos += vec2(velocity, 0.0);
				}
				outColor = ((test / 8) + (smoothColor)) / 2.0;
			} else {
				outColor = smoothColor;
			}
		}
	));
	return shader;
}


/****************************************
 * PRIVATE SINGLETON RESOURCE ACCESSORS *
 ****************************************
 * We have these so we can access the static shaders. The reason each
 * of the static shaders is not a member of the class is because we need
 * to initialize them after we have an OpenGL context, and we shouldn't
 * initialize them if we never end up using them.
 */
 

/**
 * getPanelProgram
 * makes a plain panel program
 */
gl::program &Panel::getPanelProgram() {
   static gl::program program = makePanelProgram();
   return program;
}

/**
 * getTransformPanelProgram
 * gets the panel program that enables scaling and rotation in two-dimensions
 */
gl::program &Panel::getTransformPanelProgram() {
   static gl::program program = makeTransformPanelProgram();
   return program;
}

gl::program &Panel::getBlurPanelProgram() {
	static gl::program program = makeBlurPanelProgram();
	return program;
}

/**
 * getPanelVAO
 * this is the accessor for a lazily initialized static panel VAO
 */
gl::vertex_array &Panel::getPanelVAO() {
   static gl::vertex_array vao("test");
   return vao;
}

/**
 * getPanelVBO
 * this is the accessor for a lazily initialized static panel VBO
 */
gl::vertex_buffer &Panel::getPanelVBO() {
   static gl::vertex_buffer vbo = makePanelVBO();
   return vbo;
}

gl::framebuffer &Panel::getFramebuffer() {
	return panelBuffer;
}

/****************************
 * LAZY STATIC INITIALIZERS *
 ****************************/

/**
 * makes a panel program that enables scaling and rotation in two-dimensions
 */
gl::program Panel::makeTransformPanelProgram() {
   gl::program program = gl::program(getTransformPanelVertexShader(), getPanelFragmentShader());
   
   getPanelVAO().bind_attribute(program.get_attribute("aPosition"), getPanelVBO(), 2, gl::type::Float, 0, 0 );
   program.set_uniform(program.get_uniform("tex"), 0);
   
   return program;
}

/**
 * makes a default panel program
 */
gl::program Panel::makePanelProgram() {
   gl::program program = gl::program(getPanelVertexShader(), getPanelFragmentShader());
   getPanelVAO().bind_attribute(program.get_attribute("aPosition"), getPanelVBO(), 2, gl::type::Float, 0, 0 );
   program.set_uniform(program.get_uniform("tex"), 0);
   return program;
}

gl::program Panel::makeBlurPanelProgram() {
	gl::program program = gl::program(getBlurPanelVertexShader(), getBlurPanelFragmentShader());
	getPanelVAO().bind_attribute(program.get_attribute("aPosition"), getPanelVBO(), 2, gl::type::Float, 0, 0);
	program.set_uniform(program.get_uniform("finalTex"), 0);
	program.set_uniform(program.get_uniform("posTex"), 1);
	return program;
}

gl::vertex_buffer Panel::makePanelVBO() {
   float vertices[] = { -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, -1.0f, -1.0f, -1.0f};
   gl::vertex_buffer vbo(vertices, sizeof(vertices), gl::buffer_usage::static_draw);
   return vbo;
}