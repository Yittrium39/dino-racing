//
//  SimpleAI.cpp
//  CrusinPangaea
//
//  Created by Robert Burton on 2/19/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#include "SimpleAI.h"
#include <iostream>

static std::ostream & operator<<(std::ostream &os, const glm::vec3 &v) {
    return os << v.x << " " << v.y << " " << v.z;
}
static std::ostream & operator<<(std::ostream &os, const glm::quat &v) {
    return os << v.x << " " << v.y << " " << v.z << " " << v.w;
}
using namespace std;

void SimpleAI::update(float deltaT){
    
    kart.setAccelerating(true, true);
    kart.enablePowerup(true);
    
    
    timePassed += deltaT;
    
    //only do this when deltaT adds up to enough time
    if(timePassed > 5)//every 5 seconds
    {
        //printf("Entered code\n");
        
        if(glm::distance(kart.getPosition(), oldPosition) < 1)
        {
            //printf("Someone is stuck, resetting\n");
            //you are stuck
            kart.reset = true;
        }
        timePassed = 0;
        oldPosition = kart.getPosition();
    }
    
    
    /*
    int localMin = 1000;
    int closestPoint = 0;
    int nextPoint = 0;
    //Find Closest Spline Point
    for(int i=0; i<spline.splinePoints.size();i++)
    {
        //printf("point %d: %f %f %f\n", i, spline.splinePoints[i].x, spline.splinePoints[i].y, spline.splinePoints[i].z);
        if(glm::length(spline.splinePoints[i] - kart.getPosition()) < localMin)
        {
            localMin = glm::length(spline.splinePoints[i] - kart.getPosition());
            closestPoint = i;
        }
    }
    
    printf("closest point %d: %f %f %f\n", closestPoint, spline.splinePoints[closestPoint].x, spline.splinePoints[closestPoint].y, spline.splinePoints[closestPoint].z);
    
    if(closestPoint+1 < spline.splinePoints.size())
    {
        nextPoint = closestPoint+1;
    }
    else
    {
        nextPoint = 0;
    }
    */
    
    //if(distance < 3.0)
    //nextPoint ++
    
    
    
    //static int nextPoint = 0;
    

    float dist2 = sqrt(  (kart.getPosition().x - spline.splinePoints[nextPoint].x)*(kart.getPosition().x - spline.splinePoints[nextPoint].x)
                      + (kart.getPosition().z - spline.splinePoints[nextPoint].z)*(kart.getPosition().z - spline.splinePoints[nextPoint].z) );
    
    if(dist2 < 15.0)
    {
        nextPoint++;
        //printf("next point updated, now: %d\n", nextPoint);
    }
    
    if(nextPoint == spline.splinePoints.size())
        nextPoint = 0;
    
    //If within X of nextPoint, nextPoint ++;
    
    
//    printf("current location: %f %f %f\n", kart.getPosition().x, kart.getPosition().y, kart.getPosition().z);
//    printf("point to aim for %d: %f %f %f\n", nextPoint, spline.splinePoints[nextPoint].x, spline.splinePoints[nextPoint].y, spline.splinePoints[nextPoint].z);
    
    float dist = sqrt(  (kart.getPosition().x - spline.splinePoints[nextPoint].x)*(kart.getPosition().x - spline.splinePoints[nextPoint].x)
                      + (kart.getPosition().z - spline.splinePoints[nextPoint].z)*(kart.getPosition().z - spline.splinePoints[nextPoint].z) );
                      
//    printf("distance between: %f\n", dist);
    
    
    //vector between you and nextPoint
    glm::vec3 goalVector = (spline.splinePoints[nextPoint] - kart.getPosition());
    goalVector = glm::normalize(glm::vec3(-goalVector.x, 0, goalVector.z));
    glm::vec3 currentVector = (glm::vec3(0,0,1)*kart.getRotation());
    
    //use atan or atan2
    
 //   printf("goal vector: %f %f %f\n", goalVector.x, goalVector.y, goalVector.z);
 //   printf("current vector: %f %f %f\n", currentVector.x, currentVector.y, currentVector.z);
    
   
    float goalAngle = atan2(goalVector.z, goalVector.x)*(180/3.14159)+180;
    float currentAngle = atan2(currentVector.z, currentVector.x)*(180/3.14159)+180;
    
//    printf("goal angle: %f\n", goalAngle ) ;
    //printf("current angle: %f\n", currentAngle );

    float rightAngle;
    float leftAngle;
    
    if(goalAngle > currentAngle)
    {
        rightAngle = (360 - goalAngle) + (currentAngle - 0);
        leftAngle = goalAngle - currentAngle;
    }
    else
    {
        leftAngle = (360 - currentAngle) + (goalAngle - 0);
        rightAngle = currentAngle - goalAngle;
    }
    
//    printf("right angle: %f\n", rightAngle);
//    printf("left angle: %f\n", leftAngle);
    
    kart.setTurningLeft(false);
    kart.setTurningRight(false);
    
    if(abs(goalAngle - currentAngle) > 2.0)
    {
    if(leftAngle < rightAngle)
    {
        kart.setTurningLeft(true);
    }
    else
    {
        kart.setTurningRight(true);
    }
    }

    

    
    //cout << "rotation" << (glm::vec3(0,0,1)*kart.getRotation()) << endl;
    //kart.setRotation(<#const glm::quat &q#>)


}