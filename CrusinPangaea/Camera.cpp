


#include "Camera.h"
#include <glm/gtc/matrix_transform.hpp>

void Camera::move(direction_t dir, float distance) {
   float px = cos(3.14159 * mYaw / 180) * distance;
   float pz = sin(3.14159 * mYaw / 180) * distance;
   float pny = cos(3.14159 * mPitch / 180);
   float py = sin(3.14159 * mPitch / 180) * distance;

   switch (dir) {
      case Forward:
         mPosition.x += px * pny;
         mPosition.z += pz * pny;
         mPosition.y += py;
      break;
      case Backward:
         mPosition.x -= px * pny;
         mPosition.z -= pz * pny;
         mPosition.y -= py;
      break;
      case Right:
         mPosition.x -= pz;
         mPosition.z += px;
      break;
      case Left:
         mPosition.x += pz;
         mPosition.z -= px;
      break;
      default:
      break;
   }
   if (mPosition.y < 0.1) {
      mPosition.y = 0.1;
   }
}

void Camera::rotate(int dx, int dy) {
   mYaw += (float)dx / 10;
   mPitch += (float)dy / 10;
   if ( mPitch > 50 ) mPitch = 50; 
   if ( mPitch < -50) mPitch = -50;
}

glm::mat4 Camera::getViewMatrix() const {
   glm::vec3 focus(cos(3.14159 * mYaw   / 180) + mPosition.x,
                   sin(3.14159 * mPitch / 180) + mPosition.y,
                   sin(3.14159 * mYaw   / 180) + mPosition.z);
   
   return glm::lookAt(mPosition, focus, glm::vec3(0, 1, 0));
}

glm::mat4 Camera::getProjMatrix(float aspectRatio) const {
   return glm::perspective(60.f, aspectRatio, 0.1f, 1500.f);
}