#include "StaticMesh.h"

#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

#include <iostream>

/****
 * Helper functions and classes
 */

class MeshException : public std::exception {
   std::string infoLog;
public:
   MeshException(const std::string& str) throw() : infoLog(str) { }
   MeshException() throw() { }
   
   virtual const char* what() const throw() {
      return (std::string("MeshException: ")+infoLog).c_str();
   }
};

struct Vertex {
   glm::vec3 position;
   glm::vec3 normal;
   glm::vec2 texture_coord;
};

// StaticMesh objects may have one or more sub-meshes
// each sub-mesh gets a MeshEntry
class MeshEntry : public Renderable {
   gl::vertex_buffer vbo;
   gl::vertex_buffer ibo;
   unsigned long num_indices;
   Material mat;
public:
   virtual bool getAttributeInfo(const ShaderContract &c, AttributeInfo &info) const;
   virtual DrawInfo getDrawInfo() const;
   virtual Material getMaterial() const { return mat; }
   virtual Material &getMaterial() { return mat; }
   MeshEntry(std::vector<Vertex> vertices, std::vector<Index> indices, Material &m) : mat(m) {
      vbo = gl::vertex_buffer(&vertices[0], vertices.size() * sizeof(Vertex), gl::buffer_usage::static_draw);
      ibo = gl::vertex_buffer(&indices[0], indices.size() * sizeof(Index), gl::buffer_usage::static_draw);
      num_indices = indices.size();
   }
};


glm::vec3 vec3convert(const aiVector3D &rv) {
   glm::vec3 lv;
   for (int i = 0; i < 3; i++) lv[i] = rv[i];
   return lv;
}

glm::vec2 vec2convert(const aiVector3D &rv) {
   glm::vec2 lv;
   for (int i = 0; i < 2; i++) lv[i] = rv[i];
   return lv;
}


/**
 * StaticMesh functions
 */


StaticMesh::StaticMesh(const std::string &meshFile, const std::string &textureDir) {
   // Create an instance of the Importer class
   Assimp::Importer importer;
   
   // And have it read the given file with some postprocessing
   const aiScene* scene = importer.ReadFile(meshFile,
                                            aiProcess_CalcTangentSpace       |
                                            aiProcess_Triangulate            |
                                            aiPrimitiveType_LINE             |
                                            aiPrimitiveType_POINT            |
                                            aiProcess_JoinIdenticalVertices  |
                                            aiProcess_FlipUVs                |
                                            aiProcess_GenUVCoords            |
                                            aiProcess_TransformUVCoords      |
                                            aiProcess_SortByPType);

   // If the import failed, report it
   if(!scene) {
      throw MeshException(importer.GetErrorString());
   }
   
   // Load the textures
   loadMaterialsFromScene(scene, textureDir);
   
   // Now load the meshes
   loadMeshesFromScene(scene);
   
   // We're done. Everything will be cleaned up by the importer destructor
}



// This function loops through the meshes of a loaded assimp model and loads
// each one
void StaticMesh::loadMeshesFromScene(const aiScene *pScene) {
   for (int i = 0 ; i < pScene->mNumMeshes ; i++) {
      const aiMesh* paiMesh = pScene->mMeshes[i];
      mRenderables.push_back(loadMesh(paiMesh));
   }
}


// Gets vertices and indices, makes a vbo and an ibo
std::shared_ptr<Renderable> StaticMesh::loadMesh(const aiMesh* pMesh) const {

   std::vector<Index> indices = loadMeshIndices(pMesh);
   std::vector<Vertex> vertices = loadMeshVertices(pMesh);
   
   unsigned int materialIndex = pMesh->mMaterialIndex;
   Material &m = *mMaterials[materialIndex];
   
   std::shared_ptr<Renderable> mesh = std::shared_ptr<Renderable>(new MeshEntry(vertices, indices, m));

   return mesh;
}


std::vector<Vertex> StaticMesh::loadMeshVertices(const aiMesh *pMesh) const {
   bool hasTextureCoords = pMesh->HasTextureCoords(0);
   bool hasNormals = pMesh->HasNormals();

   std::vector<Vertex> vertices;
   // Add all of the vertices to the vertices vector
   for (int i = 0 ; i < pMesh->mNumVertices ; i++) {
      Vertex v;
      v.position = vec3convert(pMesh->mVertices[i]);
      
      // for compatibility, populate the main vertices vector
      this->vertices.push_back(v.position);
      
      if (hasNormals)
         v.normal = vec3convert(pMesh->mNormals[i]);
      if (hasTextureCoords) {
         v.texture_coord = vec2convert(pMesh->mTextureCoords[0][i]);
      }
      vertices.push_back(v);
   }
   
   return vertices;
}

std::vector<Index> StaticMesh::loadMeshIndices(const aiMesh *pMesh) const {
   std::vector<Index> indices;

   // compatibility code...
   unsigned int maxIndex = (unsigned int)this->vertices.size();
   // Add all of the indices to the indices vector
   for (int i = 0 ; i < pMesh->mNumFaces ; i++) {
      const aiFace& face = pMesh->mFaces[i];
      assert(face.mNumIndices == 3);
      indices.push_back(face.mIndices[0]);
      indices.push_back(face.mIndices[1]);
      indices.push_back(face.mIndices[2]);
      
      // for compatibility, populate the main indices vector
      this->indices.push_back(face.mIndices[0]+maxIndex);
      this->indices.push_back(face.mIndices[1]+maxIndex);
      this->indices.push_back(face.mIndices[2]+maxIndex);
   }
   
   return indices;
}

void StaticMesh::loadMaterialsFromScene(const aiScene* pScene, const std::string &textureDir) {
   for (int i = 0 ; i < pScene->mNumMaterials ; i++) {
      const aiMaterial* pMaterial = pScene->mMaterials[i];
      mMaterials.push_back(loadMaterial(pMaterial, textureDir));
   }
}

std::shared_ptr<Material> StaticMesh::loadMaterial(const aiMaterial* pMaterial, const std::string &texDir) const {
   std::shared_ptr<Material> m = std::shared_ptr<Material>(new Material());

   if (pMaterial->GetTextureCount(aiTextureType_DIFFUSE) > 0) {
      aiString texPath;
      aiTextureMapping mapping;
      aiTextureMapMode mode;
      if (pMaterial->GetTexture(aiTextureType_DIFFUSE, 0, &texPath, &mapping, NULL, NULL, NULL, &mode) == AI_SUCCESS) {
         std::string texFile = texDir + "/" + texPath.data;
         gl::Image image(texFile);
         gl::texture tex(image);
         tex.SetWrapping(gl::Wrapping::Repeat, gl::Wrapping::Repeat);
         m->diffuse.hasTexture = true;
         m->diffuse.weight = 1.0f;
         m->diffuse.texture = tex;
      }
   } else {
       m->diffuse.hasTexture = false;
       m->diffuse.color = glm::vec3(0.6,0.8,0.3);
   }
   return m;
}

/**
 * MeshEntry functions
 */
DrawInfo MeshEntry::getDrawInfo() const {
   DrawInfo info;
   info.isSkinned = false;
   info.mode = gl::primitive::triangles;
   info.num_elements = (unsigned int)num_indices;
   info.draw_type = DrawType::elements;
   info.ibo = ibo;
   info.index_type = gl::type::UnsignedInt;
   return info;
}

// Specifies which attributes the vbo has and where they are located
bool MeshEntry::getAttributeInfo(const ShaderContract &c, AttributeInfo &info) const {
   info.stride_bytes = sizeof(Vertex);
   info.buffer = vbo;
   info.data_type = gl::type::Float;
   
   if (c.attribute_type == ShaderAttribute::Position) {
      if (c.size != 3)
         return false;
      info.start_bytes = 0;
      
   } else if (c.attribute_type == ShaderAttribute::Normal) {
      if (c.size != 3)
         return false;
      info.start_bytes = 3 * sizeof(float);
   
   } else if (c.attribute_type == ShaderAttribute::TextureCoordinate) {
      if (c.size != 2)
         return false;
      info.start_bytes = 6 * sizeof(float);
      
   } else {
      return false;
   }
   return true;
}
   
   
   