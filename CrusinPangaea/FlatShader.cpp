//
//  FlatShader.cpp
//  CrusinPangaea
//
//  Created by Richie on 2/17/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#include "FlatShader.h"

#include <exception>
#include <iostream>
#include <glm/gtc/type_ptr.hpp>

FlatShader::FlatShader() {
   gl::shader mesh_vertex(gl::shader_type::vertex, GLSL(
      in vec3 position;
      in vec3 normal;
      
      out vec3 norm;
      out vec3 pos;
      
      uniform mat4 model;
      uniform mat4 projection;
      uniform mat4 view;
      
      void main() {
			mat3 mv = mat3(model[0].xyz, model[1].xyz, model[2].xyz);
			norm = mv * normal;
         pos = (model * vec4(position, 1)).xyz;
         gl_Position = projection * view * vec4(pos, 1);
      }
   ));
   gl::shader mesh_fragment(gl::shader_type::fragment, GLSL(
      in vec3 norm;
      in vec3 pos;
      uniform vec3 color;
      
      out vec3 fnormal;
      out vec3 fcolor;
      out vec3 fpos;
      
      void main() {
         fcolor = color;
         fnormal = norm;
         fpos = pos;
      }
   ));
   meshProgram.Attach(mesh_vertex);
   meshProgram.Attach(mesh_fragment);
   glBindFragDataLocation(meshProgram, 0, "fcolor");
   glBindFragDataLocation(meshProgram, 1, "fnormal");
   glBindFragDataLocation(meshProgram, 2, "fpos");
   meshProgram.Link();
}

bool FlatShader::canRenderRenderable(const Renderable &renderable) const {
   ShaderContract position_contract(ShaderAttribute::Position, 3);
   ShaderContract normal_contract(ShaderAttribute::Normal, 3);
   
   AttributeInfo position_info;
   AttributeInfo normal_info;
   
   bool has_position = renderable.getAttributeInfo(position_contract, position_info);
   bool has_normal = renderable.getAttributeInfo(normal_contract, normal_info);
      
   return has_position && has_normal;
}

gl::vertex_array FlatShader::vaoForRenderable(const Renderable &renderable) const {
   ShaderContract position_contract(ShaderAttribute::Position, 3);
   ShaderContract normal_contract(ShaderAttribute::Normal, 3);
   
   AttributeInfo position_info;
   AttributeInfo normal_info;
   
   bool has_position = renderable.getAttributeInfo(position_contract, position_info);
   bool has_normal = renderable.getAttributeInfo(normal_contract, normal_info);
   if (!has_position || !has_normal) {
      throw std::runtime_error("FlatShader: missing either position, normal, or texcoord.");
   }
   
   gl::vertex_array vao;
   gl::attribute position = meshProgram.get_attribute("position");
   bind_contract(vao, position, position_contract, position_info);
   
   gl::attribute normal = meshProgram.get_attribute("normal");
   bind_contract(vao, normal, normal_contract, normal_info);
   
   return vao;
}

void FlatShader::setModel(const glm::mat4 &modelm) {
   gl::uniform model = meshProgram.get_uniform("model");
   meshProgram.set_uniform_mat4(model, glm::value_ptr(modelm));
}

void FlatShader::setView(const glm::mat4 &viewm) {
   gl::uniform view = meshProgram.get_uniform("view");
   meshProgram.set_uniform_mat4(view, glm::value_ptr(viewm));
}

void FlatShader::setProjection(const glm::mat4 &proj) {
   gl::uniform projection = meshProgram.get_uniform("projection");
   meshProgram.set_uniform_mat4(projection, glm::value_ptr(proj));
}

void FlatShader::setMaterial(const Material &mat) {
   meshProgram.use();
   
   gl::uniform color = meshProgram.get_uniform("color");
   meshProgram.set_uniform_vec3(color, glm::value_ptr(mat.diffuse.color));
}

void FlatShader::use() {
   meshProgram.use();
}