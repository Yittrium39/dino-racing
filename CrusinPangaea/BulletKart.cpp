// BulletKart class - Kevin Ubay-Ubay

#include <iostream>
#include <string>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include "BulletKart.h"

using namespace glm;
using namespace std;

namespace physics {

BulletKart::BulletKart() {
	startPoint = vec3(0, 0, 0);

	gEngineForce = 0.f;
	gBreakingForce = 100.f;
	maxEngineForce = 10000.f;
	maxBreakingForce = 1000.f;
	wheelRadius = 0.9f;
	wheelWidth = 1.0f;
	suspensionRestLength = btScalar(1.2);

	suspensionStiffness = 2000.f;
	suspensionDamping = 2000.f;
	suspensionCompression = 2000.f;
	wheelFriction = 10000000;
	rollInfluence = 0.00f;

	gVehicleSteering = 0.f;
	steeringIncrement = 0.04f;
	steeringClamp = 0.4f;

	steerRight = steerLeft = isReverse = false;
	cameraPosition = btVector3(-180.0, 12.0f, -345.0f);

	// btCollisionShape* chassisShape = new btBoxShape(btVector3(kartDimensions[0], kartDimensions[1], kartDimensions[2]));
	btCollisionShape* chassisShape = new btBoxShape(btVector3(1.6f, 1.0, 4.0));
	collisionShapes.push_back(chassisShape);

	btCompoundShape* compound = new btCompoundShape();

   collisionShapes.push_back(compound);
   btTransform localTrans;
   localTrans.setIdentity();

   localTrans.setOrigin(btVector3(0.0, 2.0, 0));
   compound->addChildShape(localTrans, chassisShape);

   btTransform myTransform;
   btQuaternion myRotate;
   // btScalar angle = (startOrientation.w * 3.14159f) / 180.0f;
   // btScalar angle = (90.0f * 3.14159f) / 180.0f;
   myTransform.setIdentity();
   myRotate.setRotation(btVector3(0.0f, -1.0, 0.0), 30.0);
   myTransform.setRotation(myRotate);

   if (0) {
      myTransform.setOrigin(btVector3(-160.0, 2.5f, -360.0f));
   } else {
      myTransform.setOrigin(btVector3(-160.0, 30.0f, -360.0f));
   }   

   float mass = 1000.0f;
   btVector3 localInertia(0, 0, 0); 

   compound->calculateLocalInertia(mass, localInertia);

   vehicleMotionState = new btDefaultMotionState(myTransform);
   btRigidBody::btRigidBodyConstructionInfo cInfo(mass, vehicleMotionState, compound, localInertia);
   kartChassis = new btRigidBody(cInfo);

   kartChassis->setLinearVelocity(btVector3(0, 0, 0));
   kartChassis->setAngularVelocity(btVector3(0, 0, 0));
   
   accelerating = false;
   braking = false;
   turningLeft = false;
   turningRight = false;
   boost = false;
   forward = true;
}

void BulletKart::update() {
	 applySteeringRight(turningRight);
	 applySteeringLeft(turningLeft);
	 //applyReverseAcceleration(braking);
    applyAccelerationForce(accelerating);

   
	int wheelIndex = 2;
	kartVehicle->applyEngineForce(gEngineForce, wheelIndex);
	kartVehicle->setBrake(gBreakingForce, wheelIndex);

	wheelIndex = 3;
	kartVehicle->applyEngineForce(gEngineForce, wheelIndex);
	kartVehicle->setBrake(gBreakingForce, wheelIndex);

	if (steerRight) {
		gVehicleSteering -= steeringIncrement;
		if (gVehicleSteering < -steeringClamp)
			gVehicleSteering = -steeringClamp;
	} else if (steerLeft) {
		gVehicleSteering += steeringIncrement;
		if (gVehicleSteering > steeringClamp) gVehicleSteering = steeringClamp;
	} else {
		gVehicleSteering = 0.0f;
	}

	wheelIndex = 0;
	kartVehicle->setSteeringValue(gVehicleSteering, wheelIndex);

	wheelIndex = 1;
	kartVehicle->setSteeringValue(gVehicleSteering, wheelIndex);

	// clamp speed to prevent going out of control
	btVector3 velocity = kartVehicle->getRigidBody()->getLinearVelocity();
	float speed = velocity.length();

	if(powerUpBoost) {
        float velocityRatio = 3;
		powerUpBoost = false;
        kartVehicle->getRigidBody()->setLinearVelocity(velocity * velocityRatio);
    }
    else if(jump) {
		jump = false;
        kartVehicle->getRigidBody()->setLinearVelocity(velocity + btVector3(0, 15, 0));
    }
    else if(drop) {
		drop = false;
        kartVehicle->getRigidBody()->setLinearVelocity(btVector3(0, 0, 0));
    }
    //player's cap speed
    else if (speed > 75.0f && !boost) {
		float velocityRatio = (speed - 75.0f) / 1.1;
        velocityRatio = (velocityRatio + 75.0f) / speed;
        kartVehicle->getRigidBody()->setLinearVelocity(velocity * velocityRatio);
    //AI's cap speed
	} else if (speed > 30.0f){
      float velocityRatio = 30.0f / speed;
		kartVehicle->getRigidBody()->setLinearVelocity(velocity * velocityRatio);
    }
    
    if(braking) {
         if (speed > 1) {
            //kartVehicle->getRigidBody()->setLinearVelocity(velocity * 0.85);
             //savedSpeed = velocity;
             //printf("braking\n");
         }/* else if (speed <= 5) {
             printf("backing up\n");
             kartVehicle->getRigidBody()->setLinearVelocity(-1*savedSpeed);
             gEngineForce = -maxEngineForce;
             if(velocity*-1.1 > 0) {
                 printf("Should be negative - ");
                 printf("%lf, %lf, %lf\n", velocity.x(), velocity.y(), velocity.z());
                 kartVehicle->getRigidBody()->setLinearVelocity(velocity*1.1);
             }
             else {
                 printf("Should be negative - ");
                 printf("%lf, %lf, %lf\n", velocity.x(), velocity.y(), velocity.z());
                 kartVehicle->getRigidBody()->setLinearVelocity(velocity*-1.1);
             }
         }/* else {
            kartVehicle->getRigidBody()->setLinearVelocity(velocity * 1.5);
         }*/
        /*speed--;
        printf("%lf\n", speed);
        kartVehicle->getRigidBody()->setLinearVelocity(velocity/speed);*/
        //printf("%lf, %lf, %lf\n", velocity.x(), velocity.y(), velocity.z());
    }
}

bool BulletKart::checkReverse() const {
	btVector3 kartAngle = kartVehicle->getForwardVector();
	btVector3 movementAngle = kartVehicle->getRigidBody()->getLinearVelocity();

	btScalar diffAngle = movementAngle.angle(kartAngle);

	//printf("Angle: %f\n", diffAngle);
	if (diffAngle >= 1) {
		return true;
	}
	return false;
}

float BulletKart::getSpeed() const {
	return kartVehicle->getRigidBody()->getLinearVelocity().length();
}

void BulletKart::applyBrakingForce(bool apply) {
	if (apply) {
		gEngineForce = 0.0f;
		gBreakingForce = maxBreakingForce;
	} else {
		gBreakingForce = 0.f;
	}
}

void BulletKart::applyAccelerationForce(bool apply) {
	if (apply) {
		if (forward) {
			gEngineForce = maxEngineForce;
			gBreakingForce = 0.f;
		} else {
			gEngineForce = -maxEngineForce;
			gBreakingForce = 0.f;
		}
	} else {
		gEngineForce = 0.f;
		
		//apply some sort of friction
		kartVehicle->getRigidBody()->setLinearVelocity(kartVehicle->getRigidBody()->getLinearVelocity() * 0.97);
	}
}

void BulletKart::applySteeringLeft(bool apply) {
	steerLeft = apply;
}

void BulletKart::applySteeringRight(bool apply) {
	steerRight = apply;
}

/* void BulletKart::toggleReverse() {
	isReverse = (isReverse ? false : true);
} */

void BulletKart::applyReverseAcceleration(bool apply) {
	if (apply) {
		gEngineForce = -maxEngineForce;
		gBreakingForce = 0.f;
	} else {
		gEngineForce = 0.f;
	}
}

mat4 BulletKart::getModelTransform() {
	mat4 model = mat4(1.0f);

	kartVehicle->getRigidBody()->getWorldTransform().getOpenGLMatrix(glm::value_ptr(model));

	return model;
}

glm::vec3 BulletKart::getPosition() const {
   glm::vec3 position;
   btVector3 kpos = kartVehicle->getRigidBody()->getWorldTransform().getOrigin();
   position.x = kpos.x();
   position.y = kpos.y();
   position.z = kpos.z();
   return position;
}

glm::quat BulletKart::getRotation() const {
   glm::quat rotation;
   btQuaternion krot = kartVehicle->getRigidBody()->getWorldTransform().getRotation();
   rotation.w = krot.w();
   rotation.x = krot.x();
   rotation.y = krot.y();
   rotation.z = krot.z();
   return rotation;
}

bool BulletKart::getBraking() const {
	return braking;
}

bool BulletKart::getAccelerating() const {
	return accelerating;
}

bool BulletKart::getPowerUpBoost() const {
	return powerUpBoost;
}

void BulletKart::setPosition(const glm::vec3 &position) {
   btTransform myTransform = kartVehicle->getRigidBody()->getWorldTransform();
   btVector3 kpos(position.x, position.y, position.z);
   myTransform.setOrigin(kpos);
   kartVehicle->getRigidBody()->setWorldTransform(myTransform);
}

void BulletKart::setRotation(const glm::quat &rotation) {
   btTransform myTransform = kartVehicle->getRigidBody()->getWorldTransform();
   btQuaternion krot(rotation.x, rotation.y, rotation.z, rotation.w);
   myTransform.setRotation(krot);
   kartVehicle->getRigidBody()->setWorldTransform(myTransform);
}


mat4 BulletKart::getViewTransform() {
	btTransform chassisWorldTrans;

	// need to improve this
	kartVehicle->getRigidBody()->getMotionState()->getWorldTransform(chassisWorldTrans);
	btVector3 cameraTargetPosition = chassisWorldTrans.getOrigin();
	cameraPosition[1] = (40.0*cameraPosition.y() + cameraTargetPosition.y()) / 41.0f;

	btVector3 camToObject = cameraTargetPosition - cameraPosition;

	float cameraDistance = camToObject.length();
	float correctionFactor = 0.f;
	if (cameraDistance <= 7.0f) {
		correctionFactor = 0.15*(7.0f - cameraDistance) / cameraDistance;
	}

	if (cameraDistance >= 15.0f) {
		correctionFactor = 0.15*(15.f - cameraDistance) / cameraDistance;
	}

	cameraPosition -= correctionFactor * camToObject;

	glm::mat4 view = glm::lookAt(glm::vec3(cameraPosition[0], cameraPosition[1] + 6.0f, cameraPosition[2]),
		glm::vec3(cameraTargetPosition[0], cameraTargetPosition[1], cameraTargetPosition[2]),
		glm::vec3(0, 1, 0));

	return view;
}
    
    void BulletKart::applyPowerUpBoost(bool enabled) {
        powerUpBoost = enabled;
    }

    void BulletKart::applyJump(bool enabled) {
        jump = enabled;
    }
    
    void BulletKart::applyDrop(bool enabled) {
        drop = enabled;
    }
    

}
