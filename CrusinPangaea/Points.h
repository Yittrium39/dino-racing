//
//  Points.h
//  CrusinPangaea
//
//  Created by Richie on 2/26/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#pragma once

#include "Renderable.h"
#include "Material.h"

#include "Program.hpp"
#include "VertexBuffer.hpp"

#include <glm/glm.hpp>
#include <vector>

class Points : public Renderable {
   std::vector<glm::vec3> points;
   gl::vertex_buffer vbo;
   Material mat;
   
   public:
   Points();
   void addPoint(const glm::vec3 &p);
   virtual bool getAttributeInfo(const ShaderContract &c, AttributeInfo &info) const;
   virtual DrawInfo getDrawInfo() const;
   virtual Material getMaterial() const { return mat; }
};