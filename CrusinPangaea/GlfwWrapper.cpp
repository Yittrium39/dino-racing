#include "GlfwWrapper.h"

#include <utility>
#include <vector>

namespace glfw {
/**
 * Creates a GLFW window using the window manager. This is needed in case an
 * inheriting class does not explicitly call the designated constructor.
 */
Window::Window() {
   WindowManager::getInstance().createWindow("Glfw Window", *this);
}

/**
 * The designated constructor. This creates a GLFW window using the window
 * manager with the specified title.
 */
Window::Window(const std::string &title) {
   WindowManager::getInstance().createWindow(title, *this);
}

std::map<int, Window *> WindowManager::mWindows;

/**
 * Initializes glfw. This will only be called once since WindowManager
 * is a singleton.
 */
WindowManager::WindowManager() {
   glfwInit();
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 3);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 2);
	glfwOpenWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
}

/** 
 * This is called by the constructor of Window to create the window and
 * register it with the window manager.
 */
void WindowManager::createWindow(const std::string &title, Window &window) {
   static bool windowExists = false;
   if (windowExists) {
      throw GLFWException("Only one GLFW window may be created.");
   }
   glfwOpenWindow(800, 600, 0, 0, 0, 0, 32, 0, GLFW_WINDOW);
   glfwSetKeyCallback(&WindowManager::keyboardCallback);
   windowExists = true;
   onlyWindow = &window;
}

void WindowManager::keyboardCallback(int key, int action) {
   gl::event e;
   if (action == GLFW_RELEASE) {
      e.type = gl::event::key_up;
   } else if (action == GLFW_PRESS) {
      e.type = gl::event::key_down;
   }
   e.key = map_key(key);
   onlyWindow->event(e);
}

gl::key_event WindowManager::map_key(int key) {
   gl::key_event ev;
   ev.control = false;
   ev.alt = false;
   ev.shift = false;
   if (key == GLFW_KEY_LEFT) {
      ev.code = gl::key::left;
   } else if (key == GLFW_KEY_RIGHT) {
      ev.code = gl::key::right;
   } else if (key == GLFW_KEY_UP) {
      ev.code = gl::key::up;
   } else if (key == GLFW_KEY_DOWN) {
      ev.code = gl::key::down;
      } else if (key == GLFW_KEY_SPACE) {
      ev.code = gl::key::space;
   } else if (key == GLFW_KEY_ENTER) {
      ev.code = gl::key::enter;
   } else if (key >= 'A' && key <= 'Z') {
      ev.shift = true;
      ev.code = (gl::key::key_t)(gl::key::a + (key - 'A'));
   } else if (key <= 'a' && key <= 'z') {
      ev.code = (gl::key::key_t)(gl::key::a + (key - 'a'));
   } else if (key == 27) {
      ev.code = gl::key::escape;
   }
   return ev;
}

void Window::run() {
    while (glfwGetWindowParam(GLFW_OPENED)) {
      glfwPollEvents();
      update();
      draw();
      glfwSwapBuffers();
   }
}

Window *WindowManager::onlyWindow;


}
