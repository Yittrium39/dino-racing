//
//  Rockets.cpp
//  CrusinPangaea
//
//  Created by Robert Burton on 3/10/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#include "Rockets.h"

void Rockets::update(float deltaT, Particles particles){
    for(int j=0; j< rockets.size(); j++)
    {
        if(rockets[j].active == 1)
        {
            //printf("rocket pos: %f %f %f, target: %f %f %f\n", rockets[j].location.x, rockets[j].location.y, rockets[j].location.z,
              //                          rockets[j].target->getPosition().x, rockets[j].target->getPosition().y, rockets[j].target->getPosition().z);
            if(glm::distance(rockets[j].location, rockets[j].target->getPosition()) < 2.0)
            {
                //printf("got to target\n");
                rockets[j].active = 0;
                //tell the target to apply an explotion force
                rockets[j].target->setJump(true);
				rockets[j].target->missileHit = true;
                particles.createExplosion(rockets[j].target->getPosition());
            }
            
            //if homeing rocket, set direction toward target
            rockets[j].direction = glm::normalize(rockets[j].target->getPosition() - rockets[j].location);
            
            //update rocket position
            rockets[j].location += rockets[j].direction*rockets[j].velocity;
        }
    }
}

void Rockets::addRocket(glm::vec3 location, glm::vec3 direction, float velocity, KartControl* target){
    //printf("adding rocket\n");
    Rocket newRocket = {glm::vec3(location.x, location.y+1, location.z), direction, velocity, glm::vec3(0.5,0,0), 1, target};
    rockets.push_back(newRocket);
}