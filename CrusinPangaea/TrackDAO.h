/**
 * This class loads game elements based on a loaded JSON document. The structure of the expected
 * document format is as follows:
 
{ 
   "track": {
      "mesh"		: "assets/levels/track1.3ds",
      "textures"	: "assets/levels",
      "scale"		: [1, 1, 1],
      "translate"	: [0, 0, 0]
   },
	"player": {
      "mesh"		: "assets/models/tricera.dae",
      "textures"  : "assets/models",
      "scale"		: [2, 2, 2],
      "translate"	: [0, 0, 0]
   },
	"music": {
      "file"		: "assets/sounds/Jumper.mp3"
	}
}

 *
 * If the document does not match this format, the behavior is unspecified.
 */

#pragma once

#include "SceneObject.h"
#include "JSONLoader.h"
#include <map>
#include <memory>
#include <vector>

class TrackDAO {
	SceneObject mTrack;
	SceneObject mPlayer;
	std::vector<SceneObject> mOpponents;
	std::string mSongFile;

	// This stores the description all of the levels we have loaded so we don't need to load the JSON files
	// a bunch of times.
	static JSONNode &getRootNode(const std::string &file);

	// Loads the main player, creates a mesh, creates an instance from the mesh
	// Called before the constructor, so static for safety
	static SceneObject loadPlayer(const std::string &file);

	// Loads the opponent player, creates a mesh, creates an instance from the mesh
	// Called before the constructor, so static for safety
	static std::vector<SceneObject> loadOpponents(const std::string &file);

	// Loads the track, creates a mesh, creates an instance from the mesh
	// Called before the constructor, so static for safety
	static SceneObject loadTrack(const std::string &file);

   // Gets the path to the song file from the JSON document
   // Called before the constructor, so static for safety
	static std::string loadSongFile(const std::string &file);

public:
	const SceneObject &getTrack() { return mTrack; }
	const SceneObject &getPlayer() { return mPlayer; }
	const std::vector<SceneObject> &getOpponents() { return mOpponents; }
	const std::string getSongFile() { return mSongFile; }

	TrackDAO(const std::string &level) : mTrack(loadTrack(level)), 
										 mPlayer(loadPlayer(level)),
										 mOpponents(loadOpponents(level)),
										 mSongFile(loadSongFile(level)) { }
};