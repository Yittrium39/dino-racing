//
//  Scene.cpp
//  CrusinPangaea
//
//  Created by Richie on 2/17/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#include "Scene.h"
#include "Points.h"
#include "Particles.h"
#include "BulletWorld.h"


Scene::Scene() {
   TrackDAO trackStuff("level3.json");
   
   track = trackStuff.getTrack();
    
    //Particle Points stuff
    particles = *new Particles();
    particles.addParticle(glm::vec3(-340, 1.5, 395), glm::vec3(0, 1, 0));
    
    //Particle Points stuff
    Points *p = new Points();
    p->addPoint(glm::vec3(0, 0, 0));
    std::shared_ptr<Renderable> renderable(p);
    SceneObject points_object(renderable);
    points = points_object;

   myBulletWorld = new physics::BulletWorld(-20.8);

   // Hard-coding the collision stuff
   std::shared_ptr<StaticMesh> pTrack(new StaticMesh("assets/levels/dinodinojungle/dinodinojungle.DAE","assets/levels/dinodinojungle/"));
   physics_track = SceneObject(pTrack->begin(), pTrack->end());
   physics_track.setMesh(pTrack);
   physics_track.setRotation(track.getRotation());
   physics_track.setTranslation(track.getTranslation()+glm::vec3(0,113.5,0));
   physics_track.setScale(track.getScale());
   myBulletWorld->setTrack(&physics_track);

   sceneKarts.push_back(trackStuff.getPlayer());
   sceneKarts.insert(sceneKarts.end(), trackStuff.getOpponents().begin(), trackStuff.getOpponents().end());
   
   std::vector<SceneObject>::iterator sceneKart = sceneKarts.begin();
   for (; sceneKart != sceneKarts.end(); sceneKart++) {
      physics::Kart *physicsKart = myBulletWorld->makeKart();
      std::shared_ptr<KartControl> kartControl(new KartControl(physicsKart, &*sceneKart));
      kartControl->setPosition(sceneKart->getTranslation());
      kartControl->setRotation(sceneKart->getRotation());
      controlKarts.push_back(kartControl);
   }
   
   spline.createSpline();
    generatePowerups();

    rearCam = false;
    boostCam = false;
    distanceBehindCar = 20.0f;
    angle = 10.0f;
    camVal = 0;

   
   // Create AI
   std::vector<std::shared_ptr<KartControl> >::iterator controlKart = controlKarts.begin();
   controlKart++; // Skip the main player
   for (; controlKart != controlKarts.end(); controlKart++) {
      simpleAIs.push_back(std::shared_ptr<SimpleAI>(new SimpleAI(**controlKart, spline)));
   }
}

void Scene::rearView(bool yn) {
    rearCam = yn;
}

static glm::quat offset(const glm::quat &q, const glm::vec3 &axis, float rad) {
   glm::vec3 ax = glm::normalize(axis);
   ax *= sinf(rad / 2.0f);
   float scalar = cosf(rad / 2.0f);
   glm::quat offset(scalar, ax.x, ax.y, ax.z);
   return glm::normalize(q * offset);
}

static glm::vec3 rotate(const glm::vec3 &v, const glm::quat &q) {
   glm::vec4 rotated =  q * glm::vec4(v, 1.0f);
   return glm::normalize(glm::vec3(rotated));
}

void Scene::generatePowerups() {
	PowerupObj obj1;
	int xLoc;
	int zLoc;
    
    StaticMesh egg_mesh("assets/models/dinoEggBlue.dae", "assets/models");
	StaticMesh egg_mesh2("assets/models/dinoEggRed.dae", "assets/models");
    
	for (int i = 0; i < 16; i++) {
		for (int j = 0; j < 13; j++) {
			powerupUSS[i][j].type = PowerupType::None;
		}
	}

	// Jungle level map locations
	glm::quat rot;
	float w;
	glm::vec3 axis;
	w = cos(-1.571/2.0f);
	axis = (float)sin(-1.571/2.0f)*glm::normalize(glm::vec3(1.0, 0.0, 0.0));
	rot = glm::quat(w, axis);

	//Boost locations
	obj1.mapLocation = glm::vec3(5, -8+3, -162);
	obj1.type = PowerupType::Boost;
	(*egg_mesh.begin())->getMaterial().diffuse.color = glm::vec3(.1,.4,.1);
    eggs.push_back(std::make_shared<SceneObject>(egg_mesh.begin(), egg_mesh.end()));
    eggs.back()->setTranslation(obj1.mapLocation);
    eggs.back()->setScale(glm::vec3(3.0));
	eggs.back()->setRotation(rot);
    obj1.model = eggs.back();
	xLoc = obj1.mapLocation.x + 650;
	zLoc = obj1.mapLocation.z + 300;
	powerupUSS[xLoc / 50][zLoc / 50] = obj1;

	obj1.mapLocation = glm::vec3(-141, -12+3, 69);
	(*egg_mesh.begin())->getMaterial().diffuse.color = glm::vec3(.1,.4,.1);
    eggs.push_back(std::make_shared<SceneObject>(egg_mesh.begin(), egg_mesh.end()));
    eggs.back()->setTranslation(obj1.mapLocation);
    eggs.back()->setScale(glm::vec3(3.0));
	eggs.back()->setRotation(rot);
    obj1.model = eggs.back();
	xLoc = obj1.mapLocation.x + 650;
	zLoc = obj1.mapLocation.z + 300;
	powerupUSS[xLoc / 50][zLoc / 50] = obj1;

	obj1.mapLocation = glm::vec3(-201, -13+3, 15);
	(*egg_mesh.begin())->getMaterial().diffuse.color = glm::vec3(.1,.4,.1);
    eggs.push_back(std::make_shared<SceneObject>(egg_mesh.begin(), egg_mesh.end()));
    eggs.back()->setTranslation(obj1.mapLocation);
    eggs.back()->setScale(glm::vec3(3.0));
	eggs.back()->setRotation(rot);
    obj1.model = eggs.back();
	xLoc = obj1.mapLocation.x + 650;
	zLoc = obj1.mapLocation.z + 300;
	powerupUSS[xLoc / 50][zLoc / 50] = obj1;

	obj1.mapLocation = glm::vec3(-474, -11+3, 182);
	(*egg_mesh.begin())->getMaterial().diffuse.color = glm::vec3(.1,.4,.1);
    eggs.push_back(std::make_shared<SceneObject>(egg_mesh.begin(), egg_mesh.end()));
    eggs.back()->setTranslation(obj1.mapLocation);
    eggs.back()->setScale(glm::vec3(3.0));
	eggs.back()->setRotation(rot);
    obj1.model = eggs.back();
	xLoc = obj1.mapLocation.x + 650;
	zLoc = obj1.mapLocation.z + 300;
	powerupUSS[xLoc / 50][zLoc / 50] = obj1;

	obj1.mapLocation = glm::vec3(-600, 20+3, 250);
	(*egg_mesh.begin())->getMaterial().diffuse.color = glm::vec3(.1,.4,.1);
    eggs.push_back(std::make_shared<SceneObject>(egg_mesh.begin(), egg_mesh.end()));
    eggs.back()->setTranslation(obj1.mapLocation);
    eggs.back()->setScale(glm::vec3(3.0));
	eggs.back()->setRotation(rot);
    obj1.model = eggs.back();
	xLoc = obj1.mapLocation.x + 650;
	zLoc = obj1.mapLocation.z + 300;
	powerupUSS[xLoc / 50][zLoc / 50] = obj1;

	obj1.mapLocation = glm::vec3(-550, 46+3, -152);
	(*egg_mesh.begin())->getMaterial().diffuse.color = glm::vec3(.1,.4,.1);
    eggs.push_back(std::make_shared<SceneObject>(egg_mesh.begin(), egg_mesh.end()));
    eggs.back()->setTranslation(obj1.mapLocation);
    eggs.back()->setScale(glm::vec3(3.0));
	eggs.back()->setRotation(rot);
    obj1.model = eggs.back();
	xLoc = obj1.mapLocation.x + 650;
	zLoc = obj1.mapLocation.z + 300;
	powerupUSS[xLoc / 50][zLoc / 50] = obj1;

	obj1.mapLocation = glm::vec3(-40, 37+4, 301);
	(*egg_mesh.begin())->getMaterial().diffuse.color = glm::vec3(.1,.4,.1);
    eggs.push_back(std::make_shared<SceneObject>(egg_mesh.begin(), egg_mesh.end()));
    eggs.back()->setTranslation(obj1.mapLocation);
    eggs.back()->setScale(glm::vec3(3.0));
	eggs.back()->setRotation(rot);
    obj1.model = eggs.back();
	xLoc = obj1.mapLocation.x + 650;
	zLoc = obj1.mapLocation.z + 300;
	powerupUSS[xLoc / 50][zLoc / 50] = obj1;


	//Missile locations
	obj1.mapLocation = glm::vec3(-72,-8+3,-124);
	(*egg_mesh2.begin())->getMaterial().diffuse.color = glm::vec3(.4,.1,.1);
	obj1.type = PowerupType::Missile;
    eggs.push_back(std::make_shared<SceneObject>(egg_mesh2.begin(), egg_mesh2.end()));
    eggs.back()->setTranslation(obj1.mapLocation);
    eggs.back()->setScale(glm::vec3(3.0));
	eggs.back()->setRotation(rot);
    obj1.model = eggs.back();
	xLoc = obj1.mapLocation.x + 650;
	zLoc = obj1.mapLocation.z + 300;
	powerupUSS[xLoc / 50][zLoc / 50] = obj1;

	obj1.mapLocation = glm::vec3(-159, -12+3, 35);
	(*egg_mesh2.begin())->getMaterial().diffuse.color = glm::vec3(.4,.1,.1);
	obj1.type = PowerupType::Missile;
    eggs.push_back(std::make_shared<SceneObject>(egg_mesh2.begin(), egg_mesh2.end()));
    eggs.back()->setTranslation(obj1.mapLocation);
    eggs.back()->setScale(glm::vec3(3.0));
	eggs.back()->setRotation(rot);
    obj1.model = eggs.back();
	xLoc = obj1.mapLocation.x + 650;
	zLoc = obj1.mapLocation.z + 300;
	powerupUSS[xLoc / 50][zLoc / 50] = obj1;

	obj1.mapLocation = glm::vec3(-569, 31+3, 146);
	(*egg_mesh2.begin())->getMaterial().diffuse.color = glm::vec3(.4,.1,.1);
	obj1.type = PowerupType::Missile;
    eggs.push_back(std::make_shared<SceneObject>(egg_mesh2.begin(), egg_mesh2.end()));
    eggs.back()->setTranslation(obj1.mapLocation);
    eggs.back()->setScale(glm::vec3(3.0));
	eggs.back()->setRotation(rot);
    obj1.model = eggs.back();
	xLoc = obj1.mapLocation.x + 650;
	zLoc = obj1.mapLocation.z + 300;
	powerupUSS[xLoc / 50][zLoc / 50] = obj1;

	obj1.mapLocation = glm::vec3(-373, 53+3, -271);
	(*egg_mesh2.begin())->getMaterial().diffuse.color = glm::vec3(.4,.1,.1);
	obj1.type = PowerupType::Missile;
    eggs.push_back(std::make_shared<SceneObject>(egg_mesh2.begin(), egg_mesh2.end()));
    eggs.back()->setTranslation(obj1.mapLocation);
    eggs.back()->setScale(glm::vec3(3.0));
	eggs.back()->setRotation(rot);
    obj1.model = eggs.back();
	xLoc = obj1.mapLocation.x + 650;
	zLoc = obj1.mapLocation.z + 300;
	powerupUSS[xLoc / 50][zLoc / 50] = obj1;

	obj1.mapLocation = glm::vec3(-115, 29+4, 120);
	(*egg_mesh2.begin())->getMaterial().diffuse.color = glm::vec3(.4,.1,.1);
	obj1.type = PowerupType::Missile;
    eggs.push_back(std::make_shared<SceneObject>(egg_mesh2.begin(), egg_mesh2.end()));
    eggs.back()->setTranslation(obj1.mapLocation);
    eggs.back()->setScale(glm::vec3(3.0));
	eggs.back()->setRotation(rot);
    obj1.model = eggs.back();
	xLoc = obj1.mapLocation.x + 650;
	zLoc = obj1.mapLocation.z + 300;
	powerupUSS[xLoc / 50][zLoc / 50] = obj1;

	obj1.mapLocation = glm::vec3(-17, 13+4, 199);
	(*egg_mesh2.begin())->getMaterial().diffuse.color = glm::vec3(.4,.1,.1);
	obj1.type = PowerupType::Missile;
    eggs.push_back(std::make_shared<SceneObject>(egg_mesh2.begin(), egg_mesh2.end()));
    eggs.back()->setTranslation(obj1.mapLocation);
    eggs.back()->setScale(glm::vec3(3.0));
	eggs.back()->setRotation(rot);
    obj1.model = eggs.back();
	xLoc = obj1.mapLocation.x + 650;
	zLoc = obj1.mapLocation.z + 300;
	powerupUSS[xLoc / 50][zLoc / 50] = obj1;

	/* Old level power up locations
	obj1.mapLocation = glm::vec3(-340, 1, -140);
    eggs.push_back(std::make_shared<SceneObject>(egg_mesh.begin(), egg_mesh.end()));
    eggs.back()->setTranslation(obj1.mapLocation);
    eggs.back()->setScale(glm::vec3(3.0));
    obj1.model = eggs.back();
    powerupList.push_back(obj1);

	xLoc = obj1.mapLocation.x;
	zLoc = obj1.mapLocation.z;
	powerupUSS[xLoc / 50][zLoc / 50] = obj1;

	obj1.mapLocation = glm::vec3(-38, 1, -367);
    eggs.push_back(std::make_shared<SceneObject>(egg_mesh.begin(), egg_mesh.end()));
    eggs.back()->setTranslation(obj1.mapLocation);
    eggs.back()->setScale(glm::vec3(3.0));
    obj1.model = eggs.back();
    powerupList.push_back(obj1);

	xLoc = obj1.mapLocation.x + 650;
	zLoc = obj1.mapLocation.z + 300;
	powerupUSS[xLoc / 50][zLoc / 50] = obj1;

	obj1.mapLocation = glm::vec3(346, 1, -201);
    eggs.push_back(std::make_shared<SceneObject>(egg_mesh.begin(), egg_mesh.end()));
    eggs.back()->setTranslation(obj1.mapLocation);
    eggs.back()->setScale(glm::vec3(3.0));
    obj1.model = eggs.back();
    powerupList.push_back(obj1);

	xLoc = obj1.mapLocation.x;
	zLoc = obj1.mapLocation.z;
	powerupUSS[xLoc / 50][zLoc / 50] = obj1;

	obj1.mapLocation = glm::vec3(161, 1, 68);
    eggs.push_back(std::make_shared<SceneObject>(egg_mesh.begin(), egg_mesh.end()));
    eggs.back()->setTranslation(obj1.mapLocation);
    eggs.back()->setScale(glm::vec3(3.0));
    obj1.model = eggs.back();
    powerupList.push_back(obj1);

	xLoc = obj1.mapLocation.x;
	zLoc = obj1.mapLocation.z;
	powerupUSS[xLoc / 50][zLoc / 50] = obj1;

	obj1.mapLocation = glm::vec3(-144, 1, 413);
    eggs.push_back(std::make_shared<SceneObject>(egg_mesh.begin(), egg_mesh.end()));
    eggs.back()->setTranslation(obj1.mapLocation);
    eggs.back()->setScale(glm::vec3(3.0));
    obj1.model = eggs.back();
    powerupList.push_back(obj1);

	xLoc = obj1.mapLocation.x;
	zLoc = obj1.mapLocation.z;
	powerupUSS[xLoc / 50][zLoc / 50] = obj1;

	obj1.mapLocation = glm::vec3(-345, 1, 207);
    eggs.push_back(std::make_shared<SceneObject>(egg_mesh.begin(), egg_mesh.end()));
    eggs.back()->setTranslation(obj1.mapLocation);
    eggs.back()->setScale(glm::vec3(3.0));
    obj1.model = eggs.back();
    powerupList.push_back(obj1);

	xLoc = obj1.mapLocation.x;
	zLoc = obj1.mapLocation.z;
	powerupUSS[xLoc / 50][zLoc / 50] = obj1;*/
}

void Scene::checkPowerups(float deltaT) {
	int xLoc;
	int zLoc;

	if (boostCam) {
		camVal -= deltaT;
	}

	if (camVal <= 0) {
		applyBoost(false);
	}

	//New powerup data structure traversal
	for (int i = 0; i < cooldownList.size(); i++) {
      xLoc = (cooldownList[i].mapLocation.x + 650) / 50;
      zLoc = (cooldownList[i].mapLocation.z + 300) / 50;

		if (cooldownList[i].cdTimer > 0) {
			cooldownList[i].cdTimer -= deltaT;
         
            if (powerupUSS[xLoc][zLoc].model != NULL) {
                glm::vec3 pos = powerupUSS[xLoc][zLoc].model->getTranslation();
                pos.y = cooldownList[i].mapLocation.y - cooldownList[i].cdTimer * 3;
                powerupUSS[xLoc][zLoc].model->setTranslation(pos);
            }
            else {
                //printf("Failed to find item at: %d, %d\n", xLoc, zLoc);
            }
		}

      if (cooldownList[i].cdTimer <= 0) {
         cooldownList.erase(cooldownList.begin() + i);
         powerupUSS[xLoc][zLoc].cdTimer = 0;
      }

	}

	std::vector<std::shared_ptr<KartControl> >::iterator controlKart = controlKarts.begin();
	int count = 0;
	//check the immediate vicinity, as well as all 8 nearby areas for powerups
	for (; controlKart != controlKarts.end(); controlKart++) {
		glm::vec3 kartPos = (*controlKart)->getPosition();
		xLoc = (kartPos.x + 650) / 50;
		zLoc = (kartPos.z + 300) / 50;
		
		for (int i = -1; i < 2; i++) {
			for (int j = -1; j < 2; j++) {
				if (xLoc+i < 0 || zLoc+j < 0) continue;
				
				if (powerupUSS[xLoc + i][zLoc + j].cdTimer <= 0 && glm::length(powerupUSS[xLoc + i][zLoc + j].mapLocation - kartPos) < 7
					&& powerupUSS[xLoc + i][zLoc + j].type != PowerupType::None) {
					//you are currently on a powerup!
					powerupUSS[xLoc + i][zLoc + j].cdTimer = 2;
					cooldownList.push_back(powerupUSS[xLoc + i][zLoc + j]);
					assignPowerup(count, powerupUSS[xLoc + i][zLoc + j]);
                    particles.createExplosion(kartPos);
					(*controlKart)->gotItem = true;
					break;
				}
			}
		}
		count++;
	}
}

void Scene::resetDistanceBehind() {
    distanceBehindCar = 20.0f;
}

void Scene::assignPowerup(int kartIndex, PowerupObj pu) {
	if (kartIndex >= controlKarts.size() || kartIndex > 100) {
		printf("Error, can't assign power up to kart with an invalid index!\n");
		return;
	}
	acquiredPowerup[kartIndex] = pu;
}

void Scene::applyBoost(bool enabled) {
    boostCam = enabled;
}

void Scene::resetPowerups() {
    for (int i = 0; i < 100; i++) {
        acquiredPowerup[i].type = PowerupType::None;
    }
}

PowerupType Scene::getKartPowerup(int index) {
	return acquiredPowerup[index].type;
}

void Scene::setKartPowerup(int index, PowerupType type) {
	acquiredPowerup[index].type = type;
}

void Scene::update(float deltaT) {
   std::vector<std::shared_ptr<SimpleAI> >::iterator simpleAI = simpleAIs.begin();
   for (; simpleAI != simpleAIs.end(); simpleAI++) {
      (*simpleAI)->update(deltaT);
   }
   std::vector<std::shared_ptr<KartControl> >::iterator controlKart = controlKarts.begin();
   for (; controlKart != controlKarts.end(); controlKart++) {
      (*controlKart)->update();

   }
   myBulletWorld->update(deltaT);

   checkPowerups(deltaT);
    //sceneKarts[0].
    //distanceBehindCar += controlKart.getLinearVelocity();
    
    if(rearCam) {
        distanceBehindCar = -20.0f;
    } else if(!boostCam){
        distanceBehindCar = (distanceBehindCar - 20.0) / 1.05;
        distanceBehindCar = distanceBehindCar + 20.0;
    }
    
    if(boostCam) {
        distanceBehindCar = (distanceBehindCar - 35.0) / 1.05;
        distanceBehindCar = distanceBehindCar + 35.0;
        angle = (angle - 7.5f) / 1.05;
        angle = angle + 7.5f;
        //angle = 5.0f;
    } else {
        angle = (angle - 10.0f) / 1.05;
        angle = angle + 10.0f;
    }
   
   // Simple game camera
   glm::vec3 position = sceneKarts[0].getTranslation();
   glm::vec3 facing = rotate(glm::vec3(0,0,1),sceneKarts[0].getRotation());
    if(facing.y > .5) {
        facing.y = .5;
    }
   glm::vec3 cam_pos = position + glm::vec3(0,angle,0) - facing * distanceBehindCar;
    /*if(cam_pos.y < 0.0) {
        cam_pos.y = 0;
    }
    if(cam_pos.z < 0.0) {
        cam_pos.z = 0;
    }*/
   glm::vec3 focus = position;
   cam.setPosition(cam_pos);
   cam.setLookAt(focus);
    
    
    //Particle Points stuff
    glm::vec3 direction = (glm::vec3(0,0,1)*getMainPlayer()->getRotation());
    particles.update(0.1, getMainPlayer()->getPosition(), direction);
    Points *p = new Points();
    for(int i=0; i< particles.particles.size(); i++)
    {
        if(particles.particles[i].active == 1)
        {
            p->addPoint(particles.particles[i].location);
        }
    }
    std::shared_ptr<Renderable> renderable(p);
    SceneObject points_object(renderable);
    points = points_object;
    
    //Rocket stuff
    rockets.update(deltaT, particles);
    
    StaticMesh rocket_mesh("assets/models/dinoEgg.obj", "assets/models");
	 (*rocket_mesh.begin())->getMaterial().diffuse.color = glm::vec3(0.8,0.1,0.1);
    // Jungle level map locations
    
    rocketModels.erase(rocketModels.begin(), rocketModels.end());    
    for(int i=0; i<rockets.rockets.size(); i++)
    {
        if(rockets.rockets[i].active == 1)
        {
        rocketModels.push_back(std::make_shared<SceneObject>(rocket_mesh.begin(), rocket_mesh.end()));
        rocketModels.back()->setTranslation(rockets.rockets[i].location);
        rocketModels.back()->setScale(glm::vec3(1.0));
        }
    }

    StaticMesh particle_mesh("assets/models/dinoEgg.obj", "assets/models");
    particleModels.erase(particleModels.begin(), particleModels.end());
    for(int i=0; i<particles.particles.size(); i++)
    {
        if(particles.particles[i].active == 1)
        {
            //set right color
            (*particle_mesh.begin())->getMaterial().diffuse.color = particles.getColor(particles.particles[i].life);

            particleModels.push_back(std::make_shared<SceneObject>(particle_mesh.begin(), particle_mesh.end()));
            particleModels.back()->setTranslation(particles.particles[i].location);
            particleModels.back()->setScale(glm::vec3(0.25));
        }
    }
}

void Scene::render() {
   renderer.setCamera(cam);

   prevViewProjMatrix = currentViewProjMatrix;
   currentViewProjMatrix = cam.getProjMatrix(1.3) * cam.getViewMatrix();

   std::vector<SceneObject>::iterator sceneKart = sceneKarts.begin();
   for (; sceneKart != sceneKarts.end(); sceneKart++) {
      renderer.enqueue(*sceneKart);
   }
   renderer.enqueue(track);
   //renderer.enqueue(points);
    
    std::vector<std::shared_ptr<SceneObject> >::iterator egg = eggs.begin();
    for(; egg != eggs.end(); egg++) {
        renderer.enqueue(**egg);
    }
    
    std::vector<std::shared_ptr<SceneObject> >::iterator rocket = rocketModels.begin();
    for(; rocket != rocketModels.end(); rocket++) {
        renderer.enqueue(**rocket);
    }
    
    std::vector<std::shared_ptr<SceneObject> >::iterator particle = particleModels.begin();
    for(; particle != particleModels.end(); particle++) {
        renderer.enqueue(**particle);
    }
    
    glPointSize(50.f);
    renderer.render();
}
const glm::mat4 &Scene::getPrevViewProjMatrix() {
	return prevViewProjMatrix;
}

const glm::mat4 &Scene::getViewProjMatrix() {
	return currentViewProjMatrix;
}

gl::texture Scene::getColorTex() {
	return renderer.getGBuffer().getColorTex();
}

gl::texture Scene::getPosTex() {
	return renderer.getGBuffer().getPosTex();
}