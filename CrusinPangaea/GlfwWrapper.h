/*
 * GlfwWrapper
 * Version 0.1
 * Richie Steigerwald
 */

#pragma once

#include <map>
#include <string>
#include <exception>

#include "Window.hpp"

#define GLFW_INCLUDE_GLCOREARB
#include <GL/glfw.h>

namespace glfw {

// An abstract class for a basic GLFW window. Extend this with a window that
// does something interesting
class Window : public gl::abstract_window {
   public:
   Window();
   explicit Window(const std::string &title);
   virtual ~Window() { }

   virtual void draw() = 0;
   virtual void update() { };
   virtual void event(gl::event &e) { };
   void run();
};


class GLFWException : public std::exception {
   std::string infoLog;
public:
   GLFWException(const std::string& str) throw() : infoLog(str) { }
   GLFWException() throw() { }
   
   virtual const char* what() const throw() {
      return (std::string("GLFWException: ")+infoLog).c_str();
   }
};


// A wrapper class for GLFW. Since GLFW is a single system resource we will
// have a singleton wrapper manage it.
class WindowManager {
   friend class Window;
   
   static Window *onlyWindow;

   // A map from GLFW window handles to windows
   static std::map<int, Window *> mWindows;

   // These are called by window when they are initialized and deleted
   void createWindow(const std::string &title, Window &window);

   // Callback functions call relevant functions on corresponding windows.
   static void GLFWCALL keyboardCallback(int key, int action);
   static gl::key_event map_key(int key);

   // Singleton boilerplate code
   public:
   static WindowManager& getInstance() {
      static WindowManager singleton; 
      return singleton;
   }

   // Call this to run glutMainLoop once all windows are initialized
   void runMainLoop();

   private:
   WindowManager();
   WindowManager(WindowManager const&);
   void operator=(WindowManager const&);
};

}