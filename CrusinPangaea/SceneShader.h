//
//  SceneShader.h
//  CrusinPangaea
//
//  Created by Richie on 2/17/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#pragma once

#include "Renderable.h"
#include "Material.h"

class SceneShader {
   protected:
      static void bind_contract(gl::vertex_array &vao, const gl::attribute &attr, const ShaderContract &con, const AttributeInfo &info) {
         vao.bind_attribute(attr, info.buffer, con.size, info.data_type, info.stride_bytes, info.start_bytes);
      }
   public:
      virtual bool canRenderRenderable(const Renderable &renderable) const = 0;
      virtual gl::vertex_array vaoForRenderable(const Renderable &renderable) const = 0;

      virtual void setModel(const glm::mat4 &model) = 0;
      virtual void setView(const glm::mat4 &view) = 0;
      virtual void setProjection(const glm::mat4 &proj) = 0;
      virtual void setMaterial(const Material &mat) = 0;
      virtual void use() = 0;
};