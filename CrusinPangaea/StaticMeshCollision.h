//
//  StaticMeshCollision.h
//  CrusinPangaea
//
//  Created by Richie on 2/19/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#pragma once

#include "Renderable.h"
#include "Material.h"
#include "StaticMesh.h"

#include "Program.hpp"
#include "VertexBuffer.hpp"
#include "VertexArray.hpp"

class StaticMeshCollision : public Renderable {
   gl::vertex_buffer vbo;
   gl::vertex_buffer ibo;
   Material mat;
   unsigned int numIndices;
   
   public:
   StaticMeshCollision(const StaticMesh &s);
   virtual bool getAttributeInfo(const ShaderContract &c, AttributeInfo &info) const;
   virtual DrawInfo getDrawInfo() const;
   virtual Material getMaterial() const { return mat; }
};