//
//  Platform.hpp
//  CrusinPangaea
//
//  Created by Richie on 2/14/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#pragma once

#include <GL/glew.h>
#if defined( _WIN32 )
	#include <GL/GL.h>
#elif defined( __linux__ )
	#include <GL/gl.h>
#elif defined( __APPLE__ )
   #include <OpenGL/gl.h>
#endif

namespace gl {
	typedef unsigned char uchar;
	typedef unsigned short ushort;
	typedef unsigned int uint;
	typedef unsigned long ulong;

	namespace type {
		enum type_t {
			Byte = GL_BYTE,
			UnsignedByte = GL_UNSIGNED_BYTE,
			Short = GL_SHORT,
			UnsignedShort = GL_UNSIGNED_SHORT,
			Int = GL_INT,
			UnsignedInt = GL_UNSIGNED_INT,
			Float = GL_FLOAT,
			Double = GL_DOUBLE,
		};
	}

	typedef GLint attribute;
	typedef GLint uniform;
}