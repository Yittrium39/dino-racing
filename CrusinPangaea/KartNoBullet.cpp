//
//  Player.cpp
//  CrusinPangaea
//
//  Created by Richie on 2/18/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#include "KartNoBullet.h"

static glm::quat offset(const glm::quat &q, const glm::vec3 &axis, float rad) {
   glm::vec3 ax = glm::normalize(axis);
   ax *= sinf(rad / 2.0f);
   float scalar = cosf(rad / 2.0f);
   glm::quat offset(scalar, ax.x, ax.y, ax.z);
   return glm::normalize(q * offset);
}

static glm::vec3 rotate(const glm::vec3 &v, const glm::quat &q) {
   glm::vec4 rotated =  q * glm::vec4(v, 1.0f);
   return glm::normalize(glm::vec3(rotated));
}

KartNoBullet::KartNoBullet() {
   turningLeft = false;
   turningRight = false;
   accelerating = false;
   braking = false;
   speed = 0;
   turn = 0;
}

void KartNoBullet::move(float deltaT) {
   deltaT *= 60;
   if (turningLeft) {
      turn += 0.05 * deltaT;
   }
   if (turningRight) {
      turn -= 0.05 * deltaT;
   }
   if (accelerating) {
      speed += .05 * deltaT;
   }
   if (braking) {
      speed -= .05 * deltaT;
   }
   turn -= deltaT * turn/5;
   speed -= deltaT * speed/20;
   rotation = offset(rotation, glm::vec3(0,1,0),0.2f * speed * turn * deltaT);
   position = position + rotation * glm::vec3(0,0,1) * speed * deltaT;
}