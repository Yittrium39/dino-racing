//
//  Scene.h
//  CrusinPangaea
//
//  Created by Richie on 2/17/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#pragma once

#include "SceneRenderer.h"
#include "Camera.h"
#include "StaticMesh.h"
#include "TrackDAO.h"

#include "KartControl.h"

#include "WorldNoBullet.h"

#include "SimpleAI.h"
#include "Spline.h"
#include "Particles.h"
#include "Rockets.h"


enum PowerupType { Boost, Missile, OilSlick, None};

struct PowerupObj {
    PowerupType type; //the type of power up
	glm::vec3 mapLocation; //the power up's location
    std::shared_ptr<SceneObject> model; //the power up's model
	float cdTimer; //the cooldown timer
    PowerupObj() : model(0), cdTimer(0) { }
};

class Scene {
   std::vector<SceneObject> sceneKarts;
   std::vector<std::shared_ptr<KartControl> > controlKarts;
   std::vector<std::shared_ptr<SimpleAI> > simpleAIs;
   
   SceneObject track;
   SceneObject physics_track;
   SceneObject points;
    std::vector<std::shared_ptr<SceneObject> > eggs;
    std::vector<std::shared_ptr<SceneObject> > rocketModels;
    std::vector<std::shared_ptr<SceneObject> > particleModels;

    
    Particles particles;
   
   SceneRenderer renderer;
   Camera cam;
   
   physics::World *myBulletWorld;
   
   Spline spline;
   SimpleAI *simpleAI;

   //maxPos = {28, Y, 274}
   //minPos = {-650,Y,-300}
   //Need USS to cover area of 700 x 600 for the jungle level
   //700 / 50 = 14
   //600 / 50 = 12
   std::vector<PowerupObj> powerupList; //Old Power up data structure
   
   PowerupObj powerupUSS[16][13]; //New data structure, uniform spatial subdivision :D
   std::vector<PowerupObj> cooldownList; //New data structure support list to track power ups on cooldown
   PowerupObj acquiredPowerup[100]; //One index per kart, keeps track of current powerup on kart

    
    bool rearCam;
    float distanceBehindCar;
    float angle;
   
   public:
    bool boostCam;
    Rockets rockets;
   Scene();
    float camVal;
   
   void generatePowerups();
   void checkPowerups(float deltaT);
   void update(float deltaT);
   void render();
   KartControl *getMainPlayer() { return &*controlKarts[0]; }
   const std::vector<std::shared_ptr<KartControl> > &getKarts() { return controlKarts; }
    void rearView(bool yn);
    void resetPowerups();
	void assignPowerup(int kartIndex, PowerupObj pu);
	void activatePowerup(int kartIndex);
    void applyBoost(bool enabled);
    void resetDistanceBehind();
	PowerupType getKartPowerup(int index);
	void setKartPowerup(int index, PowerupType type);

	glm::mat4 currentViewProjMatrix;
	glm::mat4 prevViewProjMatrix;

	const glm::mat4 &getPrevViewProjMatrix();
	const glm::mat4 &getViewProjMatrix();
	gl::texture getColorTex();
	gl::texture getPosTex();
};