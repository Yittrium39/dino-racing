//
//  WorldNoBullet.h
//  CrusinPangaea
//
//  Created by Richie on 2/23/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#pragma once

#include "World.h"
#include "KartNoBullet.h"
#include "BVH.hpp"
#include "Points.h"
#include <list>

class WorldNoBullet : public physics::World {
   std::list<KartNoBullet> mKarts;
   std::shared_ptr<BVH> mTrack;
   
   public:
   WorldNoBullet() { }
   virtual void setTrack(SceneObject *t);
   virtual physics::Kart *makeKart();
   virtual void update(float deltaT);
   
   std::shared_ptr<Renderable> debug;
};