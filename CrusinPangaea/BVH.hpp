#pragma once

#include <cmath>
#include <algorithm>
#include <limits>

#include <iostream>

template <typename t>
const t &max(const t &a, const t &b) {
   return (a > b) ? a : b;
}

template <typename t>
const t &min(const t &a, const t &b) {
   return (a > b) ? b : a;
}

namespace data {
   struct vec3 {
      union { float a[3]; struct { float x,y,z; }; };
      vec3() { }
      vec3(const glm::vec3 &v) { x = v.x, y = v.y, z = v.z; }
      vec3(float d, float e, float f) { x = d, y = e, z = f; }
      const float &operator[] (int i) const { return a[i]; }
      vec3 operator*(float f) { return vec3(f*x, f*y, f*z); }
      vec3 operator/(float f) { return vec3(x/f, y/f, z/f); }
      vec3 operator+(const vec3 &f) const { return vec3(x + f.x, y + f.y, z + f.z); }
      vec3 operator-(const vec3 &f) const { return vec3(x - f.x, y - f.y, z - f.z); }
      vec3 &operator*=(float f) { x*=f, y*=f, z*=f; return *this; }
      vec3 &operator+=(const vec3 &f) { return *this = *this + f; }
      float dot(const vec3 &f) const { return x*f.x + y*f.y + z*f.z; }
      float distance() const { return sqrtf(dot(*this)); }
      vec3 cross(const vec3 &v) const { return vec3(y*v.z - z*v.y, z*v.x - x*v.z, x*v.y - y*v.x); }
      vec3 normalize() const { float d = distance(); return vec3(x/d, y/d, z/d); }
      vec3 abs() const { return vec3(x>0?x:-x, y>0?y:-y, z>0?z:-z); }
   };
}

struct BVH {
#ifdef _WIN32
   const float THRESHOLD;
#else
   const float THRESHOLD = 0.00001;
#endif
   struct Sphere {
      data::vec3 position;
      float radius;
      data::vec3 triangle[3];
      static Sphere enclosingSphere(const Sphere &s1, const Sphere &s2) {
         // Check to see if one sphere completely surrounds the other
         float difference = (s2.position - s1.position).distance();
         if (difference + s2.radius <= s1.radius) {
            return Sphere(s1);
         }
         if (difference + s1.radius <= s2.radius) {
            return Sphere(s2);
         } 

         // Get the midpoint of their outermost edges
         data::vec3 direction = (s2.position - s1.position).normalize();
         data::vec3 rightEdge = s2.position + direction * s2.radius;
         data::vec3 leftEdge = s1.position - direction * s1.radius;
         
         Sphere enclosing;
         enclosing.position = (rightEdge + leftEdge) / (float)2; 
         enclosing.radius = (rightEdge - leftEdge).distance() / (float)2;
         return enclosing;
      }

      static Sphere makeSphere(const data::vec3 &position, const float radius) {
         Sphere s;
         s.position = position;
         s.radius = radius;
         return s;
      }

      static Sphere minimumBoundingSphere(const data::vec3 &p1, const data::vec3 &p2, const data::vec3 &p3) {
         Sphere s;
         // Calculate relative distances
         float A = (p1 - p2).distance();
         float B = (p2 - p3).distance();
         float C = (p3 - p1).distance();
         const data::vec3 *a = &p3, *b = &p1, *c = &p2;
         // Re-orient triangle
         if (A < B) std::swap(A, B), std::swap(a, b);
         if (B < C) std::swap(B, C), std::swap(b, c);
         if (A < B) std::swap(A, B), std::swap(a, b);
         // Check if obtuse
         if ((B*B) + (C*C) <= (A*A)) {
            // Use the longest diameter
            s.radius = A / (float)2;
            s.position = (*b + *c) / (float)2;
         } else {
            // Circumscribe
            float cos_a = (B*B + C*C - A*A) / (B*C*2);
            s.radius = A / (sqrt(1 - cos_a*cos_a)*2);
            data::vec3 alpha = *a - *c, beta = *b - *c;
            s.position = (beta * alpha.dot(alpha) - alpha * beta.dot(beta)).cross(alpha.cross(beta)) /
             (alpha.cross(beta).dot(alpha.cross(beta)) * (float)2) + *c;
         }
         return s;
      }
      Sphere() : position(0,0,0), radius(0) { }
   };
   
   
   template<typename Vec>
   class VecComp {
      int index;
      public:
      VecComp(int i) : index(i) { }
      bool operator()(const Vec &a, const Vec &b) const {
         return a.position[index] < b.position[index];
      }
   };

   // Create a temporary kd-tree with the sphere centers for generating the bvh
   void KDSort(Sphere *vecs, int numVecs) {
      if (numVecs <= 2)
         return;

      int v = numVecs - 1, split = 1;
      while (v >>= 1) split <<= 1;

      // Choose an axis
      data::vec3 average(0,0,0), stdDev(0,0,0);
      for (int i = 0; i < numVecs; i++) { average = average + vecs[i].position * 1.f / (float)numVecs; }
      for (int i = 0; i < numVecs; i++) { stdDev = stdDev + (vecs[i].position - average).abs() * 1.f / (float)numVecs; }
      int axis = std::max_element(&stdDev[0],&stdDev[3]) - &stdDev[0];
      VecComp<Sphere> cmp(axis);

      // Sort on that axis
      std::nth_element(&vecs[0], &vecs[split], &vecs[numVecs], cmp);
      KDSort(&vecs[0], split);
      KDSort(&vecs[split], numVecs - split);
   }
   
   struct Ray {
      data::vec3 start;
      data::vec3 dir;
   };
   
   struct Hit {
      float distance;
      data::vec3 position;
      data::vec3 normal;
      Hit() : distance(std::numeric_limits<float>::infinity()) { }
   };

   struct Node {
      bool isLeaf;
      Sphere sphere;
   };

   // The tree for our BVH
   Node *mNodes;
   int treeSize;

#ifdef _WIN32
   BVH (Sphere *spheres, int numSpheres) : THRESHOLD(0.00001) {
#else
   BVH(Sphere *spheres, int numSpheres) {
#endif
      KDSort(spheres, numSpheres);
      
      // Allocate space for the BVH
      int treeDepth = 1, s = numSpheres;
      while (s >>= 1) treeDepth++;
      treeSize = (2 << treeDepth);
      mNodes = new Node[treeSize];

      // Copy the spheres into the BVH
      int firstNode = (1 << treeDepth);
      for (int i = 0; i < firstNode; i++) {
         if (i < numSpheres)
            mNodes[firstNode + i].sphere = spheres[i];
         else {
            mNodes[firstNode + i].sphere.position = spheres[numSpheres - 1].position;
            mNodes[firstNode + i].sphere.radius = 0;
         }
         mNodes[firstNode + i].isLeaf = true;
        
         // Create parent sphere
         int node = firstNode + i;
         while (node % 2) {
            Node *lc = &mNodes[node - 1];
            Node *rc = &mNodes[node];
            mNodes[node >> 1].sphere = Sphere::enclosingSphere(lc->sphere, rc->sphere);
            mNodes[node >> 1].isLeaf = false;
            node >>= 1;
         }
      }
   }

   Sphere CastRay(Ray &r, Hit *sh) {
      Hit h;
      Sphere s;
      int stack[32], *sp = &stack[1];
      stack[0] = 0, stack[1] = 1;
      while(sp >= &stack[1]) {
         bool hit = SphereIntersectionTest(mNodes[*sp].sphere, r, &h);
         if(hit){
            bool tri_hit = false;
            if (mNodes[*sp].isLeaf) {
               tri_hit = TriangleIntersectionTest(mNodes[*sp].sphere, r, &h);
            }
            if (tri_hit && h.distance < sh->distance) {
               *sh = h;
               s = mNodes[*sp].sphere;
               while (*sp % 2) sp--;
               (*sp)++;
            } else {
               int first_child = (*sp) << 1;
               (*++sp) = first_child;
            }
         } else {
            while (*sp % 2) sp--;
            (*sp)++;
         }
      }
      return s;
   }
   
   float HeightTest(const data::vec3 &start) {
      Ray r;
      r.start = start;
      r.dir = data::vec3(0,-1,0);
      Hit h;
      float prevDepth = h.distance;
      Sphere s = CastRay(r, &h);
      if (h.distance < prevDepth) {
         return h.position.y;
      }
      return -std::numeric_limits<float>::infinity();
   }
   
   // Calculates if and where a ray intersects a sphere
   bool SphereIntersectionTest(Sphere s, Ray r, Hit *h) {
      data::vec3 o = r.start - s.position;
      data::vec3 dir = r.dir.normalize();

      float b = 2.f * dir.dot(o);
      float c = o.dot(o) - s.radius * s.radius;

      float determinant = b * b - 4 * c;   
      float dist = sqrtf(determinant) / 2.0f;

      if (determinant < THRESHOLD) {
         return false;
      } else {
         float d1 = -b / 2.f + dist,  d2 = -b / 2.f - dist;
         h->distance = (d1 < THRESHOLD || d2 < THRESHOLD) ? std::max(d1, d2) : std::min(d1, d2);
         h->position = r.start + dir * h->distance;
         h->normal = (h->position - s.position).normalize();
         return d1 >= THRESHOLD || d2 >= THRESHOLD;
      }   
   }
   /*
      // Calculates if and where a ray intersects a sphere
   bool TriangleIntersectionTest(Sphere s, Ray r, Hit *h) {
      data::vec3 o = r.start - s.position;
      data::vec3 dir = data::normalize(r.dir);

      float b = 2.f * data::dot(dir,o);
      float c = data::dot(o,o) - s.radius * s.radius;

      float determinant = b * b - 4 * c;   
      float dist = sqrtf(determinant) / 2.0f;

      if (determinant < THRESHOLD) {
         return false;
      } else {
         float d1 = -b / 2.f + dist,  d2 = -b / 2.f - dist;
         h->distance = (d1 < THRESHOLD || d2 < THRESHOLD) ? max(d1, d2) : min(d1, d2);
         h->position = r.start + dir * h->distance;
         h->normal = data::normalize(h->position - s.position);
         return d1 >= THRESHOLD || d2 >= THRESHOLD;
      }   
   }*/
   
   bool TriangleIntersectionTest(Sphere s ,Ray R, Hit *I) {
    data::vec3 u, v, n;             // triangle vectors
    data::vec3 dir, w0, w;          // ray vectors
    float     r, a, b;             // params to calc ray-plane intersect
 
    // get triangle edge vectors and plane normal
    u = s.triangle[1] - s.triangle[0];
    v = s.triangle[2] - s.triangle[0];
    n = u.cross(v);             // cross product
    if (n.x == 0 && n.y == 0 && n.z == 0)            // triangle is degenerate
        return false;                 // do not deal with this case
 
    dir = R.dir;             // ray direction vector
    w0 = R.start - s.triangle[0];
    a = -n.dot(w0);
    b = n.dot(dir);
    if (fabs(b) < THRESHOLD) {     // ray is parallel to triangle plane
        if (a == 0)                // ray lies in triangle plane
            return false;
        else return false;             // ray disjoint from plane
    }
 
    // get intersect point of ray with triangle plane
    r = a / b;
    if (r < 0.0)                   // ray goes away from triangle
        return false;                  // => no intersect
    // for a segment, also test if (r > 1.0) => no intersect
 
    I->position = R.start + dir * r;           // intersect point of ray and plane
    I->normal = n;
    I->distance = r;
 
    // is I inside T?
    float    uu, uv, vv, wu, wv, D;
    uu = u.dot(u);
    uv = u.dot(v);
    vv = v.dot(v);
    w = I->position - s.triangle[0];
    wu = w.dot(u);
    wv = w.dot(v);
    D = uv * uv - uu * vv;
 
    // get and test parametric coords
    float sp, t;
    sp = (uv * wv - vv * wu) / D;
    if (sp < 0.0 || sp > 1.0)        // I is outside T
        return 0;
    t = (uv * wu - uu * wv) / D;
    if (t < 0.0 || (sp + t) > 1.0)  // I is outside T
        return 0;
 
    return true;                      // I is in T
}
   
};

