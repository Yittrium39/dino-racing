#include "Application.h"

#include "RaceController.h"

Application::Application() {
	mControllers.push(std::shared_ptr<Controller>(new RaceController(*this)));
}

void Application::draw() {
   mControllers.top()->Render();
}

void Application::update() {
   Time currentTime = Timer::now();
   float deltaT = std::chrono::duration_cast<Seconds>(currentTime - mCurrentTime).count();
   mCurrentTime = currentTime;
   mControllers.top()->Update(deltaT);
}

void Application::event(gl::event &e) {
   mControllers.top()->Event(e);
}