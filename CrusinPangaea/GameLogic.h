//
//  GameLogic.h
//  CrusinPangaea
//
//  Created by Steven Livingston on 2/19/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#ifndef __CrusinPangaea__GameLogic__
#define __CrusinPangaea__GameLogic__

#pragma once
#include <vector>
#include <iostream>
#include <glm/glm.hpp>
#include "Spline.h"
#include "KartControl.h"
#include "Scene.h"

enum GameState { WaitState, CountDownState, RaceState, EndState };


struct kartInfo {
    int prevPoint; //Closest spline point to the kart
    int nextPoint; //Next spline point to advance to
    int lap;
    int place; //1st, 2nd, 3rd, etc.
    int distance;
    int pointsCrossed; //number of spline points crossed
    KartControl *kart;
   
   kartInfo() : prevPoint(30), nextPoint(0), lap(0), place(1), distance(0), pointsCrossed(0) { }
   bool operator<(const kartInfo &other) const { return this->distance < other.distance; }
};

class GameLogic {
public:
	std::vector<glm::vec3> spline;
    int goingBackwards;
    void setSpline(std::vector<glm::vec3> spline);
    void createKartList(int numKarts, Scene &curScene);
	void checkKarts();
	void checkTriggeredPowerups();
    void updatePositions();
    void updatePoints();
    std::string getPlayerPlace();
    std::string getPlayerLap();
    int getPlayerLapInt();
    int getPlayerPlaceInt();
	int getKartPlaceInt(int index);
    kartInfo getKartAtPlace(int place);
private:
    std::vector<kartInfo> kartList;
	Scene *scene;
};

#endif
