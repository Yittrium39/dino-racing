//
//  Text.cpp
//  CrusinPangaea
//
//  Created by Richie on 2/18/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#include "Font.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <exception>

#include "Panel.h"

#include <iostream>

Font::Font(const std::string &file, int faceSize) {
   // load the font face
   if(FT_New_Face(getFTLibrary(), file.c_str(), 0, &face)) {
      throw FontException("Could not open font");
   }
   
   this->faceSize = faceSize;
   FT_Set_Pixel_Sizes(face, 0, faceSize);

   FT_GlyphSlot g = face->glyph;
   int w = 0;
   int h = 0;
   for(int i = 32; i < 128; i++) {
      if(FT_Load_Char(face, i, FT_LOAD_RENDER)) {
         printf("Loading character %c failed!\n", i);
         continue;
      }
 
      w += g->bitmap.width;
      h = std::max(h, g->bitmap.rows);
   }
   atlasWidth = w;
   atlasHeight = h;
      
   fontFace.SetFilters(gl::Filter::Linear, gl::Filter::Linear);
   
   GLint uplast;
   glGetIntegerv(GL_UNPACK_ALIGNMENT, &uplast);
   glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
   fontFace.Image2D(0, gl::DataType::UnsignedByte, gl::Format::Red, w, h, gl::InternalFormat::R8);
   int x = 0;
 
   for(int i = 32; i < 128; i++) {
      if(FT_Load_Char(face, i, FT_LOAD_RENDER))
         continue;
      
      fontFace.SubImage2D(g->bitmap.buffer, gl::DataType::UnsignedByte, gl::Format::Red, x, 0, g->bitmap.width, g->bitmap.rows);
      
      c[i].ax = g->advance.x >> 6;
      c[i].ay = g->advance.y >> 6;
 
      c[i].bw = g->bitmap.width;
      c[i].bh = g->bitmap.rows;
 
      c[i].bl = g->bitmap_left;
      c[i].bt = g->bitmap_top;
 
      c[i].tx = (float)x / w;
 
      x += g->bitmap.width;
   }
   glPixelStorei(GL_UNPACK_ALIGNMENT, uplast);
}



void Font::drawGlyph(int vWidth, int vHeight, const std::string &string, int x, int y) {
   getGlyphProgram().use();
   fontFace.bind(0);
   std::string::const_iterator it;

   for (it = string.begin(); it != string.end(); it++) {   
      float pixelPositionX = (x + c[*it].bl) - vWidth / 2.0;
      float pixelPositionY = vHeight / 2 - y + c[*it].bt;
      float width = c[*it].bw;
      float height = c[*it].bh;
      
      float screenPositionX = 2 * pixelPositionX / (float)vWidth;
      float screenPositionY = 2 * pixelPositionY / (float)vHeight;
      float screenWidth = 2 * width / (float)vWidth;
      float screenHeight = 2 * height / (float)vHeight;
   
      getGlyphProgram().set_uniform_vec4(getGlyphProgram().get_uniform("rect"),
       glm::value_ptr(glm::vec4(screenPositionX,screenPositionY,screenWidth, screenHeight)));
      getGlyphProgram().set_uniform_vec4(getGlyphProgram().get_uniform("texpos"),
       glm::value_ptr(glm::vec4(c[*it].tx, 0, c[*it].bw/atlasWidth, c[*it].bh/atlasHeight)));
      getGlyphVAO().draw_arrays(gl::primitive::triangle_fan, 0, 4);

      x += c[*it].ax;
      y += c[*it].ay;
   }
}


/***********
 * SHADERS *
 ***********/

gl::program Font::makeGlyphProgram() {
   gl::program program(gl::shader(gl::shader_type::vertex, GLSL(
      // Vertex Shader
      in vec2 coord;
      out vec2 texcoord;
      uniform vec4 rect; // upper left corner x, upper left corner y, width, height
      void main(void) {
         vec2 position;
         position.x = coord.x * rect.z + rect.x;
         position.y = rect.y - coord.y * rect.w;
         texcoord = coord;
         gl_Position = vec4(position, 0, 1);
      }
   )), gl::shader(gl::shader_type::fragment, GLSL(
      // Fragment Shader
      in vec2 texcoord;
      out vec4 outColor;
      uniform vec4 texpos;
      uniform sampler2D tex;
 
      void main(void) {
         outColor = vec4(0,0,0,texture(tex, texpos.zw * texcoord + texpos.xy).r);
      }
   )));
   program.set_uniform(program.get_uniform("tex"), 0);
   getGlyphVAO().bind_attribute(program.get_attribute("coord"), getGlyphVBO(), 2, gl::type::Float, 0, 0);
   return program;
}

/****************************************
 * PRIVATE SINGLETON RESOURCE ACCESSORS *
 ****************************************/
gl::program &Font::getGlyphProgram() {
   static gl::program program = makeGlyphProgram();
   return program;
}

/**
 * getGlyphVAO
 * this is the accessor for a lazily initialized static VAO
 */
gl::vertex_array &Font::getGlyphVAO() {
   static gl::vertex_array vao = makeGlyphVAO(getGlyphVBO());
   return vao;
}

/**
 * getGlyphVBO
 * this is the accessor for a lazily initialized static VBO
 */
gl::vertex_buffer &Font::getGlyphVBO() {
   static gl::vertex_buffer vbo = makeGlyphVBO();
   return vbo;
}

FT_Library &Font::getFTLibrary() {
   static FT_Library ft = makeFTLibrary();
   return ft;
}

/****************************
 * LAZY STATIC INITIALIZERS *
 ****************************/
FT_Library Font::makeFTLibrary() {
   FT_Library ft;
   if (FT_Init_FreeType(&ft)) {
      throw FontException("Could not init freetype library");
   }
   return ft;
}

gl::vertex_buffer Font::makeGlyphVBO() {
   float vertices[] = { 0.0f, 1.0f,
                        1.0f, 1.0f,
                        1.0f, 0.0f,
                        0.0f, 0.0f};
   gl::vertex_buffer vbo(vertices, sizeof(vertices), gl::buffer_usage::static_draw);
   return vbo;
}

/**
 * makeGlyphVAO
 * this is the static initializer for the glyph VAO
 */
gl::vertex_array Font::makeGlyphVAO(gl::vertex_buffer &vbo) {
   gl::vertex_array vao;
   return vao;
}