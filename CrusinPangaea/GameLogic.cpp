//
//  GameLogic.cpp
//  CrusinPangaea
//
//  Created by Steven Livingston on 2/19/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#include "GameLogic.h"
#include <algorithm>
#include <cmath>

//Gets the spline for the map and creates the list of karts
void GameLogic::createKartList(int numKarts, Scene &curScene) {
   std::vector<std::shared_ptr<KartControl> >::const_iterator kart = curScene.getKarts().begin();
   for (int i = 1 ; kart != curScene.getKarts().end(); kart++, i++) {
      kartInfo player;
      player.kart = &**kart;
      player.place = i;
       player.nextPoint = 0;
       player.prevPoint = static_cast<int>(spline.size()) - 1;
       player.lap = 1;
       player.pointsCrossed = 0;
	   player.kart->boostUsed = false;
	   player.kart->missileFired = false;
	   player.kart->missileHit = false;
	   player.kart->gotItem = false;
	   player.kart->powerup = false;
      kartList.push_back(player);
   }
   scene = &curScene;
}

std::string GameLogic::getPlayerPlace() {
    char buf[5];
    sprintf(buf, "%d", kartList[0].place);
    return buf;
}
int GameLogic::getPlayerPlaceInt() {
    return kartList[0].place;
}
int GameLogic::getKartPlaceInt(int index) {
	return kartList[index].place;
}
std::string GameLogic::getPlayerLap() {
    char buf[5];
    sprintf(buf, "%d", kartList[0].lap);
    return buf;
}
int GameLogic::getPlayerLapInt() {
    return kartList[0].lap;
}

kartInfo GameLogic::getKartAtPlace(int place) {
    for(int i=0; i<kartList.size();i++)
    {
        if(kartList[i].place == place)
        {
            return kartList[i];
        }
    }
    //printf("WARNING - no kart found at place %d\n", place);
    return kartList[0];
}


void GameLogic::setSpline(std::vector<glm::vec3> splineu) {
    spline = splineu;
}

//Check if any kart is off course. If so, place it at its prevPoint
void GameLogic::checkKarts() {
	for (int i = 0; i < kartList.size(); i++) {
        //If we have fallen, set us back on the track!
		if (fabs(spline[kartList[i].prevPoint].y) - fabs(kartList[i].kart->getPosition().y) < -20 &&
			spline[kartList[i].prevPoint].y > kartList[i].kart->getPosition().y) {
			kartList[i].kart->reset = true;       
		}
        //If reset is set to true, reset that kart
        if (kartList[i].kart->reset == true)        {
            
            kartList[i].kart->reset = false;
            kartList[i].kart->setPosition(spline[kartList[i].prevPoint]+glm::vec3(0,1,0));
			kartList[i].kart->setAccelerating(false, true);
            
            glm::vec3 direction = glm::normalize(spline[kartList[i].nextPoint] - spline[kartList[i].prevPoint]);

            float rad = acos(glm::dot(
                                            glm::vec3(0.0,0.0,1.0),
                                            direction));
            
            glm::vec3 axis = glm::cross(
                                              glm::vec3(0.0,0.0,1.0),
                                              direction);
            
            glm::vec3 ax = glm::normalize(axis);
            ax *= sinf(rad / 2.0f);
            float scalar = cosf(rad / 2.0f);
            glm::quat offset(scalar, ax.x, ax.y, ax.z);
            kartList[i].kart->setRotation(offset);
            
            kartList[i].kart->setDrop(true);
        }
	}
}

void GameLogic::checkTriggeredPowerups() {
	for (int i = 0; i < kartList.size(); i++) {
		if (kartList[i].kart->powerup) {
			//B-B-B-BOOSTER!
			if(scene->getKartPowerup(i) == PowerupType::Boost) {
				if (i == 0) {
					scene->applyBoost(true);
					scene->camVal = .75;
				}
				kartList[i].kart->setPowerUpBoost(true);
				kartList[i].kart->boostUsed = true;
				scene->setKartPowerup(i, PowerupType::None);
			}
			//FIYAH ZE MISSILES!
			if(scene->getKartPowerup(i) == PowerupType::Missile) {
				if(getKartPlaceInt(i) != 1) {
					kartList[i].kart->missileFired = true;
                    glm::vec3 dir = glm::vec3(0,0,1)*scene->getMainPlayer()->getRotation();
                    scene->rockets.addRocket(kartList[i].kart->getPosition(), dir, 4.0, getKartAtPlace(getKartPlaceInt(i)-1).kart);
					scene->setKartPowerup(i, PowerupType::None);
                }
			}
		}
		kartList[i].kart->enablePowerup(false);
	}
}

//Update the spline points for each kart and determine who is in what place
void GameLogic::updatePositions() {
    int temp;
    
    //calculate distance travelled for each kart
    for (int i = 0; i < kartList.size(); i++) {
        kartList[i].distance = (static_cast<int>(spline.size()) * kartList[i].lap) + kartList[i].pointsCrossed;
    }
   
    //get place for each kart
    for (int i = 0; i < kartList.size(); i++) {
        for (int j = 0; j < kartList.size(); j++) {
            if (i == j) {
                continue;
            }
            if (kartList[i].distance > kartList[j].distance && kartList[i].place > kartList[j].place) {
                temp = kartList[i].place;
                kartList[i].place = kartList[j].place;
                kartList[j].place = temp;
            }
            if (kartList[i].distance == kartList[j].distance) {
                float kart1Dist = glm::length(spline[kartList[i].nextPoint] - kartList[i].kart->getPosition());
                float kart2Dist = glm::length(spline[kartList[j].nextPoint] - kartList[j].kart->getPosition());
                
                if (kart1Dist < kart2Dist && kartList[i].place > kartList[j].place) {
                    temp = kartList[i].place;
                    kartList[i].place = kartList[j].place;
                    kartList[j].place = temp;
                }
            }
        }
    }
}

//update prevPoint, nextPoint, and lap according to kart's distance to spline points
void GameLogic::updatePoints() {
    for (int i = 0; i < kartList.size(); i++) {
        float nextDist = glm::length(spline[kartList[i].nextPoint] - kartList[i].kart->getPosition());
        float prevDist = glm::length(spline[kartList[i].prevPoint] - kartList[i].kart->getPosition());
        
        if (nextDist < prevDist) {
            if(i == 0) {
                goingBackwards = 0;
            }
            kartList[i].nextPoint++;
            kartList[i].prevPoint++;
            kartList[i].pointsCrossed++;
        }
        if (kartList[i].pointsCrossed >= static_cast<int>(spline.size())) {
            kartList[i].pointsCrossed = 0;
            kartList[i].lap++;
        }
        if (kartList[i].nextPoint >= spline.size()) {
            kartList[i].nextPoint = 0;
        }
        if (kartList[i].prevPoint >= spline.size()) {
            kartList[i].prevPoint = 0;
        }
        
        //Going backwards.
        if(kartList[i].prevPoint-1 >= 0) {
            float prevPrevDist = glm::length(spline[kartList[i].prevPoint-1] - kartList[i].kart->getPosition());
            if(prevPrevDist < prevDist) {
                if(i == 0) {
                    goingBackwards++;
                }
                kartList[i].prevPoint--;
                kartList[i].nextPoint--;
                kartList[i].pointsCrossed--;
            }
            if(kartList[i].nextPoint < 0) {
                kartList[i].nextPoint = static_cast<int>(spline.size())-1;
            }
        }
        else if(kartList[i].prevPoint-1 < 0) {
            float prevPrevDist = glm::length(spline[spline.size()-1] - kartList[i].kart->getPosition());
            if(prevPrevDist < prevDist) {
                kartList[i].prevPoint = static_cast<int>(spline.size())-1;
                kartList[i].nextPoint--;
                kartList[i].pointsCrossed--;
            }
        }
    }
}