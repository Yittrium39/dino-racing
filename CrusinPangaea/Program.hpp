#pragma once

#include "Platform.hpp"
#include "Factory.hpp"
#include "Shader.hpp"
#include <exception>

namespace gl {

	class LinkException : public std::exception {
		std::string infoLog;
	public:
		LinkException(const std::string& str) throw() : infoLog(str) { }
		~LinkException() throw() { }

		virtual const char* what() const throw() {
			return (std::string("LinkException: ")+infoLog).c_str();
		}
	};

	class program {
		GLuint obj;
		factory &factory() {
			static class factory factory;
			return factory;
		}
	public:

		program() {
			obj = factory().create(glCreateProgram(), glDeleteProgram);
		}

		program(const program& other ) {
			factory().copy(other.obj, obj);
		}

		program(const shader& vertex) {
			obj = factory().create(glCreateProgram(), glDeleteProgram);
			Attach(vertex);
			Link();
			glUseProgram(obj);
		}

		program(const shader& vertex, const shader& fragment) {
			obj = factory().create(glCreateProgram(), glDeleteProgram);
			Attach(vertex);
			Attach(fragment);
			Link();
			glUseProgram(obj);
		}

		~program() {
			factory().destroy(obj);
		}

		operator GLuint() const {
			return obj;
		}

		const program& operator=(const program& other) {
			factory().copy( other.obj, obj, true );
			return *this;
		}

		void Attach(const shader& shader) {
			glAttachShader(obj, shader);
		}

		void Link() {
			GLint res;

			glLinkProgram(obj);
			glGetProgramiv(obj, GL_LINK_STATUS, &res);

			if (res == GL_FALSE)
				throw LinkException(get_info_log());
		}

		std::string get_info_log() {
			GLint res;
			glGetProgramiv(obj, GL_INFO_LOG_LENGTH, &res);

			if (res > 0) {
				std::string infoLog(res, 0);
				glGetProgramInfoLog(obj, res, &res, &infoLog[0]);
				return infoLog;
			} else {
				return "";
			}
		}

		attribute get_attribute(const std::string &name) const {
			return glGetAttribLocation(obj, name.c_str());
		}

		uniform get_uniform(const std::string &name) const {
			return glGetUniformLocation(obj, name.c_str());
		}

		void set_uniform(const uniform &uniform, int value) {
			glUniform1i(uniform, value);
		}

		void set_uniform(const uniform &uniform, float value) {
			glUniform1f(uniform, value);
		}

		void set_uniform(const uniform &uniform, const float* values, uint count) {
			glUniform1fv(uniform, count, values);
		}
      
      void set_uniform_vec2(const uniform &uniform, const GLfloat *value) {
         glUniform2fv(uniform, 1, value);
      }
      
      void set_uniform_vec3(const uniform &uniform, const GLfloat *value) {
         glUniform3fv(uniform, 1, value);
      }
      
      void set_uniform_vec4(const uniform &uniform, const GLfloat *value) {
         glUniform4fv(uniform, 1, value);
      }
      
      void set_uniform_mat2(const uniform &uniform, const GLfloat *value) {
         glUniformMatrix2fv(uniform, 1, GL_FALSE, value);
      }
      
      void set_uniform_mat3(const uniform &uniform, const GLfloat *value) {
         glUniformMatrix3fv(uniform, 1, GL_FALSE, value);
      }
      
      void set_uniform_mat4(const uniform &uniform, const GLfloat *value) {
         glUniformMatrix4fv(uniform, 1, GL_FALSE, value);
      }
      
      void use() {
         glUseProgram(obj);
      }
	};

}