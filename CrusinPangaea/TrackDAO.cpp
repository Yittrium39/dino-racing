
#include "TrackDAO.h"
#include "StaticMesh.h"
#include "SkinMesh.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>

using namespace glm;
using namespace std;

JSONNode &TrackDAO::getRootNode(const std::string &file) {
   static std::map<const std::string, std::shared_ptr<JSONNode> > levelCache;
   
   std::map<const std::string, std::shared_ptr<JSONNode> >::iterator it;
   it = levelCache.find(file);
   if (it == levelCache.end()) {
      levelCache[file] = JSONLoader::load(std::string("assets/levels/")+file);
   }
   return *levelCache[file];
}

SceneObject TrackDAO::loadPlayer(const std::string &file) {
   JSONNode &rootNode = getRootNode(file);
   
   // Load the racer mesh
   JSONNode *playerNode = JSONLoader::getNode(&rootNode, "player");
   std::string meshFile = JSONLoader::getString(playerNode, "mesh");
   std::string textureDir = JSONLoader::getString(playerNode, "textures");
   
   StaticMesh myRacer(meshFile, textureDir);
   SkinMesh *winningRacer = new SkinMesh("assets/models/triceraWinning.dae", "assets/models");

   // Create an instance for the racer
   SceneObject player(myRacer.begin(), myRacer.end());
   player.setTranslation(JSONLoader::getVec3(playerNode, "translate", vec3(0, 0, 0)));
   player.setScale(JSONLoader::getVec3(playerNode, "scale", vec3(1.0)));
   player.setRotation(glm::normalize(JSONLoader::getVec4(playerNode, "rotate", vec4(0, 0, 0, 0))));

   // Create an instance for an animated racer
   /* SceneObject player(winningRacer->begin(), winningRacer->end());
   player.setTranslation(JSONLoader::getVec3(playerNode, "translate", vec3(0, 0, 0)));
   player.setScale(JSONLoader::getVec3(playerNode, "scale", vec3(1.0)));
   player.setRotation(glm::normalize(JSONLoader::getVec4(playerNode, "rotate", vec4(0, 0, 0, 0))));
   player.setScale(glm::vec3(2.5)); */

   return player;
}

std::vector<SceneObject> TrackDAO::loadOpponents(const std::string &file) {
   JSONNode &rootNode = getRootNode(file);
   std::vector<SceneObject> objects;
   
   std::vector<JSONNode*> opponentNodes = JSONLoader::getNodes(&rootNode,"opponents");
   std::vector<JSONNode*>::iterator nodeIter = opponentNodes.begin();
   
   // Load the opponent mesh
   std::string meshFile = JSONLoader::getString(*nodeIter, "mesh");
   std::string textureDir = JSONLoader::getString(*nodeIter, "textures");
   StaticMesh myRacer(meshFile, textureDir);

   for (; nodeIter != opponentNodes.end(); nodeIter++) {
      // Create an instance for the mesh
      SceneObject player(myRacer.begin(), myRacer.end());
      player.setTranslation(JSONLoader::getVec3(*nodeIter, "translate", vec3(0, 0, 0)));
      player.setScale(JSONLoader::getVec3(*nodeIter, "scale", vec3(1.0)));
      player.setRotation(glm::normalize(JSONLoader::getVec4(*nodeIter, "rotate", vec4(0, 0, 0, 0))));
      
      objects.push_back(player);
   }
   
   return objects;
}


SceneObject TrackDAO::loadTrack(const std::string &file) {
   JSONNode &rootNode = getRootNode(file);
   
   // Load the track mesh
   JSONNode *trackNode = JSONLoader::getNode(&rootNode, "track");
   std::string meshFile = JSONLoader::getString(trackNode, "mesh");
   std::string textureDir = JSONLoader::getString(trackNode, "textures");
   
   std::shared_ptr<StaticMesh> myTrack(new StaticMesh(meshFile, textureDir));
   
   // Create an instance for the track
   SceneObject track(myTrack->begin(), myTrack->end());
   
   // Set the mesh of the track. We do this so we can get the vertices later to compute the height
   // map
   track.setMesh(myTrack);
   
   track.setRotation(glm::normalize(JSONLoader::getVec4(trackNode, "rotate", vec4(0, 0, 0, 0))));
   track.setTranslation(JSONLoader::getVec3(trackNode, "translate", vec3(0, 0, 0)));
   track.setScale(JSONLoader::getVec3(trackNode, "scale", vec3(1.0)));
   
   return track;
}


std::string TrackDAO::loadSongFile(const std::string &file) {
   JSONNode &rootNode = getRootNode(file);
   JSONNode *musicNode =  JSONLoader::getNode(&rootNode, "music");
   return  JSONLoader::getString(musicNode, "file");
}