#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <iostream>

#include "SkinMesh.h"

using namespace std;
using namespace Assimp;

/****
 * Helper functions and classes
 */

// auxillary function
void copyAImat(const aiMatrix4x4 *from, glm::mat4 &to) {
	to[0][0] = from->a1; to[1][0] = from->a2;
	to[2][0] = from->a3; to[3][0] = from->a4;
	to[0][1] = from->b1; to[1][1] = from->b2;
	to[2][1] = from->b3; to[3][1] = from->b4;
	to[0][2] = from->c1; to[1][2] = from->c2;
	to[2][2] = from->c3; to[3][2] = from->c4;
	to[0][3] = from->d1; to[1][3] = from->d2;
	to[2][3] = from->d3; to[3][3] = from->d4;
}

void SkinMesh::VertexBoneData::addBoneData(unsigned int boneID, float weight) {
	for (unsigned int i = 0; i < sizeof(IDs)/sizeof(IDs[0]); i++) {
		if (weights[i] == 0.0) {
			IDs[i]     = boneID;
			weights[i] = weight;
			return;
		}        
	}
}

class MeshException : public std::exception {
   std::string infoLog;
public:
   MeshException(const std::string& str) throw() : infoLog(str) { }
   MeshException() throw() { }
   
   virtual const char* what() const throw() {
      return (std::string("MeshException: ")+infoLog).c_str();
   }
};

struct SkinVertex {
   glm::vec3 position;
   glm::vec3 normal;
   glm::vec2 texture_coord;
   glm::ivec4 boneIndex;
   glm::vec4 boneWeight;
};

// StaticMesh objects may have one or more sub-meshes
// each sub-mesh gets a MeshEntry
class SkinMeshEntry : public Renderable {
   gl::vertex_buffer vbo;
   gl::vertex_buffer ibo;
   unsigned long num_indices;
   Material mat;
public:
   virtual bool getAttributeInfo(const ShaderContract &c, AttributeInfo &info) const;
   virtual DrawInfo getDrawInfo() const;
   virtual Material getMaterial() const { return mat; }
   virtual Material &getMaterial() { return mat; }
   bool getBoneTransforms(float TimeInSeconds, vector<glm::mat4>& Transforms) {
	   currentSkinMesh->boneTransform(TimeInSeconds, Transforms);
	   return true;
   }

   SkinMeshEntry(std::vector<SkinVertex> vertices, std::vector<Index> indices, Material &m) : mat(m) {
      vbo = gl::vertex_buffer(&vertices[0], vertices.size() * sizeof(SkinVertex), gl::buffer_usage::static_draw);
      ibo = gl::vertex_buffer(&indices[0], indices.size() * sizeof(Index), gl::buffer_usage::static_draw);
      num_indices = indices.size();
   }
};

/**
 * StaticMesh functions
 */


SkinMesh::SkinMesh(const std::string &meshFile, const std::string &textureDir) {
	m_NumBones = 0;
   // Create an instance of the Importer class
   Assimp::Importer *importer = new Assimp::Importer();
   
   // And have it read the given file with some postprocessing
   mScene = importer->ReadFile(meshFile,
                                            aiProcess_CalcTangentSpace       |
                                            aiProcess_Triangulate            |
											aiProcess_LimitBoneWeights		 |
											aiPrimitiveType_LINE             |
                                            aiPrimitiveType_POINT            |
                                            aiProcess_JoinIdenticalVertices  |
                                            aiProcess_FlipUVs                |
                                            aiProcess_SortByPType);

   // If the import failed, report it
   if(!mScene) {
      throw MeshException(importer->GetErrorString());
   }

   copyAImat(&mScene->mRootNode->mTransformation.Inverse(), m_GlobalInverseTransform);
   
   // Load the textures
   loadMaterialsFromScene(mScene, textureDir);
   
   // Now load the meshes
   loadMeshesFromScene(mScene);
   
   // We're done. Everything will be cleaned up by the importer destructor
}

SkinMesh::~SkinMesh() {
}

// This function loops through the meshes of a loaded assimp model and loads
// each one
void SkinMesh::loadMeshesFromScene(const aiScene *pScene) {
   for (int i = 0 ; i < pScene->mNumMeshes ; i++) {
      const aiMesh* paiMesh = pScene->mMeshes[i];
      mRenderables.push_back(loadMesh(paiMesh));
   }
}


// Gets vertices and indices, makes a vbo and an ibo
std::shared_ptr<Renderable> SkinMesh::loadMesh(const aiMesh* pMesh) {

   std::vector<Index> indices = loadMeshIndices(pMesh);
   std::vector<SkinVertex> vertices = loadMeshVertices(pMesh);
   
   cout << "Animations: " << mScene->mNumAnimations << endl;

   bones.resize(vertices.size());
   loadBones(pMesh);
   for (int i = 0; i < bones.size(); i++) {
	   vertices[i].boneIndex = glm::ivec4(bones[i].IDs[0], bones[i].IDs[1], bones[i].IDs[2], bones[i].IDs[3]);
	   vertices[i].boneWeight = glm::vec4(bones[i].weights[0], bones[i].weights[1], bones[i].weights[2], bones[i].weights[3]);
   }

   unsigned int materialIndex = pMesh->mMaterialIndex;
   Material &m = *mMaterials[materialIndex];
   
   std::shared_ptr<Renderable> mesh = std::shared_ptr<Renderable>(new SkinMeshEntry(vertices, indices, m));
   mesh->currentSkinMesh = this;

   return mesh;
}


std::vector<SkinVertex> SkinMesh::loadMeshVertices(const aiMesh *pMesh) const {
   bool hasTextureCoords = pMesh->HasTextureCoords(0);
   bool hasNormals = pMesh->HasNormals();

   std::vector<SkinVertex> vertices;
   // Add all of the vertices to the vertices vector
   for (int i = 0 ; i < pMesh->mNumVertices ; i++) {
      SkinVertex v;
      v.position = glm::vec3(pMesh->mVertices[i].x, pMesh->mVertices[i].y, pMesh->mVertices[i].z);
      
      // for compatibility, populate the main vertices vector
      this->vertices.push_back(v.position);
      
      if (hasNormals)
         v.normal = glm::vec3(pMesh->mNormals[i].x, pMesh->mNormals[i].y, pMesh->mNormals[i].z);
      if (hasTextureCoords)
         v.texture_coord = glm::vec2(pMesh->mTextureCoords[0][i].x, pMesh->mTextureCoords[0][i].y);
      vertices.push_back(v);
   }
   
   return vertices;
}

void SkinMesh::loadBones(const aiMesh *pMesh) {
	for (unsigned int i = 0 ; i < pMesh->mNumBones ; i++) {                
		unsigned int BoneIndex = 0;        
		std::string BoneName(pMesh->mBones[i]->mName.data);
        
		if (m_BoneMapping.find(BoneName) == m_BoneMapping.end()) {
			BoneIndex = m_NumBones;
			m_NumBones++;            
			BoneInfo bi;			
			m_BoneInfo.push_back(bi);
			copyAImat(&pMesh->mBones[i]->mOffsetMatrix, m_BoneInfo[BoneIndex].boneOffset);
			m_BoneMapping[BoneName] = BoneIndex;
		} else {
			BoneIndex = m_BoneMapping[BoneName];
		} 

		for (unsigned int j = 0 ; j < pMesh->mBones[i]->mNumWeights ; j++) {
			unsigned int VertexID = pMesh->mBones[i]->mWeights[j].mVertexId;
			float Weight  = pMesh->mBones[i]->mWeights[j].mWeight;                   
			bones[VertexID].addBoneData(BoneIndex, Weight);
		}
	}
	cout << "Bones: " << pMesh->mNumBones << endl;
}

std::vector<Index> SkinMesh::loadMeshIndices(const aiMesh *pMesh) const {
   std::vector<Index> indices;

   // compatibility code...
   unsigned int maxIndex = (unsigned int)this->vertices.size();
   // Add all of the indices to the indices vector
   for (int i = 0 ; i < pMesh->mNumFaces ; i++) {
      const aiFace& face = pMesh->mFaces[i];
      // assert(face.mNumIndices == 3);
	  if (face.mNumIndices != 3)
		  continue;

      indices.push_back(face.mIndices[0]);
      indices.push_back(face.mIndices[1]);
      indices.push_back(face.mIndices[2]);
      
      // for compatibility, populate the main indices vector
      this->indices.push_back(face.mIndices[0]+maxIndex);
      this->indices.push_back(face.mIndices[1]+maxIndex);
      this->indices.push_back(face.mIndices[2]+maxIndex);
   }
   
   return indices;
}

void SkinMesh::loadMaterialsFromScene(const aiScene* pScene, const std::string &textureDir) {
   for (int i = 0 ; i < pScene->mNumMaterials ; i++) {
      const aiMaterial* pMaterial = pScene->mMaterials[i];
      mMaterials.push_back(loadMaterial(pMaterial, textureDir));
   }
}

std::shared_ptr<Material> SkinMesh::loadMaterial(const aiMaterial* pMaterial, const std::string &texDir) const {
   std::shared_ptr<Material> m = std::shared_ptr<Material>(new Material());

   if (pMaterial->GetTextureCount(aiTextureType_DIFFUSE) > 0) {
      aiString texPath;
      if (pMaterial->GetTexture(aiTextureType_DIFFUSE, 0, &texPath, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS) {
         std::string texFile = texDir + "/" + texPath.data;
         gl::Image image(texFile);
         gl::texture tex(image);
         tex.SetWrapping(gl::Wrapping::Repeat);
         m->diffuse.hasTexture = true;
         m->diffuse.weight = 1.0f;
         m->diffuse.texture = tex;
      }
   } else {
       m->diffuse.hasTexture = false;
       m->diffuse.color = glm::vec3(0.6,0.8,0.3);
   }
   return m;
}

/**
 * MeshEntry functions
 */
DrawInfo SkinMeshEntry::getDrawInfo() const {
   DrawInfo info;
   info.isSkinned = true;
   info.mode = gl::primitive::triangles;
   info.num_elements = (unsigned int)num_indices;
   info.draw_type = DrawType::elements;
   info.ibo = ibo;
   info.index_type = gl::type::UnsignedInt;
   return info;
}

// Specifies which attributes the vbo has and where they are located
bool SkinMeshEntry::getAttributeInfo(const ShaderContract &c, AttributeInfo &info) const {
   info.stride_bytes = sizeof(SkinVertex);
   info.buffer = vbo;
   info.data_type = gl::type::Float;
   
   if (c.attribute_type == ShaderAttribute::Position) {
      if (c.size != 3)
         return false;
      info.start_bytes = 0;
      
   } else if (c.attribute_type == ShaderAttribute::Normal) {
      if (c.size != 3)
         return false;
      info.start_bytes = 3 * sizeof(float);
   
   } else if (c.attribute_type == ShaderAttribute::TextureCoordinate) {
      if (c.size != 2)
         return false;
      info.start_bytes = 6 * sizeof(float);
   } else if (c.attribute_type == ShaderAttribute::BoneIndices) {
	  if (c.size != 4)
		  return false;
	  info.start_bytes = 8 * sizeof(float);
   } else if (c.attribute_type == ShaderAttribute::BoneWeights) {
	   if (c.size != 4)
		   return false;
	   info.start_bytes = (8 * sizeof(float)) + (4 * sizeof(int));
   } else {
      return false;
   }
   return true;
}
   
unsigned int SkinMesh::findPosition(float AnimationTime, const aiNodeAnim* pNodeAnim) {    
	for (unsigned int i = 0 ; i < pNodeAnim->mNumPositionKeys - 1 ; i++) {
		if (AnimationTime < (float)pNodeAnim->mPositionKeys[i + 1].mTime) {
			return i;
		}
	}
    
	return 0;
}

unsigned int SkinMesh::findRotation(float AnimationTime, const aiNodeAnim* pNodeAnim) {
	for (unsigned int i = 0 ; i < pNodeAnim->mNumRotationKeys - 1 ; i++) {
		if (AnimationTime < (float)pNodeAnim->mRotationKeys[i + 1].mTime) {
			return i;
		}
	}

	return 0;
}

unsigned int SkinMesh::findScaling(float AnimationTime, const aiNodeAnim* pNodeAnim) {   
	for (unsigned int i = 0 ; i < pNodeAnim->mNumScalingKeys - 1 ; i++) {
		if (AnimationTime < (float)pNodeAnim->mScalingKeys[i + 1].mTime) {
			return i;
		}
	}
    
	return 0;
}

void SkinMesh::calcInterpolatedPosition(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim) {
	if (pNodeAnim->mNumPositionKeys == 1) {
		Out = pNodeAnim->mPositionKeys[0].mValue;
		return;
	}
            
	unsigned int PositionIndex = findPosition(AnimationTime, pNodeAnim);
	unsigned int NextPositionIndex = (PositionIndex + 1);

	float DeltaTime = pNodeAnim->mPositionKeys[NextPositionIndex].mTime - pNodeAnim->mPositionKeys[PositionIndex].mTime;
	float Factor = (AnimationTime - (float)pNodeAnim->mPositionKeys[PositionIndex].mTime) / DeltaTime;

	const aiVector3D& Start = pNodeAnim->mPositionKeys[PositionIndex].mValue;
	const aiVector3D& End = pNodeAnim->mPositionKeys[NextPositionIndex].mValue;

	aiVector3D Delta = End - Start;
	Out = Start + Factor * Delta;
}

void SkinMesh::calcInterpolatedRotation(aiQuaternion& Out, float AnimationTime, const aiNodeAnim* pNodeAnim) {
	if (pNodeAnim->mNumRotationKeys == 1) {
		Out = pNodeAnim->mRotationKeys[0].mValue;
		return;
	}
    
	unsigned int RotationIndex = findRotation(AnimationTime, pNodeAnim);
	unsigned int NextRotationIndex = (RotationIndex + 1);

	float DeltaTime = pNodeAnim->mRotationKeys[NextRotationIndex].mTime - pNodeAnim->mRotationKeys[RotationIndex].mTime;
	float Factor = (AnimationTime - (float)pNodeAnim->mRotationKeys[RotationIndex].mTime) / DeltaTime;

	const aiQuaternion& StartRotationQ = pNodeAnim->mRotationKeys[RotationIndex].mValue;
	const aiQuaternion& EndRotationQ   = pNodeAnim->mRotationKeys[NextRotationIndex].mValue;

	aiQuaternion::Interpolate(Out, StartRotationQ, EndRotationQ, Factor);

	Out = Out.Normalize();
}

void SkinMesh::calcInterpolatedScaling(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim) {
	if (pNodeAnim->mNumScalingKeys == 1) {
		Out = pNodeAnim->mScalingKeys[0].mValue;
		return;
	}

	unsigned int ScalingIndex = findScaling(AnimationTime, pNodeAnim);
	unsigned int NextScalingIndex = (ScalingIndex + 1);

	float DeltaTime = pNodeAnim->mScalingKeys[NextScalingIndex].mTime - pNodeAnim->mScalingKeys[ScalingIndex].mTime;
	float Factor = (AnimationTime - (float)pNodeAnim->mScalingKeys[ScalingIndex].mTime) / DeltaTime;

	const aiVector3D& Start = pNodeAnim->mScalingKeys[ScalingIndex].mValue;
	const aiVector3D& End   = pNodeAnim->mScalingKeys[NextScalingIndex].mValue;
	aiVector3D Delta = End - Start;
	Out = Start + Factor * Delta;
}

void SkinMesh::readNodeHierarchy(float AnimationTime, const aiNode* pNode, const glm::mat4& ParentTransform) {    
	string NodeName(pNode->mName.data);
    
	const aiAnimation* pAnimation = mScene->mAnimations[0];
        
	glm::mat4 NodeTransformation;
	copyAImat(&pNode->mTransformation, NodeTransformation);
     
	const aiNodeAnim* pNodeAnim = findNodeAnim(pAnimation, NodeName);
    
	if (pNodeAnim) {
		aiVector3D Scaling;
		calcInterpolatedScaling(Scaling, AnimationTime, pNodeAnim);
		glm::mat4 ScalingM = glm::mat4(1.0);
		ScalingM = glm::scale(glm::vec3(Scaling.x, Scaling.y, Scaling.z));

		aiQuaternion RotationQ;
		calcInterpolatedRotation(RotationQ, AnimationTime, pNodeAnim);
		aiMatrix3x3 rotation = RotationQ.GetMatrix();

		glm::mat4 RotationM = glm::mat4(1.0f);
		RotationM[0][0] = rotation.a1; RotationM[1][0] = rotation.a2;
		RotationM[2][0] = rotation.a3; RotationM[3][0] = 0.0f;
		RotationM[0][1] = rotation.b1; RotationM[1][1] = rotation.b2;
		RotationM[2][1] = rotation.b3; RotationM[3][1] = 0.0f;
		RotationM[0][2] = rotation.c1; RotationM[1][2] = rotation.c2;
		RotationM[2][2] = rotation.c3; RotationM[3][2] = 0.0f;
		RotationM[0][3] = 0.0f; RotationM[1][3] = 0.0f;
		RotationM[2][3] = 0.0f; RotationM[3][3] = 1.0f;

		aiVector3D Translation;
		calcInterpolatedPosition(Translation, AnimationTime, pNodeAnim);
		glm::mat4 TranslationM = glm::mat4(1.0f);
		TranslationM = glm::translate(Translation.x, Translation.y, Translation.z);

		// compute transformation for node
		NodeTransformation =  TranslationM * RotationM * ScalingM;
	}

	glm::mat4 GlobalTransformation = ParentTransform * NodeTransformation;
    
	if (m_BoneMapping.find(NodeName) != m_BoneMapping.end()) {
		unsigned int BoneIndex = m_BoneMapping[NodeName];
		m_BoneInfo[BoneIndex].finalTransformation = m_GlobalInverseTransform * GlobalTransformation * m_BoneInfo[BoneIndex].boneOffset;
	}
    
	for (unsigned int i = 0 ; i < pNode->mNumChildren ; i++) {
		readNodeHierarchy(AnimationTime, pNode->mChildren[i], GlobalTransformation);
	}
}

void SkinMesh::boneTransform(float TimeInSeconds, vector<glm::mat4>& Transforms) {
	glm::mat4 Identity;
	Identity = glm::mat4(1.0f);

	if (mScene) {
		float TicksPerSecond = mScene->mAnimations[0]->mTicksPerSecond != 0 ? mScene->mAnimations[0]->mTicksPerSecond : 30.0f;

		float TimeInTicks = TimeInSeconds * TicksPerSecond;
		float AnimationTime = fmod((float)TimeInTicks, (float)mScene->mAnimations[0]->mDuration);

		readNodeHierarchy(AnimationTime, mScene->mRootNode, Identity);

		Transforms.resize(m_NumBones);

		for (unsigned int i = 0 ; i < m_NumBones ; i++) {
			Transforms[i] = m_BoneInfo[i].finalTransformation;
		}
		printf("Hello!\n");
	}
}

const aiNodeAnim* SkinMesh::findNodeAnim(const aiAnimation* pAnimation, const std::string NodeName) {
	for (unsigned int i = 0; i < pAnimation->mNumChannels; i++) {
		const aiNodeAnim* currentNodeAnim = pAnimation->mChannels[i];
        
		if (string(currentNodeAnim->mNodeName.data) == NodeName) {
			return currentNodeAnim;
		}
    }
    
    return NULL;
}
