//
//  GBuffer2.h
//  CrusinPangaea
//
//  Created by Richie on 2/27/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#pragma once

#ifdef _WIN32
#define GLEW_STATIC
#endif

#include "Shader.hpp"
#include "Program.hpp"
#include "Framebuffer.hpp"
#include "VertexArray.hpp"

#include <glm/glm.hpp>

class PointLight {
public:
	glm::vec3 intensity;
	glm::vec3 position;
};

class GBuffer {   
   // Vertex Shaders
   static gl::shader &getVertexShader();
   static gl::shader &getDepthVertexShader();

   // Fragment Shaders
   static gl::shader &getFragmentShader();
   static gl::shader &getDepthFragmentShader();
   
   // private singleton resource accessors
   static gl::program &getProgram();
   static gl::program &getDepthProgram();
   
   static gl::vertex_array &getQuadVAO();
   static gl::vertex_buffer &getQuadVBO();
   
   // static initializers
   static gl::vertex_array makeQuadVAO(gl::vertex_buffer &vbo);
   static gl::vertex_buffer makeQuadVBO();
   static gl::program makeProgram();
   static gl::program makeDepthProgram();
   
   // if we are rendering, this contains the previous framebuffer
   GLint restoreFB;
   
   GLuint framebuffer;
   GLuint shadowbuffer;
   gl::texture texColor;
   gl::texture texDepth;
   gl::texture texNormal;
   gl::texture texPosition;
   gl::texture texShadow;
   std::vector<PointLight> myLights;

   
   gl::factory &factory() {
      static class gl::factory factory;
      return factory;
   }
   
public:
   GBuffer(int width, int height);
   void begin();
   void end();
   void begin_shadow();
   void end_shadow();
   void draw(float depth);
   void draw_shadow(float depth);
   
   void addLight(const glm::vec3 &lightPosition, const glm::vec3 &lightIntensity);
	void setLights();
	void setLight(const glm::vec3& lightPosition, const glm::vec3& lightIntensity);
	void setEye(const glm::vec3 &eyePos);
   void setSpot(const glm::mat4 &spot);
   
   gl::texture getColorTex();
   gl::texture getPosTex();
};