
#pragma once

#include "Renderable.h"
#include "Material.h"

#include <list>
#include <vector>
#include <memory>

class aiScene;
class aiMesh;
class aiMaterial;
#ifndef _WIN32
class std::vector;
#endif
class Vertex;
typedef GLuint Index;

class StaticMesh {
   std::list<std::shared_ptr<Renderable> > mRenderables;
   std::vector<std::shared_ptr<Material> > mMaterials;
   
   void loadMeshesFromScene(const aiScene *pScene);

   std::shared_ptr<Renderable> loadMesh(const aiMesh *pMesh) const;
   std::vector<Vertex> loadMeshVertices(const aiMesh *pMesh) const;
   std::vector<Index> loadMeshIndices(const aiMesh *pMesh) const;
   
   void loadMaterialsFromScene(const aiScene *pScene, const std::string &textureDir);
   
   std::shared_ptr<Material> loadMaterial(const aiMaterial *pMaterial, const std::string &texDir) const;
   
public:
   
   StaticMesh(const std::string &filename, const std::string &textureDir);
   
   std::list<std::shared_ptr<Renderable> >::iterator begin() {
      return mRenderables.begin();
   }
   
   std::list<std::shared_ptr<Renderable> >::iterator end() {
      return mRenderables.end();
   }
      
   std::list<std::shared_ptr<Renderable> >::const_iterator begin() const {
      return mRenderables.begin();
   }
   
   std::list<std::shared_ptr<Renderable> >::const_iterator end() const {
      return mRenderables.end();
   }
   
   // This is here for compatibility with old code. Don't use this with any new code.
   // However, there is currently no other way to retrieve the mesh data, so deal with
   // it.
   mutable std::vector<glm::vec3> vertices;
   mutable std::vector<unsigned int> indices;
};