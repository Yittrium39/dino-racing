//
//  Plane.h
//  CrusinPangaea
//
//  Created by Richie on 2/14/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#pragma once

#include "Renderable.h"
#include "Material.h"

#include "Program.hpp"
#include "VertexBuffer.hpp"
#include "VertexArray.hpp"

class Plane : public Renderable {
   gl::vertex_buffer vbo;
   gl::vertex_buffer ibo;
   Material mat;
   
   public:
   Plane();
   virtual bool getAttributeInfo(const ShaderContract &c, AttributeInfo &info) const;
   virtual DrawInfo getDrawInfo() const;
   virtual Material getMaterial() const { return mat; }
};