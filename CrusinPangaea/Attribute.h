//
//  Attribute.h
//  CrusinPangaea
//
//  Created by Richie on 2/16/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#pragma once

namespace ShaderAttribute {
   enum shader_attribute_t {
      Position, Normal, TextureCoordinate, BoneIndices, BoneWeights
   };
}

namespace LevelOfDetail {
   enum level_of_detail_t {
      High, Medium, Low, Billboard
   };
}


struct FragDataInfo {
   bool hasNormal;
   bool hasColor;
   bool hasPosition;
   bool hasShadow;
   
   GLuint normalLoc;
   GLuint colorLoc;
   GLuint positionLoc;
   GLuint shadowLoc;
   FragDataInfo() : hasNormal(false), hasColor(false), hasPosition(false), hasShadow(false) { }
};

// Object that is used to request where data for an attribute is stored
struct ShaderContract {
   ShaderAttribute::shader_attribute_t attribute_type;
   unsigned int size;
   LevelOfDetail::level_of_detail_t level_of_detail;
   ShaderContract(ShaderAttribute::shader_attribute_t at, unsigned int s) :
      attribute_type(at), size(s) { }
};


// Contains info about where data for an attribute is stored
struct AttributeInfo {
   gl::vertex_buffer buffer;
   unsigned int stride_bytes;
   intptr_t start_bytes;
   gl::type::type_t data_type;
};