//
//  WorldNoBullet.cpp
//  CrusinPangaea
//
//  Created by Richie on 2/23/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#include "WorldNoBullet.h"
#include "BVH.hpp"
#include <vector>

#include <iostream>
using namespace std;

void WorldNoBullet::setTrack(SceneObject *t) {
   std::vector<BVH::Sphere> spheres;
   
   Points *p = new Points();
   debug = std::shared_ptr<Renderable>(p);
   
   data::vec3 triangle[3];
   std::vector<unsigned int>::iterator index = t->getMesh()->indices.begin();
   for(int i = 0; index != t->getMesh()->indices.end(); i++) {
		glm::vec4 v = t->getModelMatrix() * glm::vec4(t->getMesh()->vertices[*(index++)], 1.0f);
		v.x /= v.w;
		v.y /= v.w;
		v.z /= v.w;
      triangle[i%3] = data::vec3(v.x, v.y, v.z);
      if (i % 3 == 2) {
         BVH::Sphere s = BVH::Sphere::minimumBoundingSphere(triangle[0],triangle[1],triangle[2]);
         s.triangle[0] = triangle[0];
         s.triangle[1] = triangle[1];
         s.triangle[2] = triangle[2];
         spheres.push_back(s);
      }
   }
   mTrack = std::shared_ptr<BVH>(new BVH(&spheres[0], spheres.size()));
}

physics::Kart *WorldNoBullet::makeKart() {
   mKarts.push_back(KartNoBullet());
   return &mKarts.back();
}

static glm::vec3 rotate(const glm::vec3 &v, const glm::quat &q) {
   glm::vec4 rotated =  q * glm::vec4(v, 1.0f);
   return glm::normalize(glm::vec3(rotated));
}

void WorldNoBullet::update(float deltaT) {
   const float kartRadius = 5.f;
   
   std::list<KartNoBullet>::iterator kart, other;
   for (kart = mKarts.begin(); kart != mKarts.end(); kart++) {
      
      glm::vec3 init = kart->getPosition();
      kart->move(deltaT);
      glm::vec3 position = kart->getPosition();
      cout << position.x << " " << position.z << endl;
    /*
        glm::vec3 change = position - init;
      
      // Get Height of wheels
      glm::vec3 facing = rotate(glm::vec3(0,0,1), kart->getRotation());
      glm::vec3 left = rotate(glm::vec3(1,0,0), kart->getRotation());
      
      glm::vec3 leftFrontWheel = glm::normalize(left + facing);
      glm::vec3 rightFrontWheel = glm::normalize(-left + facing);
      glm::vec3 leftBackWheel = glm::normalize(left - facing);
      glm::vec3 rightBackWheel = glm::normalize(-left - facing);
      
      float lf = mTrack->HeightTest(position + leftFrontWheel + glm::vec3(0,5,0));
      float rf = mTrack->HeightTest(position + rightFrontWheel + glm::vec3(0,5,0));
      float lb = mTrack->HeightTest(position + leftBackWheel + glm::vec3(0,5,0));
      float rb = mTrack->HeightTest(position + rightBackWheel + glm::vec3(0,5,0));
      
      glm::vec3 pos = kart->getPosition();
      if (abs(lf - position.y) > 2) {
         pos -= leftFrontWheel * glm::dot(change, leftFrontWheel);
      }
      if (abs(rf - position.y) > 2) {
         pos -= rightFrontWheel * glm::dot(change, rightFrontWheel);
      }
      if (abs(lb - position.y) > 2) {
         pos -= leftBackWheel * glm::dot(change, leftBackWheel);
      }
      if (abs(rb - position.y) > 2) {
         pos -= rightBackWheel * glm::dot(change, rightBackWheel);
      }
      kart->setPosition(pos);
      */
   }
   
   
   for (kart = mKarts.begin(); kart != mKarts.end(); kart++) {
      for (other = mKarts.begin(); other != mKarts.end(); other++) {
         if (kart != other) {
            float distance = glm::length(kart->getPosition() - other->getPosition());
            if (distance < kartRadius) {
               // We collided with another kart
               float moveDist = (kartRadius - distance) / 2.f;
               glm::vec3 moveVec = glm::normalize(kart->getPosition() - other->getPosition());
               kart->setPosition(kart->getPosition() + moveVec * moveDist);
               other->setPosition(other->getPosition() - moveVec * moveDist);
            }
         }
      }
   }
   
}