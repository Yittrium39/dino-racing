#pragma once

#include "VertexBuffer.hpp"

namespace gl {
	namespace primitive {
		enum primitive_t {
			triangles = GL_TRIANGLES,
         triangle_fan = GL_TRIANGLE_FAN,
			lines = GL_LINES,
			points = GL_POINTS,
		};
	}
   
	class vertex_array {
      GLuint obj;
		factory &factory() {
			static class factory factory;
			return factory;
		}
	public:
      vertex_array() {
         factory().create(obj, glGenVertexArrays, glDeleteVertexArrays);
      };
      
		explicit vertex_array(const std::string &debug) {
         factory().create(obj, glGenVertexArrays, glDeleteVertexArrays);
      }
      
		vertex_array(const vertex_array &other) {
         factory().copy(other.obj, obj);
      }

		~vertex_array() {
         factory().destroy(obj);
      }

		operator GLuint() const {
         return obj;
      }
      
		const vertex_array& operator=(const vertex_array &other) {
         factory().copy(other.obj, obj, true);
         return *this;
      }

		void bind_attribute(const attribute& attribute, const vertex_buffer& buffer, uint count, type::type_t type, uint stride, intptr_t offset ) {
         glBindVertexArray(obj);
         glBindBuffer(GL_ARRAY_BUFFER, buffer);
         glEnableVertexAttribArray(attribute);
         glVertexAttribPointer(attribute, count, type, GL_FALSE, stride, (const GLvoid*)offset);
      }

		void bind_elements(const vertex_buffer &elements) {
         glBindVertexArray(obj);
         glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elements);
      }
      
      void draw_arrays(primitive::primitive_t mode, uint offset, uint vertices) {
         glBindVertexArray(obj);
         glDrawArrays(mode, offset, vertices);
      }
      
		void draw_elements(primitive::primitive_t mode, intptr_t offset, uint count, uint type) {
         glBindVertexArray(obj);
         glDrawElements(mode, count, type, (const GLvoid*)offset);
      }

	};
}