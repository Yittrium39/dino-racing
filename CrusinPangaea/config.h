//
//  GL.h
//  CrusinPangaea
//
//  Created by Richie on 2/14/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#pragma once

#include "Platform.hpp"

#ifdef USE_GLUT_WINDOW
#include "GlutWrapper.hpp"
#else
#include "GlfwWrapper.h"
#endif

namespace gl {
   class window :
   #ifdef USE_GLUT_WINDOW
      public glut::Window
   #else
      public glfw::Window
   #endif
   {
      public:
      window() {
         // Some platforms need experimental for multiple VAOs
         glewExperimental = GL_TRUE;
         glewInit();
         
         // Check to make sure we have a context of at least 3.2
         // Most versions of GLUT do not create a context of >= 3.2
         // Try linking to freeglut instead and manually changing the
         // context version in GlutWrapper.cpp
         int gl_version_major, gl_version_minor;
         glGetIntegerv( GL_MAJOR_VERSION, &gl_version_major );
         glGetIntegerv( GL_MINOR_VERSION, &gl_version_minor );
         assert(gl_version_major > 3 || (gl_version_major >= 3 && gl_version_minor >= 2));
      }
   };
}