//
//  HUD.cpp
//  CrusinPangaea
//
//  Created by Richie on 2/18/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#include "HUD.h"
#include <stdlib.h>

HUD::HUD() : hud(800,600), obelix("assets/ObelixPro.ttf", 40), obelix_small("assets/ObelixPro.ttf", 30) {

}

void HUD::drawCountdown(const std::string &number, const std::string &number2, float stateTimer, GameState gameState, PowerupType type, double list[], int goingBackwards) {
   hud.begin();
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  
    char buffer[50];
    sprintf (buffer, "%.2f", stateTimer);
    std::string helper;
    helper = buffer;
    
    
    if (gameState == WaitState) {
		obelix.drawGlyph(800, 600, "Welcome to Crusin Pangaea!", 150, 100);
        obelix.drawGlyph(800, 600, "Press Enter to Start", 200, 500);
    }
    else if(gameState == RaceState) {
		if (stateTimer < 1.5) {
			obelix.drawGlyph(800, 600, "Go!", 360, 200);
		}
        obelix_small.drawGlyph(800, 600, "Place " + number, 25, 50);
        obelix.drawGlyph(800, 600, "Lap " + number2 + "/3", 580, 50);
        obelix_small.drawGlyph(800, 600, "Time: " + helper , 25, 100);

		if (type == PowerupType::Missile) {
			obelix.drawGlyph(800, 600, "Missile", 600, 575);
		}
		if (type == PowerupType::Boost) {
			obelix.drawGlyph(800, 600, "Boost", 600, 575);
		}
		if (type == PowerupType::None) {
			obelix.drawGlyph(800, 600, "", 600, 575);
		}
        if(goingBackwards > 1) {
            obelix.drawGlyph(800, 600, "Wrong Way!", 275, 200);
        }
    }
    else if (gameState == CountDownState) {
        if (stateTimer > 4) {}
        else if(stateTimer > 2.7f) {
            obelix.drawGlyph(800, 600, "Go!", 363, 200);
        }
        else if(stateTimer > 1.25) {
            obelix.drawGlyph(800, 600, "Set!", 360, 200);
        }
        else if(stateTimer > 0) {
            obelix.drawGlyph(800, 600, "Get Ready!", 300, 200);
        }
    }
    else {
        if(atoi(number.c_str()) == 1) {
            obelix.drawGlyph(800, 600, "You placed 1st!", 245, 60);
        }
        else if(atoi(number.c_str()) == 2) {
			obelix.drawGlyph(800, 600, "You placed 2nd", 250, 60);
        }
		else if(atoi(number.c_str()) == 3) {
            obelix.drawGlyph(800, 600, "You placed 3rd", 250, 60);
        }
        else
        {
            obelix.drawGlyph(800, 600, "Place: " + number + "th", 245, 60);
        }
        obelix.drawGlyph(800, 600, "Time: " + helper, 285, 110);
        obelix.drawGlyph(800, 600, "High Scores", 275, 190);
        for(int i = 0; i < 10; i++) {
            char buffer[50];
            sprintf (buffer, "%.2f", list[i]);
            std::string helper;
            helper = buffer;
            //printf("hmm: %s\n", helper.c_str());
            if(i == 0) {
                obelix_small.drawGlyph(800, 600, "1st " + helper, 315, 230 + 40*i);
            }
            else if(i == 1) {
                obelix_small.drawGlyph(800, 600, "2nd " + helper, 315, 230 + 40*i);
            }
            else if(i == 2) {
                obelix_small.drawGlyph(800, 600, "3rd " + helper, 315, 230 + 40*i);
            }
            else {
                char buf[10];
                sprintf(buf, "%d", i+1);
                std::string placement = buf;
                obelix_small.drawGlyph(800, 600, placement + "th " + helper, 315, 230 + 40*i);
            }
        }
    }
   hud.end();
   hud.draw(-1);
}

void HUD::drawHighScores(double list[]) {
    
}