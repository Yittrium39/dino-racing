#include "GlutWrapper.hpp"

#include <GLUT/GLUT.h>

#include <utility>
#include <vector>

namespace glut {
/**
 * Creates a GLUT window using the window manager. This is needed in case an
 * inheriting class does not explicitly call the designated constructor.
 */
Window::Window() {
   mHandle = WindowManager::getInstance().createWindow("GLUT Window", *this);
}

/**
 * The designated constructor. This creates a GLUT window using the window
 * manager with the specified title.
 */
Window::Window(const std::string &title) {
   mHandle = WindowManager::getInstance().createWindow(title, *this);
}

/**
 * Notifies the GLUT manager to destroy this window
 */
Window::~Window() {
   WindowManager::getInstance().removeWindow(mHandle);
}

std::map<int, Window *> WindowManager::mWindows;

/**
 * Initializes GLUT. This will only be called once since WindowManager
 * is a singleton.
 */
WindowManager::WindowManager() {   
   // We will pass some dummy command line args to GLUT since we can change
   // everything programmatically that we would tell it from the command line
   char dummy_arg = '\0';
   char *dummy_argv[1] = {&dummy_arg};
   int dummy_argc = 1;

   glutInit(&dummy_argc, dummy_argv);
   glutIdleFunc(&idleCallback);
   glutIgnoreKeyRepeat(true);
   
   glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
   glutInitWindowSize(800, 600);
}

/**
 * Gets the window that needs to respond to the callback function
 */
Window &WindowManager::currentWindow() {
   int handle = glutGetWindow();
   std::map<int, Window *>::iterator window = mWindows.find(handle);
   if (window == mWindows.end()) {
      throw UnmanagedWindowException();
   }
   return *window->second;
}

/** 
 * This is called by the constructor of Window to create the window and
 * register it with the window manager.
 */
int WindowManager::createWindow(const std::string &title, Window &window) {
   // GLUT interface requires non-const string, so we give it a copy
   std::vector<char> titleCopy(title.c_str(), title.c_str() + title.size() + 1u);

   int handle = glutCreateWindow(&titleCopy[0]);
   mWindows.insert(std::pair<int, Window *>(handle, &window));

   // GLUT can only have one callback for all windows, have it callback
   // our own static function that can decide which window to act on
   glutDisplayFunc(&displayCallback);
   glutKeyboardFunc(&keyboardCallback);
   glutKeyboardUpFunc(&keyboardUpCallback);
   glutSpecialFunc(&specialCallback);
   glutSpecialUpFunc(&specialUpCallback);
   
   return handle;
}

/**
 * This is called by the destructor of Window.
 */
void WindowManager::removeWindow(int window) {
   mWindows.erase(window);
   glutDestroyWindow(window);
}

void WindowManager::runMainLoop() {
   glutMainLoop();
}

void WindowManager::displayCallback() {
   currentWindow().draw();
   glutSwapBuffers();
   glutPostRedisplay();
}

void WindowManager::keyboardCallback(unsigned char key, int x, int y) {
   gl::event e;
   e.type = gl::event::key_down;
   e.key = map_key(key);
   currentWindow().event(e);
}

void WindowManager::keyboardUpCallback(unsigned char key, int x, int y) {
   gl::event e;
   e.type = gl::event::key_up;
   e.key = map_key(key);
   currentWindow().event(e);
}

void WindowManager::specialCallback(int key, int x, int y) {
   gl::event e;
   e.type = gl::event::key_down;
   e.key = map_special(key);
   currentWindow().event(e);
}

void WindowManager::specialUpCallback(int key, int x, int y) {
   gl::event e;
   e.type = gl::event::key_up;
   e.key = map_special(key);
   currentWindow().event(e);
}

void WindowManager::idleCallback() {
   std::map<int, Window *>::iterator windowIterator;
   for(windowIterator = mWindows.begin(); windowIterator != mWindows.end(); windowIterator++) {
      windowIterator->second->update();
   }
}

gl::key_event WindowManager::map_key(unsigned char key) {
   gl::key_event ev;
   ev.control = false;
   ev.alt = false;
   ev.shift = false;
   if (key >= 'A' && key <= 'Z') {
      ev.shift = true;
      ev.code = (gl::key::key_t)(gl::key::a + (key - 'A'));
   } else if (key <= 'a' && key <= 'z') {
      ev.code = (gl::key::key_t)(gl::key::a + (key - 'a'));
   } else if (key == ' ') {
      ev.code = gl::key::space;
   } else if (key == '\n') {
      ev.code = gl::key::enter;
   } else if (key == 27) {
      ev.code = gl::key::escape;
   }
   return ev;
}
   
gl::key_event WindowManager::map_special(int key) {
   gl::key_event ev;
   ev.control = false;
   ev.alt = false;
   ev.shift = false;
   if (key == GLUT_KEY_DOWN) {
      ev.code = gl::key::down;
   } else if (key == GLUT_KEY_LEFT) {
      ev.code = gl::key::left;
   } else if (key == GLUT_KEY_UP) {
      ev.code = gl::key::up;
   } else if (key == GLUT_KEY_RIGHT) {
      ev.code = gl::key::right;
   }
   return ev;
}

void Window::run() {
   WindowManager::getInstance().runMainLoop();
}



}
