//
//  HUD.h
//  CrusinPangaea
//
//  Created by Richie on 2/18/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#pragma once

#include "Panel.h"
#include "Font.h"
#include "GameLogic.h"

class HUD {
   Panel hud;
   Font obelix;
    Font obelix_small;
   
   public:
   HUD();
    void drawCountdown(const std::string &number, const std::string &number2, float stateTimer, GameState gameState, PowerupType type, double list[], int goingBackwards);
    void drawHighScores(double list[]);
};