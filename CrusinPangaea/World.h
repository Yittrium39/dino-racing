//
//  World.h
//  CrusinPangaea
//
//  Created by Richie on 2/20/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#pragma once

#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include "SceneObject.h"

namespace physics {

   class Track {
      public:
      virtual std::vector<glm::vec3>::iterator beginVertices() = 0;
      virtual std::vector<glm::vec3>::iterator endVertices() = 0;
      virtual std::vector<unsigned int>::iterator beginIndices() = 0;
      virtual std::vector<unsigned int>::iterator endIndices() = 0;
   };

   class Kart {
      public:
      virtual void update() { }
      virtual void setAccelerating(bool, bool) = 0;
      virtual void setBraking(bool) = 0;
      virtual void setTurningLeft(bool) = 0;
      virtual void setTurningRight(bool) = 0;
      virtual void setBoost(bool) = 0;
      virtual void setPowerUpBoost(bool) = 0;
      virtual void setJump(bool) = 0;
      virtual void setDrop(bool) = 0;


      virtual void setPosition(const glm::vec3 &p) = 0;
      virtual void setRotation(const glm::quat &q) = 0;
      virtual glm::vec3 getPosition() const = 0;
      virtual glm::quat getRotation() const = 0;
	  virtual bool getBraking() const = 0;
	  virtual bool getAccelerating() const = 0;
	  virtual bool getPowerUpBoost() const = 0;
	  virtual bool checkReverse() const = 0;
	  virtual float getSpeed() const = 0;
   };
   
   class World {
      public:
      virtual void setTrack(SceneObject *t) = 0;
      virtual Kart *makeKart() = 0;
      virtual void update(float deltaT) = 0;
   };
   
}