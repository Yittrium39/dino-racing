//
//  Framebuffer.hpp
//  CrusinPangaea
//
//  Created by Richie on 2/18/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#pragma once

#include "Texture.hpp"
#include <exception>

namespace gl {
   
   class FramebufferException : public std::exception {
      const char * what_str;
   public:
      FramebufferException(const char * str) : what_str(str) { }
      
      virtual const char* what() const throw() {
			return what_str;
		}
	};
   
	class framebuffer {
      GLuint obj;
		factory &factory() {
			static class factory factory;
			return factory;
		}
      texture texColor;
		texture texDepth;
	public:
      
		framebuffer(const framebuffer& other) {
         factory().copy(other.obj, obj);
         texColor = other.texColor;
         texDepth = other.texDepth;
      }
      
		framebuffer(uint width, uint height, uchar color = 32, uchar depth = 24)	{
         GLint restoreId;
         glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &restoreId);
         
         // Determine appropriate formats
         InternalFormat::internal_format_t colorFormat;
         if ( color == 24 ) colorFormat = InternalFormat::RGB;
         else if ( color == 32 ) colorFormat = InternalFormat::RGBA;
         else throw FramebufferException("Unable to create color buffer for framebuffer");
         
         InternalFormat::internal_format_t depthFormat;
         if ( depth == 8 ) depthFormat = InternalFormat::DepthComponent;
         else if ( depth == 16 ) depthFormat = InternalFormat::DepthComponent16;
         else if ( depth == 24 ) depthFormat = InternalFormat::DepthComponent24;
         else if ( depth == 32 ) depthFormat = InternalFormat::DepthComponent32F;
         else throw FramebufferException("Unable to create depth buffer for framebuffer");
         
         // Create FBO
         factory().create(obj, glGenFramebuffers, glDeleteFramebuffers);
         glBindFramebuffer(GL_DRAW_FRAMEBUFFER, obj);
         
         // Create texture to hold color buffer
         texColor.Image2D(0, DataType::UnsignedByte, Format::RGBA, width, height, colorFormat);
         texColor.SetWrapping(Wrapping::ClampEdge, Wrapping::ClampEdge);
         texColor.SetFilters(Filter::Linear, Filter::Linear);
         glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texColor, 0);
         
         // Create renderbuffer to hold depth buffer
         if (depth > 0) {
            glBindTexture(GL_TEXTURE_2D, texDepth);
            glTexImage2D(GL_TEXTURE_2D, 0, depthFormat, width, height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);
            texDepth.SetWrapping(Wrapping::ClampEdge, Wrapping::ClampEdge);
            texDepth.SetFilters(Filter::Nearest, Filter::Nearest);
            glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, texDepth, 0);
         }
         
         // Check
         if (glCheckFramebufferStatus( GL_DRAW_FRAMEBUFFER ) != GL_FRAMEBUFFER_COMPLETE)
            throw FramebufferException("Unable to create framebuffer");
         
         glBindFramebuffer( GL_DRAW_FRAMEBUFFER, restoreId );
      }
		
      ~framebuffer() {
         factory().destroy(obj);
      }
      
		operator GLuint() const {
         return obj;
      }
      
		const framebuffer& operator=(const framebuffer& other) {
         factory().copy(other.obj, obj, true);
         texColor = other.texColor;
         texDepth = other.texDepth;
         
         return *this;
      }
      
		const texture& get_texture() {
         return texColor;
      }
		const texture& get_depth_texture() {
         return texDepth;
      }
      
      void bind() {
         glBindFramebuffer(GL_DRAW_FRAMEBUFFER, obj);
         
         // Set viewport to frame buffer size
         GLint object, width, height;
         glGetFramebufferAttachmentParameteriv(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME, &object);
         
         GLint res; glGetIntegerv( GL_TEXTURE_BINDING_2D, &res );
			glBindTexture(GL_TEXTURE_2D, object);
			glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &width);
			glGetTexLevelParameteriv( GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &height);
         glBindTexture(GL_TEXTURE_2D, res);
         
         glViewport(0, 0, width, height);
         
      }
      
      void unbind() {
         glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
         // TODO: need to set back to previous viewport width
      }
      
	};
}