//
//  RaceController.cpp
//  CrusinPangaea
//
//  Created by Richie on 2/14/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#include "RaceController.h"
#include <iostream>
#include <fstream>
#include <iostream>
using namespace std;

RaceController::RaceController(gl::window &w) : Controller(w), spanel(800,600) {
    gameState = WaitState;
    stateTimer = 0.0f;
    finalPlace = "";
	brakeOn = false;
    Spline spline;
    spline.createSpline();
	gameLogic.goingBackwards = 0;
    gameLogic.setSpline(spline.splinePoints);
    gameLogic.createKartList(1, scene);
    scene.resetPowerups();
	scene.getMainPlayer()->setAccelerating(false, true);
    once = true;
   
	//FMOD sounds
	createSounds();
    Sound::Init();
    Sound::Load("assets/sounds/Jumper.mp3");
    Sound::Play(false);
}


void RaceController::Update(float deltaT) {
    static bool updateOnce = true;
	static bool finalMusicStarted = false;
	checkSounds();
    
    if(gameState == EndState) {
        scene.rearView(true);
        scene.getMainPlayer()->setBraking(true);
    }
   scene.update(deltaT);
    if(gameState == CountDownState) {
		playSound(SoundEffects::STARTRACE, 0, glm::vec3(0));
        stateTimer += deltaT;
        if(stateTimer > 2.7f) {
            gameState = RaceState;
           std::vector<std::shared_ptr<KartControl> >::const_iterator kart = scene.getKarts().begin();
           for(; kart != scene.getKarts().end(); kart++) {
            (*kart)->enableKart(true);
           }
        }
    }
    
    if(gameState == RaceState) {
        if(once) {
            stateTimer = 0;
            once = false;
			Sound::Load("assets/sounds/RaceAroundTheWorld.mp3");
			Sound::Play(false);
			Sound::SetVolume(.25);
        }
        stateTimer += deltaT;

		//Check Game logic
        gameLogic.updatePoints();
        gameLogic.updatePositions();
		gameLogic.checkKarts();
		gameLogic.checkTriggeredPowerups();
    }
	//Final lap music!
	if(gameLogic.getPlayerLapInt() == 3) {
		if (!finalMusicStarted) {
			Sound::Load("assets/sounds/TheShow.mp3");
			Sound::Play(false);
			Sound::SetVolume(.5);
			finalMusicStarted = true;
		}
	}
    if(gameLogic.getPlayerLapInt() > 3) {
        if(updateOnce) {
			Sound::Load("assets/sounds/TrickysSong.mp3");
			Sound::Play(false);
            updateHighScores();
            updateOnce = false;
        }
        gameState = EndState;
        if (finalPlace == "") {
            finalPlace = gameLogic.getPlayerPlace();
        }
        scene.getMainPlayer()->setAccelerating(false, true);
        scene.getMainPlayer()->setBraking(false);
        scene.getMainPlayer()->setTurningLeft(false);
        scene.getMainPlayer()->setTurningRight(false);
    }
}

void RaceController::updateHighScores() {
    string line;
    int i = 0;
    ifstream ifile("HighScoresList.txt");
    if (ifile) {
        while ( ifile.good() )
        {
            getline (ifile,line);
            highScores[i] = atof(line.c_str());
            cout << line << endl;
            if(i == 9) {
                i++;
                break;
            }
            i++;
        }
        highScores[10] = stateTimer;
        std::sort(std::begin(highScores), std::end(highScores));
        for(int j = 0; j < i; j++) {
            //printf("%lf\n", highScores[j]);
        }
        ifile.close();
        
        ofstream myfile;
        myfile.open ("HighScoresList.txt");
        for(int j = 0; j < i; j++) {
            myfile << highScores[j];
            myfile << "\n";
        }
        myfile.close();
        
    }
    else {
        printf("HighScoresList.txt does not exist, creating file...");
        
        ofstream myfile;
        myfile.open ("HighScoresList.txt");
        myfile << stateTimer;
        myfile << "\n";
        highScores[0] = stateTimer;
        for(int i = 0; i < 9; i++) {
            myfile << 9999.99;
            highScores[i+1] = 9999.99;
            myfile << "\n";
        }
        myfile.close();
    }
}

int cmp(const void *x, const void *y)
{
    double xx = *(double*)x, yy = *(double*)y;
    if (xx < yy) return -1;
    if (xx > yy) return  1;
    return 0;
}

void RaceController::Render() {
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   spanel.begin();
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      scene.render();
   spanel.end();
   
    if (gameState == EndState) {
		hud.drawCountdown(finalPlace, gameLogic.getPlayerLap(), stateTimer, gameState, PowerupType::None, highScores, gameLogic.goingBackwards);
        hud.drawHighScores(highScores);
    }
    else {
		hud.drawCountdown(gameLogic.getPlayerPlace(), gameLogic.getPlayerLap(), stateTimer, gameState, scene.getKartPowerup(0), highScores, gameLogic.goingBackwards);
    }
  

	if (scene.boostCam) {
		spanel.drawMotionBlur(spanel.getFramebuffer().get_texture(), spanel.getFramebuffer().get_texture(), scene.getPrevViewProjMatrix(), scene.getViewProjMatrix(), true);
	} else {
        spanel.draw(0);
		//spanel.drawMotionBlur(spanel.getFramebuffer().get_texture(), spanel.getFramebuffer().get_texture(), scene.getPrevViewProjMatrix(), scene.getViewProjMatrix(), false);
	}
    //spanel.draw(0);
}

void RaceController::startRace() {
    gameState = CountDownState;
}

void RaceController::Event(gl::event &e) {
   if (e.type == gl::event::key_down) {
       if(gameState == RaceState) {
           switch(e.key.code) {
               case gl::key::up: scene.getMainPlayer()->setAccelerating(true, true); break;
			   case gl::key::down: scene.getMainPlayer()->setAccelerating(true, false); brakeOn = true; break;
               case gl::key::left: scene.getMainPlayer()->setTurningLeft(true); break;
               case gl::key::right: scene.getMainPlayer()->setTurningRight(true); break;
               case gl::key::r: scene.rearView(true); break;
               case gl::key::e: scene.getMainPlayer()->enablePowerup(true); break;
               case gl::key::v: scene.getMainPlayer()->setJump(true); break;
               case gl::key::x: scene.getMainPlayer()->reset = true; break;

               default: break;
           }
       }
       else if(gameState == WaitState && e.key.code == gl::key::enter) {
           startRace();
       }
   } else if (e.type == gl::event::key_up) {
         if(gameState == RaceState) {
            glm::vec3 p = scene.getMainPlayer()->getPosition();

             switch(e.key.code) {
                 case gl::key::up: scene.getMainPlayer()->setAccelerating(false, true); break;
				 case gl::key::down: scene.getMainPlayer()->setAccelerating(false, false); brakeOn = false; break;
                 case gl::key::left: scene.getMainPlayer()->setTurningLeft(false); break;
                 case gl::key::right: scene.getMainPlayer()->setTurningRight(false); break;
                 case gl::key::r:
                     scene.rearView(false);
                     scene.resetDistanceBehind();
                     break;
                 case gl::key::v: scene.getMainPlayer()->setJump(false); break;
                 default: break;
             }
         }
   }
   
}

RaceController::~RaceController() {

   startSound->release();
   idleSound->release();
   accelSound->release();
   brakeSound->release();
   bumpSound->release();
   boostSelfSound->release();
   boostOthersSound->release();
   itemSound->release();
   missileSound->release();
   
   system->close();
   system->release();
}

void RaceController::checkSounds() {
	//Iterate through each kart and determine speed and distance from camera
	//Speed determines sound played, closer to camera = louder sound
	std::vector<std::shared_ptr<KartControl> >::const_iterator kart = scene.getKarts().begin();
	for(; kart != scene.getKarts().end(); kart++) {
		//Item found!
		if ((*kart)->gotItem) {
			(*kart)->gotItem = false;
			playSound(SoundEffects::GOTITEM, (*kart)->getSpeed(), (*kart)->getPosition());
		}
		//Boost used!
		if ((*kart)->boostUsed) {
			(*kart)->boostUsed = false;
			playSound(SoundEffects::BOOST, (*kart)->getSpeed(), (*kart)->getPosition());
		}
		//Missile fired!
		if ((*kart)->missileFired) {
			(*kart)->missileFired = false;
			playSound(SoundEffects::MISSILEFIRED, (*kart)->getSpeed(), (*kart)->getPosition());
		}
		//Missile hit!
		if ((*kart)->missileHit) {
			(*kart)->missileHit = false;
			playSound(SoundEffects::CARBUMP, (*kart)->getSpeed(), (*kart)->getPosition());
		}

		//Only do kart sounds for our kart
		if (!((*kart)->getPosition() == scene.getMainPlayer()->getPosition())) { continue; }

		//If we aren't moving backwards we are braking
		if (!(*kart)->checkReverse() && brakeOn && (*kart)->getSpeed() > 3.0) {
			playSound(SoundEffects::BRAKE, (*kart)->getSpeed(), (*kart)->getPosition());
		}
		//If accel, play accel sound
		else if ((*kart)->getAccelerating()) {
			playSound(SoundEffects::ENGINEACCEL, (*kart)->getSpeed(), (*kart)->getPosition());
		}
		//Else, play idle sound
		else {
			playSound(SoundEffects::ENGINEIDLE, (*kart)->getSpeed(), (*kart)->getPosition());
		}
    }
}

void RaceController::createSounds() {
   kartChannel = 0;
   effectChannel = 0;
   musicChannel = 0;

   FMOD::System_Create(&system);

   system->setSpeakerMode(FMOD_SPEAKERMODE_STEREO);
   system->init(100, FMOD_INIT_NORMAL, 0);
   system->set3DSettings(1.0, 1.0f, 1.0f);

   //Initiate each sound effect here and tie it to each of the appropriate sound objects
   system->createSound("assets/sounds/mk64.wav", FMOD_SOFTWARE, 0, &startSound);
   system->createSound("assets/sounds/kartIdleFinal.wav", FMOD_SOFTWARE, 0, &idleSound);
   system->createSound("assets/sounds/kartAccelFinal.wav", FMOD_SOFTWARE, 0, &accelSound);
   system->createSound("assets/sounds/kartNewBrakeFinal.wav", FMOD_SOFTWARE, 0, &brakeSound);
   system->createSound("assets/sounds/kartHit.wav", FMOD_3D, 0, &bumpSound);
   system->createSound("assets/sounds/booster.wav", FMOD_SOFTWARE, 0, &boostSelfSound);
   system->createSound("assets/sounds/booster.wav", FMOD_3D, 0, &boostOthersSound);
   system->createSound("assets/sounds/shotsFired.wav", FMOD_3D, 0, &missileSound);
   system->createSound("assets/sounds/gotItem.wav", FMOD_3D, 0, &itemSound);

   idleSound->set3DMinMaxDistance(15.0f, 5000.0f);
   accelSound->set3DMinMaxDistance(15.0f, 5000.0f);
   bumpSound->set3DMinMaxDistance(15.0f, 5000.0f);
   boostOthersSound->set3DMinMaxDistance(15.0f, 5000.0f);
   missileSound->set3DMinMaxDistance(15.0f, 5000.0f);
   itemSound->set3DMinMaxDistance(15.0f, 5000.0f);
}

void RaceController::playSound(SoundEffects::soundEffect effect, float speed, glm::vec3 kartPos) {
   FMOD::Sound *curSound;
   bool isPlaying;
   glm::vec3 player = scene.getMainPlayer()->getPosition();

   FMOD_VECTOR pos = {kartPos.x, kartPos.y, kartPos.z};
    FMOD_VECTOR vel = {  0.0f, 0.0f, 0.0f };

   switch (effect) {
	case SoundEffects::STARTRACE:
		musicChannel->getCurrentSound(&curSound);
		if (curSound != startSound) {
			musicChannel->stop();
			Sound::SetPause(true);
			system->playSound(FMOD_CHANNEL_FREE, startSound, false, &musicChannel);
		}
		break;
	case SoundEffects::ENGINEIDLE:
		kartChannel->getCurrentSound(&curSound);
		if (curSound != idleSound)
			kartChannel->stop();

		kartChannel->isPlaying(&isPlaying);
		if (!isPlaying) {
			kartChannel->setPaused(false);
			system->playSound(FMOD_CHANNEL_FREE, idleSound, false, &kartChannel);
		}
		kartChannel->set3DAttributes(&pos, &vel);
		break;
	case SoundEffects::ENGINEACCEL:
		kartChannel->getCurrentSound(&curSound);
		if (curSound != accelSound)
			kartChannel->stop();

		kartChannel->isPlaying(&isPlaying);
		if (!isPlaying) {
			kartChannel->setPaused(false);
			system->playSound(FMOD_CHANNEL_FREE, accelSound, false, &kartChannel);
		}
		kartChannel->setVolume((speed + 5) / 35);
		kartChannel->setMode(FMOD_LOOP_NORMAL);
		kartChannel->set3DAttributes(&pos, &vel);
		break;
	case SoundEffects::BRAKE:
		kartChannel->getCurrentSound(&curSound);
		if (curSound != brakeSound)
			kartChannel->stop();

		kartChannel->isPlaying(&isPlaying);
		if (!isPlaying) {
			kartChannel->setPaused(false);
			system->playSound(FMOD_CHANNEL_FREE, brakeSound, false, &kartChannel);
			kartChannel->setVolume(.7);
		}
		kartChannel->setMode(FMOD_LOOP_NORMAL);
		break;
   case SoundEffects::CARBUMP:
        system->playSound(FMOD_CHANNEL_FREE, bumpSound, false, &effectChannel);
		effectChannel->set3DAttributes(&pos, &vel);
		effectChannel->setPaused(false);
		break;
	case SoundEffects::BOOST:
		if (kartPos == player) {
			system->playSound(FMOD_CHANNEL_FREE, boostSelfSound, false, &effectChannel);
			effectChannel->setPaused(false);
		}
		else {
			system->playSound(FMOD_CHANNEL_FREE, boostOthersSound, false, &effectChannel);
			effectChannel->set3DAttributes(&pos, &vel);
			effectChannel->setPaused(false);
	    }
		break;
   case SoundEffects::MISSILEFIRED:
		system->playSound(FMOD_CHANNEL_FREE, missileSound, false, &effectChannel);
		effectChannel->set3DAttributes(&pos, &vel);
		effectChannel->setPaused(false);
		break;
	case SoundEffects::GOTITEM:
		system->playSound(FMOD_CHANNEL_FREE, itemSound, false, &effectChannel);
		effectChannel->set3DAttributes(&pos, &vel);
		effectChannel->setPaused(false);
		break;
	}

	FMOD_VECTOR listenerpos = { player.x, player.y, player.z};
	FMOD_VECTOR forward = { 0.0f, 0.0f, 1.0f };
	FMOD_VECTOR up = { 0.0f, 1.0f, 0.0f };

	FMOD_RESULT result = system->set3DListenerAttributes(0, &listenerpos, &vel, &forward, &up);
	if (result != FMOD_OK)
    {
        printf("FMOD error! (%d)\n", result);
        //exit(-1);
    }

	system->update();
}