#pragma once

#include "Factory.hpp"
#include <exception>
#include <string>

#define GLSL( x ) "#version 150\n" #x

namespace gl {

	namespace shader_type {
		enum shader_type_t {
			vertex = GL_VERTEX_SHADER,
			fragment = GL_FRAGMENT_SHADER,
		};
	}


	class CompileException : public std::exception {
		std::string infoLog;
	public:
		CompileException(const std::string& str) throw() : infoLog(str) { }
		~CompileException() throw() {}
		virtual const char* what() const throw() {
			return (std::string("CompileException: ")+infoLog).c_str();
		}
	};


	class shader {
		GLuint obj;
		factory &factory() {
			static class factory factory;
			return factory;
		}
	public:
		shader( const shader& other ) {
			factory().copy( other.obj, obj );
		}

		shader(shader_type::shader_type_t shader) {
			obj = factory().create(glCreateShader(shader), glDeleteShader);
		}

		shader(shader_type::shader_type_t shader, const std::string& code) {
			obj = factory().create(glCreateShader(shader), glDeleteShader);
			source(code);
			compile();
		}

		~shader() {
			factory().destroy( obj );
		}

		operator GLuint() const {
			return obj;
		}

		const shader& operator=(const shader& other) {
			factory().copy( other.obj, obj, true );
			return *this;
		}

		void source(const std::string& code) {
			const char* c = code.c_str();
			glShaderSource( obj, 1, &c, NULL );
		}

		void compile() {
			GLint res;

			glCompileShader(obj);
			glGetShaderiv(obj, GL_COMPILE_STATUS, &res);

			if (res == GL_FALSE)
				throw CompileException(get_info_log());
		}

		std::string get_info_log() {
			GLint res;
			glGetShaderiv(obj, GL_INFO_LOG_LENGTH, &res);

			if (res > 0) {
				std::string infoLog(res, 0);
				glGetShaderInfoLog(obj, res, &res, &infoLog[0]);
				return infoLog;
			} else {
				return "";
			}
		}
	};
}