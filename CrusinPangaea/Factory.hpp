#pragma once

#include "Platform.hpp"
#include <map>

namespace gl {

#if defined( _WIN32 )
	typedef void ( __stdcall * createFunc ) ( GLsizei, GLuint* );
	typedef void ( __stdcall * deleteFunc ) ( GLsizei, const GLuint* );
	typedef void ( __stdcall * deleteFunc2 ) ( GLuint );
#else
	typedef void ( * createFunc ) ( GLsizei, GLuint* );
	typedef void ( * deleteFunc ) ( GLsizei, const GLuint* );
	typedef void ( * deleteFunc2 ) ( GLuint );
#endif

	class factory {
	public:
   
		void create(GLuint& obj, createFunc c, deleteFunc d) {
			c(1, &obj);
			refs.insert(std::pair<GLuint, uint>(obj, 1));

			this->d = d;
			this->d2 = 0;
		}

		int create(const GLuint& obj, deleteFunc2 d2) {
			refs.insert(std::pair<GLuint, uint>(obj, 1));

			this->d = 0;
			this->d2 = d2;

			return obj;
		}

		void copy(const GLuint& from, GLuint& to, bool destructive = false) {
			if (destructive)
				destroy(to);

			to = from;
			refs[from]++;
		}

		void destroy(GLuint& obj) {
			if ( --refs[obj] == 0 ) {
				if (d != 0) { 
					d(1, &obj);
				} else { 
					d2( obj );
				}
				refs.erase(obj);
			}
		}

	private:
		std::map<GLuint, uint> refs;
		deleteFunc d;
		deleteFunc2 d2;
	};
}