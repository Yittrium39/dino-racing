//
//  PlayerPhysics.h
//  CrusinPangaea
//
//  Created by Richie on 2/18/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#pragma once

#include "World.h"
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

class KartControl {
   physics::Kart *physicsKart;
   SceneObject *renderKart;
   bool enabled;

   public:
   bool powerup, boostUsed, missileFired, missileHit, gotItem, reset;

   KartControl(physics::Kart *p, SceneObject *r) : physicsKart(p), renderKart(r), enabled(false) { }
   
   void update() {
      physicsKart->update();
      renderKart->setTranslation(physicsKart->getPosition());
      renderKart->setRotation(physicsKart->getRotation());
   }

   void enablePowerup(bool a) { powerup = a; }
   
   void enableKart(bool a) { enabled = a; }
   void setAccelerating(bool a, bool b) { if (enabled) physicsKart->setAccelerating(a, b); }
   void setBraking(bool a) { if (enabled) physicsKart->setBraking(a); }
   void setTurningLeft(bool a) { if(enabled) physicsKart->setTurningLeft(a); }
   void setTurningRight(bool a) { if(enabled) physicsKart->setTurningRight(a); }
   void setBoost(bool a) { if(enabled) physicsKart->setBoost(a); }
   void setPowerUpBoost(bool a) { if(enabled) physicsKart->setPowerUpBoost(a); }
   void setJump(bool a) { if(enabled) physicsKart->setJump(a); }
   void setDrop(bool a) { if(enabled) physicsKart->setDrop(a); }



   void setPosition(const glm::vec3 &p) { physicsKart->setPosition(p); }
   void setRotation(const glm::quat &q) { physicsKart->setRotation(q); }
   glm::vec3 getPosition() const { return physicsKart->getPosition(); }
   glm::quat getRotation() const { return physicsKart->getRotation(); }
   bool getBraking() const { return physicsKart->getBraking(); }
   bool getAccelerating() const { return physicsKart->getAccelerating(); }
   bool getPowerUpBoost() const { return physicsKart->getPowerUpBoost(); }
   bool checkReverse() const { return physicsKart->checkReverse(); }
   float getSpeed() const { return physicsKart->getSpeed(); }
};