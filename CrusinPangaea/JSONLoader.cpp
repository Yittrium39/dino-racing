#include <string>
#include <fstream>
#include <streambuf>

#include "cJSON.h"
#include "JSONLoader.h"

using namespace std;
using namespace glm;

size_t GetFileSize(std::ifstream &file) {
	long pos = file.tellg();
	file.seekg(0, ios_base::end);
	size_t dataSize = file.tellg();
	file.seekg(pos, ios_base::beg);
	return dataSize;
}

std::shared_ptr<JSONNode> JSONLoader::load(string filename) {
	std::ifstream file(filename.c_str());
	if (!file) {
      throw JSONException(std::string("Could not load file: ")+filename);
   }
   
   std::string contents((std::istreambuf_iterator<char>(file)),std::istreambuf_iterator<char>());

	std::shared_ptr<JSONNode> root = std::shared_ptr<JSONNode>(cJSON_Parse(contents.c_str()));
   if (root == NULL) {
      throw JSONException(std::string("Error parsing ")+filename+" : "+cJSON_GetErrorPtr());
	}
   return root;
}

JSONNode *JSONLoader::getNode(JSONNode *parentNode, string name) {
	JSONNode *myNode = cJSON_GetObjectItem(parentNode, name.c_str());

	if (myNode == NULL) {
      throw JSONException(name + " does not exist");
	}
	return myNode;
}

std::vector<JSONNode*> JSONLoader::getNodes(JSONNode *parentNode, string name) {
   std::vector<JSONNode*> nodes;
   JSONNode *parent = getNode(parentNode, name);
   int numItems = cJSON_GetArraySize(parent);
   for (int i = 0; i < numItems; i++) {
      nodes.push_back(cJSON_GetArrayItem(parent, i));
   }
   return nodes;
}

JSONNode *JSONLoader::getNode(JSONNode *parentNode, int i) {
	return cJSON_GetArrayItem(parentNode, i);
}

int JSONLoader::getInt(JSONNode *parentNode, string name, int _default) {
	if (parentNode != NULL) {
		JSONNode *node = getNode(parentNode, name);
		if (node != NULL)
			return node->valueint;
	}

	return _default;
}

vec2 JSONLoader::getVec2(JSONNode *parentNode, string name, vec2 _default) {
	if (parentNode != NULL) {
		JSONNode *node = getNode(parentNode, name);
		if (node != NULL) {
			glm::vec2 vec;
			vec.x = float(getNode(node, 0)->valuedouble);
			vec.y = float(getNode(node, 1)->valuedouble);
			return vec;
		}
	}

	return _default;
}

vec3 JSONLoader::getVec3(JSONNode *parentNode, string name, vec3 _default) {
	if (parentNode != NULL) {
		JSONNode *node = getNode(parentNode, name);
		if (node != NULL) {
			glm::vec3 vec;
			vec.x = float(getNode(node, 0)->valuedouble);
			vec.y = float(getNode(node, 1)->valuedouble);
			vec.z = float(getNode(node, 2)->valuedouble);
			return vec;
		}
	}

	return _default;
}

vec4 JSONLoader::getVec4(JSONNode *parentNode, string name, vec4 _default) {
	if (parentNode != NULL) {
		JSONNode *node = getNode(parentNode, name);
		if (node != NULL) {
			glm::vec4 vec;
			vec.x = float(getNode(node, 0)->valuedouble);
			vec.y = float(getNode(node, 1)->valuedouble);
			vec.z = float(getNode(node, 2)->valuedouble);
			vec.w = float(getNode(node, 3)->valuedouble);
			return vec;
		}
	}

	return _default;
}

float JSONLoader::getFloat(JSONNode *parentNode, std::string name, float _default) {
	if (parentNode != NULL) {
		JSONNode *node = getNode(parentNode, name);
		if (node != NULL)
			return float(node->valuedouble);
	}

	return _default;
}

int JSONLoader::getCount(JSONNode *parentNode) {
	if (parentNode != NULL) {
		return cJSON_GetArraySize(parentNode);
	}

	return 0;
}

string JSONLoader::getString(JSONNode *parentNode, string name) {
	if (parentNode != NULL) {
		JSONNode *node = getNode(parentNode, name.c_str());
		if (node != NULL)
			return node->valuestring;
	}

	return string("");
}