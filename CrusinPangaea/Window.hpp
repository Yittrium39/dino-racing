#pragma once

#include "Platform.hpp"

#include <assert.h>

namespace gl {

	namespace mouse_button {
		enum mouse_button_t { left, right, middle, COUNT };
	}

	struct mouse_event {
		uint x, y;
		union {
			int delta;
			mouse_button::mouse_button_t button;
		};
	};

	namespace key {
		enum key_t {
			unknown,
			a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z,
			escape, space, enter, left, right, up, down,
         COUNT
		};
	}

	struct key_event {
		key::key_t code;
		bool alt;
		bool control;
		bool shift;
	};

	struct window_event {
		union { int x; uint width; };
		union { int y; uint height; };
	};

	struct event {
		enum event_t {
			unknown, close, key_down, key_up, mouse_down, mouse_up, mouse_move
		};

		event_t type;

		union {
			mouse_event mouse;
			key_event key;
			window_event window;
		};
	};

   class abstract_window {
      public:
      abstract_window() { }
      virtual ~abstract_window() { }
      
      virtual void draw() = 0;
      virtual void update() = 0;
      virtual void event(event &e) = 0;
      virtual void run() = 0;
   };

}