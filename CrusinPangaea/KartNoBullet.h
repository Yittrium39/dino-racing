#pragma once

#include "World.h"
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

class KartNoBullet : public physics::Kart {
   friend class WorldNoBullet;
   bool turningLeft;
   bool braking;
   bool accelerating;
   bool turningRight;
   bool boost;
   bool forward;
   float speed;
   float turn;
   glm::quat rotation;
   glm::vec3 position;
   
   void move(float deltaT);
public:
   KartNoBullet();
   
   void setAccelerating(bool a, bool b) { accelerating = a; forward = b; }
   void setBraking(bool a) { braking = a; }
   void setTurningLeft(bool a) { turningLeft = a; }
   void setTurningRight(bool a) { turningRight = a; }
   void setBoost(bool a) { boost = a; }
   void setPowerUpBoost(bool a) { setBoost(a); }
   void setJump(bool a) { setBoost(a); }
   void setDrop(bool a) { setBoost(a); }


   
   void setPosition(const glm::vec3 &p) { position = p; }
   void setRotation(const glm::quat &q) { rotation = q; }
   glm::vec3 getPosition() const { return position; }
   glm::quat getRotation() const { return rotation; }
   bool getBraking() const { return getBraking(); }
   bool getAccelerating() const { return getAccelerating(); }
   bool getPowerUpBoost() const { return getPowerUpBoost(); }
   bool checkReverse() const { return checkReverse(); }
   float getSpeed() const { return getSpeed(); }
};