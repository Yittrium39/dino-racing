//
//  RaceController.h
//  CrusinPangaea
//
//  Created by Richie on 2/14/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#pragma once

#ifdef _WIN32
#define GLEW_STATIC
#endif

#include "Controller.h"
#include "Scene.h"
#include "Panel.h"
#include "HUD.h"
#include "GameLogic.h"
#include "Spline.h"
#include "Rockets.h"
#include "FMODsound.h"
#include "fmod.hpp"
#include "BulletKart.h"

//Sound enums to play appropriate sound effect
namespace SoundEffects {
    enum soundEffect {
       STARTRACE, ENDRACE, GETREADY, 
       LAPCOMPLETE, ENGINEIDLE, ENGINEACCEL,
       BRAKE, CARBUMP, BOOST, MISSILEFIRED, GOTITEM
    };  
};

class RaceController : public Controller {
   Scene scene;
   Panel spanel;
   HUD hud;
    GameState gameState;
    float stateTimer;
    GameLogic gameLogic;
    std::string finalPlace;
    bool once, brakeOn;
    double highScores[11];
   
   FMOD::System *system;
   FMOD::Sound *startSound, *endSound, *readySound, *lapSound, *idleSound, *accelSound, *brakeSound, *bumpSound, *boostSelfSound, *boostOthersSound, *missileSound, *itemSound;
   FMOD::Channel *kartChannel, *effectChannel, *musicChannel;  
   
   void checkSounds();
   void createSounds();
   void playSound(SoundEffects::soundEffect effect, float speed, glm::vec3 kartPos);
   
   public:
   RaceController(gl::window &w);
   virtual void Update(float deltaT);
	virtual void Render();
	virtual void Event(gl::event &e);
   void startRace();
   void updateHighScores();
   virtual ~RaceController();
};