//
//  Text.h
//  CrusinPangaea
//
//  Created by Richie on 2/18/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#pragma once

#ifdef _WIN32
#define GLEW_STATIC
#endif

#include "VertexArray.hpp"
#include "Program.hpp"
#include "Texture.hpp"
#include <exception>

#include <ft2build.h>
#include FT_FREETYPE_H

struct FontException : std::exception {
   const char* whatStr;
   FontException(const char * str) : whatStr(str) { }
   char const* what() const throw() {
      return whatStr;
   }
};

class Font {
   static FT_Library &getFTLibrary();
   static gl::vertex_array &getGlyphVAO();
   static gl::vertex_buffer &getGlyphVBO();
   static gl::program &getGlyphProgram();
   
   // static initializers
   static gl::vertex_array makeGlyphVAO(gl::vertex_buffer &vbo);
   static gl::vertex_buffer makeGlyphVBO();
   static FT_Library makeFTLibrary();
   static gl::program makeGlyphProgram();
   
   FT_Face face;
   gl::texture fontFace;
   int faceSize;
   
   int atlasWidth;
   int atlasHeight;
   struct character_info {
      float ax; // advance.x
      float ay; // advance.y

      float bw; // bitmap.width;
      float bh; // bitmap.rows;
 
      float bl; // bitmap_left;
      float bt; // bitmap_top;
 
      float tx; // x offset of glyph in texture coordinates
   } c[128];

public:
   Font(const std::string &fontFile, int faceSize);
   void drawGlyph(int w, int h, const std::string &str, int x, int y);
};