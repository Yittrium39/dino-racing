
#pragma once

#include "Renderable.h"
#include <memory>
#include <list>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

#include "StaticMesh.h"

class SceneObject {
   glm::vec3 mTranslation;
   glm::quat mRotation;
   glm::vec3 mScale;
   
   std::list<std::shared_ptr<Renderable> > mRenderables;
   
   // This is kept for legacy reasons, but it is not guaranteed to be set to anything
   // or work. Do not use this in any new code.
   mutable std::shared_ptr<StaticMesh> mMesh;

   public:
   SceneObject() : mTranslation(0), mRotation(0,0,0,0), mScale(1) { }
   explicit SceneObject(std::shared_ptr<Renderable> r)  : mTranslation(0), mRotation(0,0,0,0), mScale(1) { mRenderables.push_back(r); }
   
   template <class InputIterator>
   SceneObject(InputIterator first, InputIterator last) : mTranslation(0), mRotation(0,0,0,0), mScale(1), mRenderables(first, last) { }
   
   template <typename vec3>
   void setTranslation(const vec3 &t) {
      mTranslation.x = t.x;
      mTranslation.y = t.y;
      mTranslation.z = t.z;
   }
   
   template <typename vec3>
   void setScale(const vec3 &t) {
      mScale.x = t.x;
      mScale.y = t.y;
      mScale.z = t.z;
   }
   
   template <typename quat>
   void setRotation(const quat &rotation) {
      mRotation.w = rotation.w;
      mRotation.x = rotation.x;
      mRotation.y = rotation.y;
      mRotation.z = rotation.z;
   }
   
   
   const glm::vec3 &getTranslation() const { return mTranslation; }
   const glm::quat &getRotation() const { return mRotation; }
   const glm::vec3 &getScale() const { return mScale; }
   
   glm::mat4 getModelMatrix() const {
      glm::mat4 rotation = glm::mat4_cast(mRotation);
      glm::mat4 scale = glm::scale(glm::mat4(1.0), mScale);
      glm::mat4 translate = glm::translate(glm::mat4(1.0), mTranslation);
      return translate * rotation * scale;
   };
   
   std::list<std::shared_ptr<Renderable> >::const_iterator begin() const {
      return mRenderables.begin();
   }
   
   std::list<std::shared_ptr<Renderable> >::const_iterator end() const {
      return mRenderables.end();
   }
   
   // Kept for legacy reasons, however not guarenteed to be stable or work
   // Don't use these in any new code
   StaticMesh *getMesh() const { return &*mMesh; }

   void setMesh(std::shared_ptr<StaticMesh> mesh) const { mMesh = mesh; }

};