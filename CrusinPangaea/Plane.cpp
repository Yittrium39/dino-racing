

#include "Plane.h"
#include "Color.hpp"


Plane::Plane() {
   float quad[] = {
		-0.5f, -0.5f,  1.0f,    0.f, 0.f, 1.0f,    1.f, 0.f, 1.f,
		 0.5f, -0.5f,  1.0f,    0.f, 0.f, 1.0f,    1.f, 0.f, 1.f,
		 0.5f,  0.5f,  1.0f,    0.f, 0.f, 1.0f,    1.f, 0.f, 0.f,
		-0.5f, -0.5f,  1.0f,    0.f, 0.f, 1.0f,    1.f, 0.f, 1.f,
		 0.5f,  0.5f,  1.0f,    0.f, 0.f, 1.0f,    1.f, 0.f, 0.f,
		-0.5f,  0.5f,  1.0f,    0.f, 0.f, 1.0f,    1.f, 0.f, 0.f
	};
   unsigned int indices[] = { 0, 1, 2, 3, 4, 5 };
   vbo = gl::vertex_buffer(quad, sizeof(quad), gl::buffer_usage::static_draw);
   ibo = gl::vertex_buffer(indices, sizeof(indices), gl::buffer_usage::static_draw);
   
   mat.diffuse.color = glm::vec3(1.0f, 0, 0);
   mat.diffuse.weight = 1.0f;
}

DrawInfo Plane::getDrawInfo() const {
   DrawInfo info;
   info.mode = gl::primitive::triangles;
   info.num_elements = 6;
   info.draw_type = DrawType::elements;
   info.ibo = ibo;
   info.index_type = gl::type::UnsignedInt;
   return info;
}

// Specifies which attributes the vbo has and where they are located
bool Plane::getAttributeInfo(const ShaderContract &c, AttributeInfo &info) const {
   info.stride_bytes = 9 * sizeof(GLfloat);
   info.buffer = vbo;
   info.data_type = gl::type::Float;
   
   switch (c.attribute_type) {
      case ShaderAttribute::Position:
         if (c.size != 3) return false;
         info.start_bytes = 0 * sizeof(GLfloat);
         break;
      case ShaderAttribute::Normal:
         if (c.size != 3) return false;
         info.start_bytes = 3 * sizeof(GLfloat);
         break;
      default:
         return false;
   };
   return true;
}