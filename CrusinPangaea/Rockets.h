//
//  Rockets.h
//  CrusinPangaea
//
//  Created by Robert Burton on 2/25/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include "KartControl.h"
#include "Spline.h"
#include "Particles.h"

typedef struct {
    glm::vec3 location;
    glm::vec3 direction;
    float velocity;
    glm::vec3 color;
    int active;
    //add a "Kart target"
    KartControl *target;
} Rocket;

class Rockets {
    
public:
    void update(float deltaT, Particles particles);
    void addRocket(glm::vec3 location, glm::vec3 direction, float velocity, KartControl* target);
    std::vector<Rocket> rockets;    
};
