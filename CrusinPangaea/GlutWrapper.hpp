/*
 * GlutWrapper
 * Version 0.3
 * Richie Steigerwald
 */

#pragma once

#include <map>
#include <string>
#include <exception>

#include "Window.hpp"

namespace glut {

// An abstract class for a basic GLUT window. Extend this with a window that
// does something interesting
class Window : public gl::abstract_window {
   private:
   int mHandle;

   public:
   Window();
   explicit Window(const std::string &title);
   virtual ~Window();

   virtual void draw() = 0;
   virtual void update() { };
   virtual void event(gl::event &e) { };
   void run();
};


// In case somebody creates a GLUT window without using the GLUT manager
struct UnmanagedWindowException : std::exception { 
   char const* what() const throw() {
      return "Callback called un unmanaged GLUT window";
   }
};


// A wrapper class for GLUT. Since GLUT is a single system resource we will
// have a singleton wrapper manage it.
class WindowManager {
   friend class Window;

   // A map from GLUT window handles to windows
   static std::map<int, Window *> mWindows;

   // These are called by window when they are initialized and deleted
   int createWindow(const std::string &title, Window &window);
   void removeWindow(int window);

   // Gets the window of context for the callback
   // This may throw an UnmanagedWindowException
   static Window &currentWindow();

   // Global callback functions for GLUT
   static void idleCallback();

   // Callback functions call relevant functions on corresponding windows.
   static void displayCallback();
   static void keyboardCallback(unsigned char key, int x, int y);
   static void keyboardUpCallback(unsigned char key, int x, int y);
   static void specialCallback(int key, int x, int y);
   static void specialUpCallback(int key, int x, int y);
   
   static gl::key_event map_key(unsigned char key);
   static gl::key_event map_special(int key);

   // Singleton boilerplate code
   public:
   static WindowManager& getInstance() {
      static WindowManager singleton; 
      return singleton;
   }

   // Call this to run glutMainLoop once all windows are initialized
   void runMainLoop();

   private:
   WindowManager();
   WindowManager(WindowManager const&);
   void operator=(WindowManager const&);
};

}