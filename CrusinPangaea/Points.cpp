//
//  Points.cpp
//  CrusinPangaea
//
//  Created by Richie on 2/26/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#include "Points.h"
#include "Color.hpp"

Points::Points() {   
   mat.diffuse.color = glm::vec3(1,0,0);
   mat.diffuse.weight = 1.0f;
}

void Points::addPoint(const glm::vec3 &point) {
   points.push_back(point);
   vbo = gl::vertex_buffer(&points[0], points.size() * 3 * sizeof(float), gl::buffer_usage::static_draw);
}

DrawInfo Points::getDrawInfo() const {
   DrawInfo info;
   info.mode = gl::primitive::points;
   info.num_elements = (unsigned int)points.size();
   info.draw_type = DrawType::arrays;
   return info;
}

// Specifies which attributes the vbo has and where they are located
bool Points::getAttributeInfo(const ShaderContract &c, AttributeInfo &info) const {
   info.stride_bytes = 3 * sizeof(GLfloat);
   info.buffer = vbo;
   info.data_type = gl::type::Float;
   
   switch (c.attribute_type) {
      case ShaderAttribute::Position:
         if (c.size != 3) return false;
         info.start_bytes = 0 * sizeof(GLfloat);
         break;
      default:
         return false;
   };
   return true;
}