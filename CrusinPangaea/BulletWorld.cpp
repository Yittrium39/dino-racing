//    _______    ___             _         _             ___           _          
//   |__ /   \  | _ \___ _ _  __| |___ _ _(_)_ _  __ _  | __|_ _  __ _(_)_ _  ___ 
//    |_ \ |) | |   / -_) ' \/ _` / -_) '_| | ' \/ _` | | _|| ' \/ _` | | ' \/ -_)
//   |___/___/  |_|_\___|_||_\__,_\___|_| |_|_||_\__, | |___|_||_\__, |_|_||_\___|
//    ___           _           ___          _   |___/_          |___/            
//   / __| ___ _ _ (_)___ _ _  | _ \_ _ ___ (_)___ __| |_                         
//   \__ \/ -_) ' \| / _ \ '_| |  _/ '_/ _ \| / -_) _|  _|                        
//   |___/\___|_||_|_\___/_|   |_| |_| \___// \___\__|\__|                        
//                                        |__/             
//
//   Kevin Ubay-Ubay (kubayubay@gmail.com)
//

#include <iostream>
#include <btBulletDynamicsCommon.h>
#include <BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include "BulletWorld.h"
#include "BulletKart.h"

namespace physics {

BulletWorld::BulletWorld(float gravity) {
   btVector3 worldAabbMin(-10000, -10000, -10000);
	btVector3 worldAabbMax(10000, 10000, 10000);
	int maxProxies = 1024;

	broadphase = new btAxisSweep3(worldAabbMin, worldAabbMax, maxProxies);

	collisionConfiguration = new btDefaultCollisionConfiguration();
	dispatcher = new btCollisionDispatcher(collisionConfiguration);
	solver = new btSequentialImpulseConstraintSolver;

	dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfiguration);
	dynamicsWorld->setGravity(btVector3(0, gravity, 0));
}

#define CUBE_HALF_EXTENTS 1.3
physics::Kart* BulletWorld::makeKart() {
   physics::BulletKart *kart = new physics::BulletKart();
	btVector3 wheelDirectionCS0(0, -1, 0);
	btVector3 wheelAxleCS(-1, 0, 0);
	int rightIndex = 0;
	int upIndex = 1;
	int forwardIndex = 2;
	float wheelRadius = kart->getWheelRadius();
	float wheelWidth = kart->getWheelWidth();
	btScalar sRestLength = kart->getSuspensionRestLength();

	dynamicsWorld->getBroadphase()->getOverlappingPairCache()->cleanProxyFromPairs(kart->getVehicleRigidBody()->getBroadphaseHandle(), dynamicsWorld->getDispatcher());
	dynamicsWorld->addRigidBody(kart->getVehicleRigidBody());

	kart->kartRaycaster = new btDefaultVehicleRaycaster(dynamicsWorld);
	kart->kartVehicle = new btRaycastVehicle(kart->kartTuning, kart->getVehicleRigidBody(), kart->getRaycaster());

	kart->getVehicleRigidBody()->setActivationState(DISABLE_DEACTIVATION);
	dynamicsWorld->addVehicle(kart->kartVehicle);

	float connectionHeight = 1.2f;

	kart->getRaycast()->setCoordinateSystem(rightIndex, upIndex, forwardIndex);

	bool isFrontWheel = true;
	btVector3 connectionPointCS0(1.8*CUBE_HALF_EXTENTS-(1*wheelWidth), connectionHeight, 3.7*CUBE_HALF_EXTENTS-wheelRadius);
	kart->getRaycast()->addWheel(connectionPointCS0, wheelDirectionCS0, wheelAxleCS, sRestLength, wheelRadius, kart->kartTuning, isFrontWheel);

	connectionPointCS0 = btVector3(-1.8*CUBE_HALF_EXTENTS+(1*wheelWidth), connectionHeight, 3.7*CUBE_HALF_EXTENTS-wheelRadius);
	kart->getRaycast()->addWheel(connectionPointCS0, wheelDirectionCS0, wheelAxleCS, sRestLength, wheelRadius, kart->kartTuning, isFrontWheel);

	isFrontWheel = false;
	connectionPointCS0 = btVector3(-1.8*CUBE_HALF_EXTENTS+(1*wheelWidth), connectionHeight, -3*CUBE_HALF_EXTENTS+wheelRadius);
	kart->getRaycast()->addWheel(connectionPointCS0, wheelDirectionCS0,wheelAxleCS, sRestLength,wheelRadius, kart->kartTuning, isFrontWheel);

	connectionPointCS0 = btVector3(1.8*CUBE_HALF_EXTENTS-(1*wheelWidth), connectionHeight, -3*CUBE_HALF_EXTENTS+wheelRadius);
	kart->getRaycast()->addWheel(connectionPointCS0, wheelDirectionCS0, wheelAxleCS, sRestLength,wheelRadius, kart->kartTuning, isFrontWheel);

	for (int i = 0; i < kart->getRaycast()->getNumWheels(); i++) {
		btWheelInfo& wheel = kart -> getRaycast()->getWheelInfo(i);
		wheel.m_suspensionStiffness = kart->getSuspensionStiffness();
		wheel.m_wheelsDampingRelaxation = kart->getSuspensionDamping();
		wheel.m_wheelsDampingCompression = kart->getSuspensionCompression();
		wheel.m_frictionSlip = kart->getWheelFriction();
		wheel.m_rollInfluence = kart->getRollInfluence();
	}
   return kart;
}

void BulletWorld::setTrack(SceneObject* raceTrack) {
	int vertStride = sizeof(btVector3);
	int indexStride = 3*sizeof(int);
	
   int totalVerts = raceTrack->getMesh()->vertices.size();
	int totalIndices = raceTrack->getMesh()->indices.size();
   int totalTriangles = totalIndices / 3;

   int *indices = new int[totalIndices];
	btVector3 *vertices = new btVector3[totalVerts];

   for (int i = 0; i < totalIndices; i++) {
      indices[i] = raceTrack->getMesh()->indices[i];
   }
   
	for (int i = 0; i < totalVerts; i++) {
		glm::vec4 myVertex = raceTrack->getModelMatrix() * glm::vec4(raceTrack->getMesh()->vertices[i], 1.0f);
		myVertex.x /= myVertex.w;
		myVertex.y /= myVertex.w;
		myVertex.z /= myVertex.w;
		vertices[i] = btVector3(myVertex.x, myVertex.y, myVertex.z);
	}

	class btTriangleIndexVertexArray* indexVertexArray = new btTriangleIndexVertexArray(totalTriangles, indices, indexStride, totalVerts, (btScalar *)&vertices[0].x(), vertStride);

	btCollisionShape *myShape = new btBvhTriangleMeshShape(indexVertexArray, true);

	collisionShapes.push_back(myShape);

	btTransform transform;
	transform.setIdentity();
	transform.setOrigin(btVector3(0,0,0));

	btDefaultMotionState *groundMotionState = new btDefaultMotionState(transform);
	btRigidBody::btRigidBodyConstructionInfo groundRigidBodyCI(0.0f, groundMotionState, myShape, btVector3(0, 0, 0));
	btRigidBody *groundRigidBody = new btRigidBody(groundRigidBodyCI);
	dynamicsWorld->addRigidBody(groundRigidBody);
}

void BulletWorld::update(float timeStep) {
	btScalar dt = timeStep;
#ifndef _WIN32
	dynamicsWorld->stepSimulation(dt, 60);
#else
	dynamicsWorld->stepSimulation(clock.getTimeMicroseconds() / 1000000.0f, 60);
	clock.reset();
#endif
}

void BulletWorld::clean() {
	delete dynamicsWorld;
	delete solver;
	delete dispatcher;
	delete collisionConfiguration;
	delete broadphase;
}

}