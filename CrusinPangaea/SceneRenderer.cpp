

#include "SceneRenderer.h"
#include "SkinShader.h"
#include "SkinMesh.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <exception>
#include <iostream>

SceneRenderer::SceneRenderer() : gbuffer(800, 600) {
   deferred = true;
}

SceneObjectShader *SceneRenderer::addShaderForRenderable(const Renderable &renderable) {
   mShaders.insert(std::make_pair(renderable.getID(), SceneObjectShader()));
   SceneObjectShader *shader = &mShaders[renderable.getID()];
   
   if (skinShader.canRenderRenderable(renderable)) {
	   shader->program = &skinShader;
	   shader->vao = skinShader.vaoForRenderable(renderable);
   } else if (textureShader.canRenderRenderable(renderable)) {
      shader->program = &textureShader;
      shader->vao = textureShader.vaoForRenderable(renderable);
   } else if (flatShader.canRenderRenderable(renderable)){
      shader->program = &flatShader;
      shader->vao = flatShader.vaoForRenderable(renderable);
   } else {
      throw std::runtime_error("cannot find appropriate shader for renderable.");
   }

   shader->material = renderable.getMaterial();

   shader->info = renderable.getDrawInfo();
   if (shader->info.draw_type == DrawType::elements) {
      shader->vao.bind_elements(shader->info.ibo);
   }
      
   return shader;
}


void SceneRenderer::enqueue(const SceneObject &o) {
   std::list<std::shared_ptr<Renderable> >::const_iterator renderable;
   for (renderable = o.begin(); renderable != o.end(); renderable++) {
      enqueue(**renderable, o.getModelMatrix());
   }
}


void SceneRenderer::enqueue(const Renderable &r, const glm::mat4 &transpose) {
   // Get the shader program the object being rendered
   std::map<unsigned long, SceneObjectShader>::iterator shader;
   shader = mShaders.find(r.getID());

   RenderTask task;
   task.currentSkinMesh = r.currentSkinMesh;
   task.depth = 0.f;
   task.transpose = transpose;
   
   // Check to see if we have a vao for the object in question, if not
   // then create one
   if (shader == mShaders.end()) {
      task.object = addShaderForRenderable(r);
   } else {
      task.object = &shader->second;
   }

   mRenderQueue.insert(task);
}

void SceneRenderer::setCamera(const Camera &c) {
   if (mRenderQueue.size() > 0) {
      throw std::runtime_error("SceneRenderer: Camera must be set before any objects are enqueued");
   }
   mCamera = c;
}

bool SceneRenderer::inFrustum(glm::mat4 MVP, glm::vec3 center, float radius)
{
   float distance = std::abs(center.x*(MVP[0][0]+MVP[0][3]) + center.y*(MVP[1][0]+MVP[1][3]) + center.z*(MVP[2][0]+MVP[2][3]) + (MVP[3][0]+MVP[3][3]))/sqrt((MVP[0][0]+MVP[0][3])*(MVP[0][0]+MVP[0][3])+(MVP[1][0]+MVP[1][3])*(MVP[1][0]+MVP[1][3])+(MVP[2][0]+MVP[2][3])*(MVP[2][0]+MVP[2][3]));
   if(center.x * (MVP[0][0] + MVP[0][3]) + center.y * (MVP[1][0] + MVP[1][3]) + center.z * (MVP[2][0] + MVP[2][3]) + (MVP[3][0] + MVP[3][3]) > 0.0f || std::abs(distance) < radius) { //(ABCD) = R1 + R4 : Left
      distance = std::abs(center.x*(-MVP[0][0]+MVP[0][3]) + center.y*(-MVP[1][0]+MVP[1][3]) + center.z*(-MVP[2][0]+MVP[2][3]) + (-MVP[3][0]+MVP[3][3]))/sqrt((-MVP[0][0]+MVP[0][3])*(-MVP[0][0]+MVP[0][3])+(-MVP[1][0]+MVP[1][3])*(-MVP[1][0]+MVP[1][3])+(-MVP[2][0]+MVP[2][3])*(-MVP[2][0]+MVP[2][3]));
      if(center.x * (-MVP[0][0] + MVP[0][3]) + center.y * (-MVP[1][0] + MVP[1][3]) + center.z * (-MVP[2][0] + MVP[2][3]) + (-MVP[3][0] + MVP[3][3]) > 0.0f  || std::abs(distance) < radius) { //(ABCD) = -R1 + R4 : Right
         distance = std::abs(center.x*(MVP[0][1] + MVP[0][3]) + center.y*(MVP[1][1] + MVP[1][3]) + center.z*(MVP[2][1] + MVP[2][3]) + (-MVP[3][0]+MVP[3][3]))/sqrt((MVP[0][1] + MVP[0][3])*(MVP[0][1] + MVP[0][3])+(MVP[1][1] + MVP[1][3])*(MVP[1][1] + MVP[1][3])+(MVP[2][1] + MVP[2][3])*(MVP[2][1] + MVP[2][3]));
         if(center.x * (MVP[0][1] + MVP[0][3]) + center.y * (MVP[1][1] + MVP[1][3]) + center.z * (MVP[2][1] + MVP[2][3]) + (MVP[3][1] + MVP[3][3]) > 0.0f  || std::abs(distance) < radius) { //(ABCD) = R2 + R4 : Bottom
            distance = std::abs(center.x*(-MVP[0][1] + MVP[0][3]) + center.y*(-MVP[1][1] + MVP[1][3]) + center.z*(-MVP[2][1] + MVP[2][3]) + (-MVP[3][1] + MVP[3][3]))/sqrt((-MVP[0][1] + MVP[0][3])*(-MVP[0][1] + MVP[0][3])+(-MVP[1][1] + MVP[1][3])*(-MVP[1][1] + MVP[1][3])+(-MVP[2][1] + MVP[2][3])*(-MVP[2][1] + MVP[2][3]));
            if(center.x * (-MVP[0][1] + MVP[0][3]) + center.y * (-MVP[1][1] + MVP[1][3]) + center.z * (-MVP[2][1] + MVP[2][3]) + (-MVP[3][1] + MVP[3][3]) > 0.0f  || std::abs(distance) < radius) { //(ABCD) = -R2 + R4 : Top
               distance = std::abs(center.x*(MVP[0][2] + MVP[0][3]) + center.y*(MVP[1][2] + MVP[1][3]) + center.z*(MVP[2][2] + MVP[2][3]) + (MVP[3][2] + MVP[3][3]))/sqrt((MVP[0][2] + MVP[0][3])*(MVP[0][2] + MVP[0][3])+(MVP[1][2] + MVP[1][3])*(MVP[1][2] + MVP[1][3])+(MVP[2][2] + MVP[2][3])*(MVP[2][2] + MVP[2][3]));
               if(center.x * (MVP[0][2] + MVP[0][3]) + center.y * (MVP[1][2] + MVP[1][3]) + center.z * (MVP[2][2] + MVP[2][3]) + (MVP[3][2] + MVP[3][3]) > 0.0f  || std::abs(distance) < radius) { //(ABCD) = R3 + R4 : Near
                  distance = std::abs(center.x*(-MVP[0][2] + MVP[0][3]) + center.y*(-MVP[1][2] + MVP[1][3]) + center.z*(-MVP[2][2] + MVP[2][3]) + (-MVP[3][2] + MVP[3][3]))/sqrt((-MVP[0][2] + MVP[0][3])*(-MVP[0][2] + MVP[0][3])+(-MVP[1][2] + MVP[1][3])*(-MVP[1][2] + MVP[1][3])+(-MVP[2][2] + MVP[2][3])*(-MVP[2][2] + MVP[2][3]));
                  if(center.x * (-MVP[0][2] + MVP[0][3]) + center.y * (-MVP[1][2] + MVP[1][3]) + center.z * (-MVP[2][2] + MVP[2][3]) + (-MVP[3][2] + MVP[3][3]) > 0.0f  || std::abs(distance) < radius) { //(ABCD) = -R3 + R4 : Far
                     return true;
                  }
               }
            }
         }
      }
   }
   return false;
}

void SceneRenderer::render() {
   glEnable(GL_DEPTH_TEST);
   glClearColor(0.5,0.5,1.0,1.0);
   glClear(GL_DEPTH_BUFFER_BIT|GL_COLOR_BUFFER_BIT);
   glClearColor(0.0,0.0,0.0,0.0);
   std::set<RenderTask>::iterator renderTask;
   
   glm::vec3 lookat_diff = mCamera.getPosition() - mCamera.lookat;
   glm::vec3 position = mCamera.getPosition() + lookat_diff;
   mSpotLight.setPosition(glm::vec3(position.x, position.y + 75 ,position.z));
   mSpotLight.setLookAt(mCamera.lookat);
   mSpotLight.setPitch(mSpotLight.getPitch()-10);
   
   gbuffer.begin_shadow();

   glClearColor(.7,.7,1.0, 1.0);
   glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
   glClearColor(0,0,0,0);
   for (renderTask = mRenderQueue.begin(); renderTask != mRenderQueue.end(); renderTask++) {
      SceneObjectShader *objectShader = renderTask->object;
	  
      objectShader->program->use();
      objectShader->program->setModel(renderTask->transpose);
      objectShader->program->setView(mSpotLight.getViewMatrix());
      objectShader->program->setProjection(mSpotLight.getProjMatrix(1.3));
      objectShader->program->setMaterial(objectShader->material);

      DrawInfo &info = objectShader->info;
	  if (info.isSkinned) {
		  SkinShader *skinProgram = (SkinShader *)objectShader->program;
		  renderTask->currentSkinMesh->boneTransform(0.5, transforms);
		  skinProgram->setBones(transforms);
	  }

      if (info.draw_type == DrawType::arrays) {
         objectShader->vao.draw_arrays(info.mode, 0, info.num_elements);
      } else {
         objectShader->vao.draw_elements(info.mode, 0, info.num_elements, info.index_type);
	  }
   }

   gbuffer.end_shadow();
   
   
   
   gbuffer.begin();
   gbuffer.setEye(mCamera.getPosition() - mCamera.getLookAt());
   gbuffer.setLight(mCamera.getPosition(), glm::vec3(1.0));

   glClearColor(.7,.7,1.0, 1.0);
   glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
   glClearColor(0,0,0,0);
   for (renderTask = mRenderQueue.begin(); renderTask != mRenderQueue.end(); renderTask++) {
      SceneObjectShader *objectShader = renderTask->object;

	   DrawInfo &info = objectShader->info;

      objectShader->program->use();
	  if (info.isSkinned) {
		  objectShader->program->setModel(renderTask->transpose * glm::rotate(90.0f, glm::vec3(1,0,0)));
	  } else {
		  objectShader->program->setModel(renderTask->transpose);
	  }

      objectShader->program->setView(mCamera.getViewMatrix());
      objectShader->program->setProjection(mCamera.getProjMatrix(1.3));
      objectShader->program->setMaterial(objectShader->material);

	  if (info.isSkinned) {
		  static float test = 0.0;
		  test += 0.01;
		  SkinShader *skinProgram = (SkinShader *)objectShader->program;
		  renderTask->currentSkinMesh->boneTransform(test, transforms);
		  skinProgram->setBones(transforms);
	  }
      if (info.draw_type == DrawType::arrays) {
         objectShader->vao.draw_arrays(info.mode, 0, info.num_elements);
      } else {
         objectShader->vao.draw_elements(info.mode, 0, info.num_elements, info.index_type);
      }
   }

   gbuffer.end();
   
   
   mRenderQueue.erase(mRenderQueue.begin(), mRenderQueue.end());
   
   glm::mat4 spotLight =  mSpotLight.getProjMatrix(1.3) * mSpotLight.getViewMatrix();
   gbuffer.setSpot(spotLight);
   
   gbuffer.draw(0.0f);
}

GBuffer SceneRenderer::getGBuffer() {
	return gbuffer;
}
