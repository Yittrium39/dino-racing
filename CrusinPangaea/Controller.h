// One sentence description:
// Classes that inherit from the Controller class are responsible for managing input
// from the mouse/keyboard and rendering one "screen" of the application.

#pragma once
#include "config.h"

class Controller {
protected:
   gl::window &mWindow;
public:
	Controller(gl::window &w) : mWindow(w) { }
	virtual void Update(float deltaT) { }
	virtual void Render() { }
	virtual void Event(gl::event &e) {  }
	virtual ~Controller() { }
};