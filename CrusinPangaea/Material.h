//
//  Material.h
//  CrusinPangaea
//
//  Created by Richie on 2/16/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#pragma once

#include "Texture.hpp"
#include <glm/glm.hpp>

struct Material {
   struct channel {
      bool hasTexture;
      float weight;
      gl::texture texture;
      glm::vec3 color;
      channel() : hasTexture(false), weight(0) { }
   };

   channel ambient;
   channel diffuse;
   channel specular;
   channel normal;
};