
#pragma once

#include "Renderable.h"
#include "Material.h"

#include <list>
#include <vector>
#include <memory>
#include <map>
#include <string>

#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

#ifndef _WIN32
class std::vector;
#endif
class SkinVertex;
typedef GLuint Index;

class SkinMesh {
   std::list<std::shared_ptr<Renderable> > mRenderables;
   std::vector<std::shared_ptr<Material> > mMaterials;
   
   void loadMeshesFromScene(const aiScene *pScene);

   std::shared_ptr<Renderable> loadMesh(const aiMesh *pMesh);
   std::vector<SkinVertex> loadMeshVertices(const aiMesh *pMesh) const;
   std::vector<Index> loadMeshIndices(const aiMesh *pMesh) const;
   void loadBones(const aiMesh *pMesh);

   void loadMaterialsFromScene(const aiScene *pScene, const std::string &textureDir);
   
   std::shared_ptr<Material> loadMaterial(const aiMaterial *pMaterial, const std::string &texDir) const;
private:
	unsigned int findPosition(float animationTime, const aiNodeAnim* pNodeAnim);
	unsigned int findRotation(float animationTime, const aiNodeAnim* pNodeAnim);
	unsigned int findScaling(float animationTime, const aiNodeAnim* pNodeAnim);

	void calcInterpolatedPosition(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
	void calcInterpolatedRotation(aiQuaternion& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
	void calcInterpolatedScaling(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
	void readNodeHierarchy(float AnimationTime, const aiNode* pNode, const glm::mat4& ParentTransform);
	const aiNodeAnim* findNodeAnim(const aiAnimation* pAnimation, const std::string NodeName);

	const aiScene* mScene;
	glm::mat4 m_GlobalInverseTransform;

	struct BoneInfo {
        glm::mat4 boneOffset;
        glm::mat4 finalTransformation;        

        BoneInfo() {
            boneOffset = glm::mat4(0.0f);
            finalTransformation = glm::mat4(0.0f);
        }
    };
    
    struct VertexBoneData {        
        unsigned int IDs[4];
        float weights[4];

        VertexBoneData() {
            reset();
        };
        
        void reset() {
            memset(IDs, 0, sizeof(IDs));
            memset(weights, 0, sizeof(weights));      
        }
        
		void addBoneData(unsigned int boneID, float weight);
	};

	std::vector<SkinMesh::VertexBoneData> bones;
	std::map<std::string, unsigned int> m_BoneMapping;
    unsigned int m_NumBones;
    std::vector<SkinMesh::BoneInfo> m_BoneInfo;
public:
   
   SkinMesh(const std::string &filename, const std::string &textureDir);
   ~SkinMesh();

   void SkinMesh::boneTransform(float TimeInSeconds, std::vector<glm::mat4>& Transforms);

   std::list<std::shared_ptr<Renderable> >::iterator begin() {
      return mRenderables.begin();
   }
   
   std::list<std::shared_ptr<Renderable> >::iterator end() {
      return mRenderables.end();
   }
      
   std::list<std::shared_ptr<Renderable> >::const_iterator begin() const {
      return mRenderables.begin();
   }
   
   std::list<std::shared_ptr<Renderable> >::const_iterator end() const {
      return mRenderables.end();
   }
   
   // This is here for compatibility with old code. Don't use this with any new code.
   // However, there is currently no other way to retrieve the mesh data, so deal with
   // it.
   mutable std::vector<glm::vec3> vertices;
   mutable std::vector<unsigned int> indices;
};