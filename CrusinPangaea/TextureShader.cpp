//
//  TextureShader.cpp
//  CrusinPangaea
//
//  Created by Richie on 2/17/13.
//  Copyright (c) 2013 Richie. All rights reserved.
//

#include "TextureShader.h"

#include <exception>
#include <iostream>
#include <glm/gtc/type_ptr.hpp>

TextureShader::TextureShader() {
   gl::shader mesh_vertex(gl::shader_type::vertex, GLSL(
      in vec3 position;
      in vec3 normal;
      in vec2 tex_coord;
      
      out vec2 tc;
      out vec3 norm;
      out vec3 pos;
      
      uniform mat4 model;
      uniform mat4 projection;
      uniform mat4 view;
      
      void main() {
         tc = tex_coord;
			mat3 mv = mat3(model[0].xyz, model[1].xyz, model[2].xyz);
			norm = mv * normal;
         pos = (model * vec4(position, 1)).xyz;
         gl_Position = projection * view * vec4(pos, 1);
      }
   ));
   gl::shader mesh_fragment(gl::shader_type::fragment, GLSL(
      in vec2 tc;
      in vec3 norm;
      in vec3 pos;
      
      out vec3 fnormal;
      out vec3 fcolor;
      out vec3 fpos;
      
      uniform sampler2D tex;
      void main() {
         vec4 color = texture(tex, tc, -4);
         if (color.a < 0.01) {
            discard;
         }
         fcolor = texture(tex, tc, -4).xyz;
         fnormal = norm;
         fpos = pos;
      }
   ));
   meshProgram.Attach(mesh_vertex);
   meshProgram.Attach(mesh_fragment);
   glBindFragDataLocation(meshProgram, 0, "fcolor");
   glBindFragDataLocation(meshProgram, 1, "fnormal");
   glBindFragDataLocation(meshProgram, 2, "fpos");
   meshProgram.Link();
}

bool TextureShader::canRenderRenderable(const Renderable &renderable) const {
   ShaderContract position_contract(ShaderAttribute::Position, 3);
   ShaderContract normal_contract(ShaderAttribute::Normal, 3);
   ShaderContract texcoord_contract(ShaderAttribute::TextureCoordinate, 2);
   
   AttributeInfo position_info;
   AttributeInfo normal_info;
   AttributeInfo texcoord_info;
   
   bool has_position = renderable.getAttributeInfo(position_contract, position_info);
   bool has_normal = renderable.getAttributeInfo(normal_contract, normal_info);
   bool has_texcoord = renderable.getAttributeInfo(texcoord_contract, texcoord_info);
   
   Material mat = renderable.getMaterial();
   
   return has_position && has_normal && has_texcoord && mat.diffuse.hasTexture;
}

gl::vertex_array TextureShader::vaoForRenderable(const Renderable &renderable) const {
   ShaderContract position_contract(ShaderAttribute::Position, 3);
   ShaderContract normal_contract(ShaderAttribute::Normal, 3);
   ShaderContract texcoord_contract(ShaderAttribute::TextureCoordinate, 2);
   
   AttributeInfo position_info;
   AttributeInfo normal_info;
   AttributeInfo texcoord_info;
   
   bool has_position = renderable.getAttributeInfo(position_contract, position_info);
   bool has_normal = renderable.getAttributeInfo(normal_contract, normal_info);
   bool has_texcoord = renderable.getAttributeInfo(texcoord_contract, texcoord_info);
   if (!has_position || !has_normal || !has_texcoord) {
      throw std::runtime_error("TextureShader: missing either position, normal, or texcoord.");
   }
   
   gl::vertex_array vao;
   gl::attribute position = meshProgram.get_attribute("position");
   bind_contract(vao, position, position_contract, position_info);
   
   gl::attribute normal = meshProgram.get_attribute("normal");
   bind_contract(vao, normal, normal_contract, normal_info);
   
   gl::attribute tex_coord = meshProgram.get_attribute("tex_coord");
   bind_contract(vao, tex_coord, texcoord_contract, texcoord_info);
   return vao;
}

void TextureShader::setModel(const glm::mat4 &modelm) {
   gl::uniform model = meshProgram.get_uniform("model");
   meshProgram.set_uniform_mat4(model, glm::value_ptr(modelm));
}

void TextureShader::setView(const glm::mat4 &viewm) {
   gl::uniform view = meshProgram.get_uniform("view");
   meshProgram.set_uniform_mat4(view, glm::value_ptr(viewm));
}

void TextureShader::setProjection(const glm::mat4 &proj) {
   gl::uniform projection = meshProgram.get_uniform("projection");
   meshProgram.set_uniform_mat4(projection, glm::value_ptr(proj));
}

void TextureShader::setMaterial(const Material &mat) {
   meshProgram.use();
   
   gl::uniform tex = meshProgram.get_uniform("tex");
   
   if (mat.diffuse.hasTexture) {
      meshProgram.set_uniform(tex, 0);
      mat.diffuse.texture.bind(0);
   }
}

void TextureShader::use() {
   meshProgram.use();
}