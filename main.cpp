// One Sentence Description:
// The entry point of the program creates and runs the Application
// and logs any fatal errors that have escaped the program.

#include "Application.h"
#include <exception>
#include <iostream>

using namespace std;

int main(int argc, char **argv) {
	try {
		Application app;
      app.run();
	} catch (std::exception &e) {
		std::cerr << "Unhandled exception: " << e.what() << std::endl;
	}
}